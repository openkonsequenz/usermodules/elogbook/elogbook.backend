/**
******************************************************************************
* Copyright © 2017-2018 PTA GmbH.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* 
*     http://www.eclipse.org/legal/epl-v10.html
* 
******************************************************************************
*/

package org.eclipse.openk.elogbook.controller;

import java.util.List;

import javax.persistence.EntityManager;

import org.eclipse.openk.elogbook.persistence.dao.AutoCloseEntityManager;
import org.eclipse.openk.elogbook.persistence.dao.EntityHelper;
import org.eclipse.openk.elogbook.persistence.dao.RefBranchDao;
import org.eclipse.openk.elogbook.persistence.dao.RefGridTerritoryDao;
import org.eclipse.openk.elogbook.persistence.dao.RefNotificationPriorityDao;
import org.eclipse.openk.elogbook.persistence.dao.RefNotificationStatusDao;
import org.eclipse.openk.elogbook.persistence.model.RefBranch;
import org.eclipse.openk.elogbook.persistence.model.RefGridTerritory;
import org.eclipse.openk.elogbook.persistence.model.RefNotificationPriority;
import org.eclipse.openk.elogbook.persistence.model.RefNotificationStatus;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class BackendControllerRefInfo {
	
	private static final Logger LOGGER = LogManager.getLogger(BackendControllerRefInfo.class.getName());

	public List<RefNotificationPriority> getNotificationPriorities() {
		LOGGER.debug("getNotificationPriorities is called");

		EntityManager emOrg = EntityHelper.getEMF().createEntityManager();
		try (AutoCloseEntityManager em = new AutoCloseEntityManager(emOrg)) {
			RefNotificationPriorityDao dao = new RefNotificationPriorityDao(em);
			return dao.findInTx(true, -1, 0);

		} finally {
			LOGGER.debug("getNotificationPriorities is finished");
		}
	}

	public List<RefNotificationStatus> getNotificationStatuses() {
		LOGGER.debug("getNotificationStatuses is called");

		EntityManager emOrg = EntityHelper.getEMF().createEntityManager();
		try (AutoCloseEntityManager em = new AutoCloseEntityManager(emOrg)) {
			RefNotificationStatusDao dao = new RefNotificationStatusDao(em);
			return dao.findInTx(true, -1, 0);

		} finally {
			LOGGER.debug("getNotificationStatuses is finished");
		}
	}

	public List<RefBranch> getBranches() {
		LOGGER.debug("getBranches is called");

		EntityManager emOrg = EntityHelper.getEMF().createEntityManager();
		try (AutoCloseEntityManager em = new AutoCloseEntityManager(emOrg)) {
			RefBranchDao dao = new RefBranchDao(em);
			return dao.findInTx(true, -1, 0);

		} finally {
			LOGGER.debug("getBranches is finished");
		}
	}

	public List<RefGridTerritory> getGridTerritories() {
		LOGGER.debug("getGridTerritories is called");

		EntityManager emOrg = EntityHelper.getEMF().createEntityManager();
		try (AutoCloseEntityManager em = new AutoCloseEntityManager(emOrg)) {
			RefGridTerritoryDao dao = new RefGridTerritoryDao(em);
			return dao.findInTx(true, -1, 0);

		} finally {
			LOGGER.debug("getGridTerritories is finished");
		}
	}
}
