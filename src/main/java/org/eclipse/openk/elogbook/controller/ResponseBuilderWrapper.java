/**
******************************************************************************
* Copyright © 2017-2018 PTA GmbH.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* 
*     http://www.eclipse.org/legal/epl-v10.html
* 
******************************************************************************
*/
package org.eclipse.openk.elogbook.controller;

import org.apache.http.HttpStatus;
import org.eclipse.openk.elogbook.common.Globals;
import org.eclipse.openk.elogbook.exceptions.BtbException;
import org.eclipse.openk.elogbook.exceptions.BtbInternalServerError;

import javax.ws.rs.core.Response;
import java.nio.charset.StandardCharsets;

public enum ResponseBuilderWrapper {
    INSTANCE;

    public Response.ResponseBuilder getResponseBuilder( String json ) throws BtbInternalServerError {
        return getResponseBuilder(jsonStringToBytes( json ));

    }

    private Response.ResponseBuilder getResponseBuilder(byte[] json) {
        return Response.status(HttpStatus.SC_OK).entity(json)
                .header("Content-Type", "application/json; charset=utf-8")
                .header("X-XSS-Protection", "1; mode = block")
                .header("X-DNS-Prefetch-Control", "off")
                .header("X-Content-Type-Options", "nosniff")
                .header("X-Frame-Options", "sameorigin")
                .header("Strict-Transport-Security", "max-age=15768000; includeSubDomains")
                .header("Cache-Control", "no-cache; no-store; must-revalidate")
                .header("Pragma", "no-cache")
                .header("Expires", "0")
                .header("Access-Control-Allow-Origin", "*");
    }

    private byte[] jsonStringToBytes( String jsonString ) throws BtbInternalServerError {
        return jsonString.getBytes(StandardCharsets.UTF_8);
    }

    public Response buildOKResponse(String jsonString) throws BtbException {
        return getResponseBuilder( jsonStringToBytes( jsonString)).build();
    }


    public Response buildOKResponse(String jsonString, String sessionToken) throws BtbException  {
        return getResponseBuilder(jsonStringToBytes(jsonString)).header(Globals.SESSION_TOKEN_TAG, sessionToken).build();
    }

}
