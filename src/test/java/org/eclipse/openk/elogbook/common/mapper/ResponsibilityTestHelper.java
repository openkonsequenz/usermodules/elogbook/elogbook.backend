/**
******************************************************************************
* Copyright © 2017-2018 PTA GmbH.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* 
*     http://www.eclipse.org/legal/epl-v10.html
* 
******************************************************************************
*/
package org.eclipse.openk.elogbook.common.mapper;


import static org.easymock.EasyMock.expect;
import static org.easymock.EasyMock.replay;

import java.util.LinkedList;
import java.util.List;
import org.eclipse.openk.elogbook.persistence.dao.RefBranchDao;
import org.eclipse.openk.elogbook.persistence.dao.RefGridTerritoryDao;
import org.eclipse.openk.elogbook.persistence.model.RefBranch;
import org.eclipse.openk.elogbook.persistence.model.RefGridTerritory;
import org.powermock.api.easymock.PowerMock;

public class ResponsibilityTestHelper {
    private static RefGridTerritory newRefGridTerritoryItem(int id, String name, String descritpion) {
        RefGridTerritory ret = new RefGridTerritory();
        ret.setId(id);
        ret.setName(name);
        ret.setDescription(descritpion);
        return ret;
    }

    private static RefBranch newBranch(int id, String name, String description) {
        RefBranch ret = new RefBranch();
        ret.setId(id);
        ret.setName(name);
        ret.setDescription(description);
        return ret;
    }

    private static RefGridTerritoryDao createStatusDaoMock(List<RefGridTerritory> rgtList) {
        RefGridTerritoryDao rgtdaoMock = PowerMock.createNiceMock(RefGridTerritoryDao.class);
        expect(rgtdaoMock.findInTx(true, -1, 0)).andReturn(rgtList);
        replay(rgtdaoMock);
        return rgtdaoMock;
    }

    private static RefBranchDao createBranchDaoMock(List<RefBranch> rbList) {
        RefBranchDao rbdaoMock = PowerMock.createNiceMock(RefBranchDao.class);
        expect(rbdaoMock.findInTx(true, -1, 0)).andReturn(rbList);
        replay(rbdaoMock);
        return rbdaoMock;
    }

    public static ResponsibilityMapper createMapper() {
        List<RefGridTerritory> refGridTerritoryLinkedList = new LinkedList<>();
        refGridTerritoryLinkedList.add(newRefGridTerritoryItem(1, "MA","Mannheim"));
        refGridTerritoryLinkedList.add(newRefGridTerritoryItem(2, "OF","Offenbach"));
        refGridTerritoryLinkedList.add(newRefGridTerritoryItem(3, "DR","Dreieich"));

        RefGridTerritoryDao rgtDao = createStatusDaoMock(refGridTerritoryLinkedList);

        List<RefBranch> rbList = new LinkedList<>();
        rbList.add(newBranch(1, "S", "Strom"));
        rbList.add(newBranch(2, "G", "Gas"));
        rbList.add(newBranch(3, "F", "Fernwärme"));
        rbList.add(newBranch(4, "W", "Wasser"));
        rbList.add(newBranch(5, "Z", "ZSM"));
        RefBranchDao rbDao = createBranchDaoMock(rbList);

        return new ResponsibilityMapper();
    }
}
