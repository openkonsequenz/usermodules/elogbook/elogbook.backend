/**
******************************************************************************
* Copyright © 2017-2018 PTA GmbH.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* 
*     http://www.eclipse.org/legal/epl-v10.html
* 
******************************************************************************
*/
package org.eclipse.openk.elogbook.persistence.dao;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import org.eclipse.openk.elogbook.persistence.model.RefVersion;

public class RefVersionDao extends GenericDaoJpa<RefVersion, Long> {
    public RefVersionDao() {
        super();
    }

    public RefVersionDao(EntityManager em) {
        super(em);
    }

    public RefVersion getVersionInTx() {
        try {
            String selectString = "from RefVersion t";
            Query q = getEM().createQuery(selectString);
            return (RefVersion) q.getSingleResult();
        } catch (Throwable t) { // NOSONAR
            LOGGER.error(t.getMessage()); // NOSONAR
            return null;
        }
    }

}
