/**
******************************************************************************
* Copyright © 2017-2018 PTA GmbH.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* 
*     http://www.eclipse.org/legal/epl-v10.html
* 
******************************************************************************
*/
package org.eclipse.openk.elogbook.common.mapper;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.eclipse.openk.elogbook.persistence.dao.RefBranchDao;
import org.eclipse.openk.elogbook.persistence.dao.RefGridTerritoryDao;
import org.eclipse.openk.elogbook.persistence.model.HTblResponsibility;
import org.eclipse.openk.elogbook.persistence.model.RefBranch;
import org.eclipse.openk.elogbook.persistence.model.RefGridTerritory;
import org.eclipse.openk.elogbook.persistence.model.TblResponsibility;
import org.eclipse.openk.elogbook.viewmodel.HistoricalResponsibility;
import org.eclipse.openk.elogbook.viewmodel.HistoricalShiftChanges;
import org.eclipse.openk.elogbook.viewmodel.Responsibility;
import org.eclipse.openk.elogbook.viewmodel.TerritoryResponsibility;

public class HResponsibilityMapper {

  private static final Logger LOGGER = LogManager.getLogger(HResponsibilityMapper.class.getName());
  private final HashMap<String, RefBranch> refBranchMap;
  private final HashMap<String, RefGridTerritory> refGridTerritoryMap;

  public HResponsibilityMapper(RefGridTerritoryDao rgtDao, RefBranchDao rbDao) {
    List<RefGridTerritory> refGridTerritoryList = rgtDao.findInTx(true, -1, 0);
    refGridTerritoryMap = new HashMap<>(refGridTerritoryList.size());
    //refGridTerritoryMap.getName is a unique index in DB so we can rely on it that it's unique
    for (RefGridTerritory item : refGridTerritoryList) {
      refGridTerritoryMap.put(item.getName(), item);
    }

    List<RefBranch> rbList = rbDao.findInTx(true, -1, 0);
    refBranchMap = new HashMap<>(rbList.size());
    for (RefBranch item : rbList) {
      refBranchMap.put(item.getName(), item);
    }
  }

  public static HTblResponsibility mapFromTblResponsibility(TblResponsibility tblResponsibility){
    return new HTblResponsibility(tblResponsibility);
  }

	/**
	 * Create the HistoricalShiftChanges Object in View Model from the
	 * historical responsibilities entities found in the specified period.
	 *
	 * @param hTblResponsibilities
	 * @param transferDateFrom
	 * @param transferDateTo
	 * @return
	 */
	public HistoricalShiftChanges mapTblResponsibilitiesInPeriod(List<HTblResponsibility> hTblResponsibilities,
			Date transferDateFrom, Date transferDateTo) {

		HistoricalShiftChanges historicalShiftChanges = new HistoricalShiftChanges();
		historicalShiftChanges.setTransferDateFrom(transferDateFrom);
		historicalShiftChanges.setTransferDateTo(transferDateTo);
		historicalShiftChanges.setHistoricalResponsibilities(mapToVModelList(hTblResponsibilities));
		return historicalShiftChanges;
	}


  public HistoricalResponsibility mapToVModel(HTblResponsibility htblResponsibility) {

    if (htblResponsibility == null) {
      return null;
    }

    HistoricalResponsibility hResponsibility = new HistoricalResponsibility();
    hResponsibility.setId(htblResponsibility.getId());
    hResponsibility.setTransactionId(htblResponsibility.getTransactionId());

    hResponsibility.setResponsibleUser(htblResponsibility.getResponsibleUser());
    hResponsibility.setFormerResponsibleUser(htblResponsibility.getFormerResponsibleUser());
    hResponsibility.setCreateUser(htblResponsibility.getCreateUser());
    hResponsibility.setModUser(htblResponsibility.getModUser());

    if (htblResponsibility.getTransferDate() != null) {
        hResponsibility.setTransferDate(new Date(htblResponsibility.getTransferDate().getTime()));
    }
    if (htblResponsibility.getCreateDate() != null) {
        hResponsibility.setCreateDate(new Date(htblResponsibility.getCreateDate().getTime()));
    }
    if (htblResponsibility.getModDate() != null) {
        hResponsibility.setModDate(new Date(htblResponsibility.getModDate().getTime()));
    }
    if (htblResponsibility.getRefBranch() != null) {
        hResponsibility.setRefBranch(htblResponsibility.getRefBranch());
    }
    if (htblResponsibility.getRefGridTerritory() != null)  {
      hResponsibility.setRefGridTerritory(htblResponsibility.getRefGridTerritory());
    }

    return hResponsibility;
  }


  public List<HistoricalResponsibility> mapToVModelList(List<HTblResponsibility> hTblResponsibilityList) {
    LOGGER.debug("mapToVModelList() is called");
    List<HistoricalResponsibility> historicalResponsibilityList = new ArrayList<>();

    for (HTblResponsibility hTblResponsibility : hTblResponsibilityList) {
        historicalResponsibilityList.add(mapToVModel(hTblResponsibility));
    }

    LOGGER.debug("mapToVModelList() is finished");
    return historicalResponsibilityList;
  }

  public List<TerritoryResponsibility> mapToContainerVModelList(List<HTblResponsibility> hTblResponsibilityList) {
    LOGGER.debug("mapToContainerVModelList() is called");

    Map<String, TerritoryResponsibility> responsibilityHashMap = new LinkedHashMap<>();

    for (HTblResponsibility hTblResponsibility : hTblResponsibilityList) {
      String tblRespLocation = hTblResponsibility.getRefGridTerritory().getDescription();
      if (responsibilityHashMap.containsKey(tblRespLocation)) {
        TerritoryResponsibility existingResponsibility = responsibilityHashMap.get(tblRespLocation);
        mapToContainerVModel(hTblResponsibility, existingResponsibility);
      } else {
        responsibilityHashMap.put(tblRespLocation, mapToContainerVModel(hTblResponsibility, null));
      }
    }

    List<TerritoryResponsibility> responsibilityList = new ArrayList<>(responsibilityHashMap.values());

    LOGGER.debug("mapToContainerVModelList() is finished");
    return responsibilityList;
  }

  public TerritoryResponsibility mapToContainerVModel(HTblResponsibility hTblResponsibility, TerritoryResponsibility existingResponsibilityCont) {
    TerritoryResponsibility vmResponsibilityRet;

    if (existingResponsibilityCont == null) {
      vmResponsibilityRet = new TerritoryResponsibility();
      vmResponsibilityRet.setGridTerritoryDescription(hTblResponsibility.getRefGridTerritory().getDescription());
      List<Responsibility> responsibilityList = new ArrayList<>();
      responsibilityList.add(mapToVModelForContainer(hTblResponsibility));
      vmResponsibilityRet.setResponsibilityList(responsibilityList);
    } else {
      vmResponsibilityRet = existingResponsibilityCont;
      vmResponsibilityRet.getResponsibilityList().add(mapToVModelForContainer(hTblResponsibility));
    }

    return vmResponsibilityRet;
  }

  public Responsibility mapToVModelForContainer(HTblResponsibility hTblResponsibility) {

    Responsibility responsibility = new Responsibility();
    responsibility.setId(hTblResponsibility.getId());
    responsibility.setResponsibleUser(hTblResponsibility.getResponsibleUser());
    responsibility.setNewResponsibleUser(hTblResponsibility.getFormerResponsibleUser());
    responsibility.setBranchName(hTblResponsibility.getRefBranch().getName());
    responsibility.setIsActive(true);

    return responsibility;
  }

}
