/**
******************************************************************************
* Copyright © 2017-2018 PTA GmbH.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* 
*     http://www.eclipse.org/legal/epl-v10.html
* 
******************************************************************************
*/
package org.eclipse.openk.elogbook.persistence.model;

import javax.persistence.Entity;
import javax.persistence.Table;


/**
 * The persistent class for the tbl_notification database table.
 *
 */
@Entity
@Table(name="TBL_NOTIFICATION")
public class TblNotification extends AbstractNotification {
	private static final long serialVersionUID = 1L;


}