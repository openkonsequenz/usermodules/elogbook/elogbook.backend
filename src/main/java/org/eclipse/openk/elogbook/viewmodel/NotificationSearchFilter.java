/**
******************************************************************************
* Copyright © 2017-2018 PTA GmbH.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* 
*     http://www.eclipse.org/legal/epl-v10.html
* 
******************************************************************************
*/
package org.eclipse.openk.elogbook.viewmodel;

import java.util.Date;
import java.util.List;

/**
 * Define criteria whether to restrict the notifications to be delivered, e. g.
 * by time, responsibility...
 *
 */
public class NotificationSearchFilter {

	/** Filtering notifications by date (only deliver notifications starting after dateFrom). */
	private Date dateFrom;

	/** Filtering notifications by date (only deliver notifications until dateTo). */
	private Date dateTo;

	private Date reminderDate;

	/** Filter notifications depending on responsibilities. Empty list, if no responsibility */
	private List<Integer> responsibilityFilterList;
	
	/** Flag indicating historical notifications request. */
	private Boolean historicalFlag;
	
	/** The shift change transaction id in historical responsibilities table. */
	private Integer shiftChangeTransactionId;
	
	/**************************************************************************
	 * Getters/Setters
	 **************************************************************************/

	public Date getDateFrom() {
		return dateFrom;
	}

	public void setDateFrom(Date dateFrom) {
		this.dateFrom = dateFrom;
	}

	public Date getDateTo() {
		return dateTo;
	}

	public void setDateTo(Date dateTo) {
		this.dateTo = dateTo;
	}

	public List<Integer> getResponsibilityFilterList() {
		return responsibilityFilterList;
	}

	public void setResponsibilityFilterList(List<Integer> responsibilityFilterList) {
		this.responsibilityFilterList = responsibilityFilterList;
	}

	public Date getReminderDate() { return reminderDate; }

	public void setReminderDate(Date reminderDate) { this.reminderDate = reminderDate; }
	
	public Boolean isHistoricalFlag() {
		if (historicalFlag == null) {
			return Boolean.FALSE;
		}
		return historicalFlag;
	}

	public void setHistoricalFlag(Boolean historicalFlag) {
		this.historicalFlag = historicalFlag;
	}

	public Integer getShiftChangeTransactionId() {
		return shiftChangeTransactionId;
	}

	public void setShiftChangeTransactionId(Integer shiftChangeTransactionId) {
		this.shiftChangeTransactionId = shiftChangeTransactionId;
	}

}
