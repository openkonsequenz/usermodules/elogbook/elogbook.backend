/**
******************************************************************************
* Copyright © 2017-2018 PTA GmbH.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* 
*     http://www.eclipse.org/legal/epl-v10.html
* 
******************************************************************************
*/
package org.eclipse.openk.elogbook.controller;

import org.eclipse.openk.elogbook.common.mapper.NotificationMapper;
import org.eclipse.openk.elogbook.common.util.ResourceLoaderBase;
import org.eclipse.openk.elogbook.exceptions.BtbInternalServerError;
import org.eclipse.openk.elogbook.exceptions.BtbLocked;
import org.eclipse.openk.elogbook.persistence.dao.HTblResponsibilityDao;
import org.eclipse.openk.elogbook.persistence.dao.RefVersionDao;
import org.eclipse.openk.elogbook.persistence.dao.TblNotificationDao;
import org.eclipse.openk.elogbook.persistence.dao.TblResponsibilityDao;
import org.eclipse.openk.elogbook.persistence.model.*;
import org.eclipse.openk.elogbook.viewmodel.*;
import org.junit.Test;
import org.powermock.api.easymock.PowerMock;
import org.powermock.reflect.Whitebox;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.easymock.EasyMock.*;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class BackendControllerTest  extends ResourceLoaderBase {


    @Test
    public void testGetVersionInfoImpl_OK() throws Exception {
        RefVersion rv = new RefVersion();
        rv.setId(1);
        rv.setVersion("-666-");
        String backendVersion = "-888-";
        RefVersionDao daoMock = PowerMock.createNiceMock(RefVersionDao.class);
        expect(daoMock.getVersionInTx()).andReturn(rv);
        replay(daoMock);
        BackendControllerVersionInfo bec = new BackendControllerVersionInfo();
        VersionInfo vi = Whitebox.invokeMethod(bec, "getVersionInfoImpl", daoMock, backendVersion);
        assertEquals(vi.getBackendVersion(), backendVersion);
        assertEquals(vi.getDbVersion(), "-666-");
    }

    @Test
    public void testGetVersionInfoImpl_NODB() throws Exception {
        String backendVersion = "-888-";
        RefVersionDao daoMock = PowerMock.createNiceMock(RefVersionDao.class);
        expect(daoMock.getVersionInTx()).andReturn(null);
        replay(daoMock);
        BackendControllerVersionInfo bec = new BackendControllerVersionInfo();
        VersionInfo vi = Whitebox.invokeMethod(bec, "getVersionInfoImpl", daoMock, backendVersion);
        assertEquals(vi.getDbVersion(), "NO_DB");
    }


    @Test
    public void testStoreNotificationInDB_Insert() throws Exception {
        Notification not = new Notification();
        not.setId(null);
        not.setIncidentId(null);  // should be ignored later on because these fields are set by the db
        not.setVersion(222); // should be ignored later on because these fields are set by the db
        not.setFkRefNotificationStatus(2);
        not.setFkRefBranch(1);
        not.setNotificationText("myTextYourText");
        not.setFkRefGridTerritory(1);

        RefBranch rb = new RefBranch();
        rb.setId(1);
        rb.setName("NamosUnos");
        rb.setDescription("Descriptore");
        RefNotificationStatus rns = new RefNotificationStatus();
        rns.setId(2);
        rns.setName("NamosDos");

        TblNotification notificationFromDB = new TblNotification();
        notificationFromDB.setId(43);
        notificationFromDB.setIncidentId(1);
        notificationFromDB.setVersion(1);
        notificationFromDB.setRefBranch(rb);
        notificationFromDB.setRefNotificationStatus(rns);
        notificationFromDB.setNotificationText("myTextYourText");
        notificationFromDB.setRefGridTerritory(createRefGridTerritory());
        NotificationMapper mappy = NotificationTestHelper.createMapper();
        TblNotificationDao daoMock = PowerMock.createNiceMock(TblNotificationDao.class);
        //expect(daoMock.storeInTx(isA(TblNotification.class))).andReturn(notificationFromDB);
        //expect(daoMock.persistInTx(isA(TblNotification.class))).andVoid();
        replay(daoMock);
        BackendControllerNotification bec = new BackendControllerNotification();

        Notification testable = Whitebox.invokeMethod(bec, "storeNotificationInDB", not, daoMock, mappy, "moddy");
        /*assertEquals((Integer)43, (Integer)testable.getId());
        assertEquals((Integer)1, (Integer)testable.getIncidentId());
        assertEquals((Integer)1, (Integer)testable.getVersion());
        assertEquals(not.getNotificationText(), testable.getNotificationText());*/ //todo

    }


    @Test
    public void testStoreNotificationInDB_Update() throws Exception {
        Notification not = new Notification();
        not.setId(22);
        not.setIncidentId(333);  // should be ignored later on because these fields are set by the db
        not.setVersion(222); // should be ignored later on because these fields are set by the db
        not.setFkRefNotificationStatus(2);
        not.setFkRefBranch(1);
        not.setFkRefGridTerritory(1);
        not.setNotificationText("myTextYourText");

        RefBranch rb = new RefBranch();
        rb.setId(1);
        rb.setName("NamosUnos");
        rb.setDescription("Descriptore");
        RefNotificationStatus rns = new RefNotificationStatus();
        rns.setId(2);
        rns.setName("NamosDos");

        TblNotification notificationFromDB = new TblNotification();
        notificationFromDB.setId(43);
        notificationFromDB.setIncidentId(1);
        notificationFromDB.setVersion(1);
        notificationFromDB.setRefBranch(rb);
        notificationFromDB.setRefNotificationStatus(rns);
        notificationFromDB.setNotificationText("myTextYourText");
        notificationFromDB.setRefGridTerritory(createRefGridTerritory());
        NotificationMapper mappy = NotificationTestHelper.createMapper();
        TblNotificationDao daoMock = PowerMock.createNiceMock(TblNotificationDao.class);
        //expect(daoMock.storeInTx(isA(TblNotification.class))).andReturn(notificationFromDB);
        //expect(daoMock.persistInTx(isA(TblNotification.class))).andVoid();
        replay(daoMock);
        BackendControllerNotification bec = new BackendControllerNotification();

        Notification testable = Whitebox.invokeMethod(bec, "storeNotificationInDB", not, daoMock, mappy, "moddy");


    }


    @Test(expected = Exception.class)
    public void testStoreNotificationInDB_Exception() throws Exception {
        Notification not = new Notification();
        not.setId(22);
        not.setIncidentId(333);  // should be ignored later on because these fields are set by the db
        not.setVersion(222); // should be ignored later on because these fields are set by the db
        not.setFkRefNotificationStatus(2);
        not.setFkRefBranch(1);
        not.setNotificationText("myTextYourText");

        RefBranch rb = new RefBranch();
        rb.setId(1);
        rb.setName("NamosUnos");
        rb.setDescription("Descriptore");
        RefNotificationStatus rns = new RefNotificationStatus();
        rns.setId(2);
        rns.setName("NamosDos");

        TblNotification notificationFromDB = new TblNotification();
        notificationFromDB.setId(43);
        notificationFromDB.setIncidentId(1);
        notificationFromDB.setVersion(1);
        notificationFromDB.setRefBranch(rb);
        notificationFromDB.setRefNotificationStatus(rns);
        notificationFromDB.setNotificationText("myTextYourText");
        NotificationMapper mappy = NotificationTestHelper.createMapper();
        TblNotificationDao daoMock = PowerMock.createNiceMock(TblNotificationDao.class);
        //expect(daoMock.storeInTx(isA(TblNotification.class))).andReturn(notificationFromDB);
        daoMock.persistInTx(anyObject());
        expectLastCall().andThrow(new Exception());
        replay(daoMock);
        BackendControllerNotification bec = new BackendControllerNotification();

        Notification testable = Whitebox.invokeMethod(bec, "storeNotificationInDB", not, daoMock, mappy, "moddy");
    }

    @Test(expected = BtbLocked.class)
    public void testCreateNotification_locked() throws Exception {
        // todo test with tblNotification instead of view model notification
        throw new BtbLocked();
        /*
        String json = super.loadStringFromResource("testLockedNotification.json");
        TblNotification noti = JsonGeneratorBase.getGson().fromJson(json, Notification.class);

        IBackendController bec = new IBackendController();
        Whitebox.invokeMethod(bec, "checkBlockedNotification", noti);
        */
    }
	@Test
	public void testGetNotificationListByType() throws Exception {
		List<TblNotification> futureList = new ArrayList<>();
		futureList.add(new TblNotification());

		List<TblNotification> openList = new ArrayList<>();
		openList.add(new TblNotification());
		openList.add(new TblNotification());

		List<TblNotification> pastList = new ArrayList<>();
		pastList.add(new TblNotification());
		pastList.add(new TblNotification());
		pastList.add(new TblNotification());

		List<TblResponsibility> responsibilityList = new ArrayList<>();
		responsibilityList.add(new TblResponsibility());
		responsibilityList.add(new TblResponsibility());

		TblNotificationDao tblNotificationDaoMock = PowerMock.createNiceMock(TblNotificationDao.class);
		expect(tblNotificationDaoMock.getFutureNotifications(null, new ArrayList<TblResponsibility>())).andReturn(futureList);

		expect(tblNotificationDaoMock.getOpenNotifications(new NotificationSearchFilter(), new ArrayList<TblResponsibility>())).andReturn(openList);
		expect(tblNotificationDaoMock.getPastNotifications(null, new ArrayList<TblResponsibility>())).andReturn(pastList);
		replay(tblNotificationDaoMock);
		TblResponsibilityDao tblResponsibilityDaoMock = PowerMock.createNiceMock(TblResponsibilityDao.class);

		BackendControllerNotification bec = new BackendControllerNotification();

		assertTrue(0 == ((List<TblNotification>) Whitebox.invokeMethod(bec, "getNotificationListByType",
				tblResponsibilityDaoMock, tblNotificationDaoMock, Notification.ListType.ALL, null)).size());
		// ALL is currently not returned by this function and ends in the
		// default case with an empty List

		assertTrue(1 == ((List<TblNotification>) Whitebox.invokeMethod(bec, "getNotificationListByType",
				tblResponsibilityDaoMock, tblNotificationDaoMock, Notification.ListType.FUTURE, null)).size());
		//TODO creating Mock for ResponsibilityDao to fix this test
		//assertTrue(2 == ((List<TblNotification>) Whitebox.invokeMethod(bec, "getNotificationListByType",
		//		tblResponsibilityDaoMock, tblNotificationDaoMock, Notification.ListType.OPEN, new NotificationSearchFilter())).size());
		assertTrue(3 == ((List<TblNotification>) Whitebox.invokeMethod(bec, "getNotificationListByType",
				tblResponsibilityDaoMock, tblNotificationDaoMock, Notification.ListType.PAST, null)).size());

	}

    private RefGridTerritory createRefGridTerritory() {
    	RefGridTerritory refGridTerritory = new RefGridTerritory();
    	refGridTerritory.setId(1);
    	refGridTerritory.setDescription("MA");
    	refGridTerritory.setName("Mannheim");
    	RefGridTerritory refMaster = new RefGridTerritory();
    	refMaster.setId(1);
    	refGridTerritory.setRefMaster(refMaster);
    	return refGridTerritory;
    }
    
	@Test
	public void testGetHistoricalShiftChanges() throws BtbInternalServerError {
		HistoricalShiftChanges historicalShiftChanges = new HistoricalShiftChanges();
		Date now = new Date();
		historicalShiftChanges.setTransferDateFrom(now);
		historicalShiftChanges.setTransferDateTo(now);

		List<HTblResponsibility> hTblResponsibilities = new ArrayList<>();
		hTblResponsibilities.add(new HTblResponsibility());
		hTblResponsibilities.add(new HTblResponsibility());
		

		HTblResponsibilityDao daoMock = PowerMock.createNiceMock(HTblResponsibilityDao.class);

		expect(daoMock.findHTblResponsibilitiesInPeriod(historicalShiftChanges.getTransferDateFrom(),
				historicalShiftChanges.getTransferDateTo())).andReturn(hTblResponsibilities);

	}

    @Test
    public void testGetNotificationsWithReminder() throws BtbInternalServerError {
        Notification notification = new Notification();
        Date now = new Date();
        notification.setReminderDate(now);
        ReminderSearchFilter nsf = new ReminderSearchFilter();
        List<TblResponsibility> tblResponsibilities = new ArrayList<>();

        List<TblNotification> notifications = new ArrayList<>();
        notifications.add(new TblNotification());

        TblNotificationDao daoMock = PowerMock.createNiceMock(TblNotificationDao.class);

        expect(daoMock.getNotificationsWithReminder(nsf, tblResponsibilities)).andReturn(notifications);

    }

}
