/**
******************************************************************************
* Copyright © 2017-2018 PTA GmbH.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* 
*     http://www.eclipse.org/legal/epl-v10.html
* 
******************************************************************************
*/
package org.eclipse.openk.elogbook.persistence.model;

import static org.junit.Assert.assertEquals;

import org.eclipse.openk.elogbook.common.JsonGeneratorBase;
import org.eclipse.openk.elogbook.common.util.ResourceLoaderBase;
import org.junit.Test;

public class RefNotificationStatusTest extends ResourceLoaderBase {
	// IMPORTANT TEST!!!
	// Make sure, our Interface produces a DEFINED Json!
	// Changes in the interface will HOPEFULLY crash here!!!

	@Test
	public void TestStructureAgainstJson() {
		String json = super.loadStringFromResource("testRefNotificationStatus.json");
		RefNotificationStatus status = JsonGeneratorBase.getGson().fromJson(json, RefNotificationStatus.class);
		assertEquals((int) status.getId(), 3);
		assertEquals(status.getName(), "status1");
	}

	@Test
	public void TestSetters() {
		RefNotificationStatus status = new RefNotificationStatus();
		status.setId(3);
		status.setName("status1");
	}
}
