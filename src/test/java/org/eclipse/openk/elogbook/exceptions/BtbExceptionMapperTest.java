/**
******************************************************************************
* Copyright © 2017-2018 PTA GmbH.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* 
*     http://www.eclipse.org/legal/epl-v10.html
* 
******************************************************************************
*/
package org.eclipse.openk.elogbook.exceptions;


import org.eclipse.openk.elogbook.common.JsonGeneratorBase;
import org.eclipse.openk.elogbook.common.util.ResourceLoaderBase;
import org.eclipse.openk.elogbook.viewmodel.ErrorReturn;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class BtbExceptionMapperTest extends ResourceLoaderBase {
    @Test
    public void testToJson() {
        String json = BtbExceptionMapper.toJson(new BtbNotFound("lalilu"));

        ErrorReturn er = JsonGeneratorBase.getGson().fromJson(json, ErrorReturn.class);
        assertEquals(er.getErrorCode(), 404);
        assertTrue(er.getErrorText().equals("lalilu"));
    }

    @Test
    public void testUnknownErrorToJson() {
        String json = BtbExceptionMapper.unknownErrorToJson();

        ErrorReturn er = JsonGeneratorBase.getGson().fromJson(json, ErrorReturn.class);
        assertEquals(er.getErrorCode(), 500);
    }

    @Test
    public void testGeneralOKJson() {
        String ok = BtbExceptionMapper.getGeneralOKJson();
        assertTrue("{\"ret\":\"OK\"}".equals(ok));
    }

    @Test
    public void testGeneralErrorJson() {
        String nok = BtbExceptionMapper.getGeneralErrorJson();
        assertTrue("{\"ret\":\"NOK\"}".equals(nok));
    }



}
