variables:
  FF_USE_FASTZIP: "true" # enable fastzip - a faster zip implementation that also supports level configuration.
  ARTIFACT_COMPRESSION_LEVEL: default
  CACHE_COMPRESSION_LEVEL: default
  TRANSFER_METER_FREQUENCY: 5s # will display transfer progress every 5 seconds for artifacts and remote caches.
  MAVEN_OPTS: "-Djava.awt.headless=true -Dmaven.repo.local=${CI_PROJECT_DIR}/.m2/repository/"
  MAVEN_CLI_OPTS: "--batch-mode --errors --fail-at-end --show-version"
  SONAR_USER_HOME: ${CI_PROJECT_DIR}/.sonar  # Defines the location of the analysis task cache
  GIT_DEPTH: 0  # Tells git to fetch all the branches of the project, required by the analysis task

cache:
  key: ${CI_COMMIT_REF_SLUG}
  paths:
    - ${CI_PROJECT_DIR}/.m2/repository/

stages:
  - build
  - test
  - sonarqube
#  - test-build
  - dockerimage
  - dockerimage-ext-job

#-----------------------------------------------------------------------------------------------------------------------
# Build
#-----------------------------------------------------------------------------------------------------------------------
stage-build:
  image: maven:3.6-jdk-8
  stage: build
  cache:
    paths:
      - ${CI_PROJECT_DIR}/.m2/repository/
  script:
    - mvn clean package -DskipTests=true
  artifacts:
    paths:
      - ./deploy/conf/
      - ./Dockerfile
      - target/*.war
  except:
    - tags

#-----------------------------------------------------------------------------------------------------------------------
# Test
#-----------------------------------------------------------------------------------------------------------------------
.stage-test:
  stage: test
  cache:
    paths:
      - ${CI_PROJECT_DIR}/.m2/repository/
  image: maven:3.6-jdk-8
  script:
    - mvn test -Dskip.asciidoc=true
  artifacts:
    paths:
      - target/
      - src/
    reports:
      junit:
        - target/surefire-reports/TEST-*.xml

#-----------------------------------------------------------------------------------------------------------------------
# SonarQube
#-----------------------------------------------------------------------------------------------------------------------
.sonarqube-check:
  stage: sonarqube
  image:
    name: sonarsource/sonar-scanner-cli:4.6
    entrypoint: [""]
  cache:
    key: ${CI_JOB_NAME}
    paths:
      - ${CI_PROJECT_DIR}/.sonar/cache
  script:
    - sonar-scanner -Dsonar.qualitygate.wait=true
  allow_failure: true
  dependencies:
    - stage-test

#-----------------------------------------------------------------------------------------------------------------------
# Dockerimage (hier ausschließlich für Init-DB-PostgreSQL)
#-----------------------------------------------------------------------------------------------------------------------
.docker-build-script:
  image: docker:20.10.7-git
  variables:
    WORKSPACE: ""
  services:
    - docker:dind
  before_script:
    - docker login -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD" $CI_REGISTRY
  script:
    - ls -l
    - |
      if [[ "$CI_COMMIT_TAG" == "" ]]; then
        tag="sha-${CI_COMMIT_SHORT_SHA}"
      else
        tag="$CI_COMMIT_TAG"
      fi
    - echo current tag ${tag}
    - CI_SOURCE_BRANCH=$(git for-each-ref | grep $CI_COMMIT_SHA | grep origin | sed "s/.*\///" | tr '[:upper:]' '[:lower:]')
    - echo CI_SOURCE_BRANCH $CI_SOURCE_BRANCH
    - REGISTRY_IMAGE_BASE="$CI_REGISTRY_IMAGE/$CI_SOURCE_BRANCH/$IMG_NAME"
    - FINAL_REGISTRY_IMAGE="$CI_REGISTRY_IMAGE/$CI_SOURCE_BRANCH/$IMG_NAME:${tag}"
    - echo "FINAL_REGISTRY_IMAGE=$FINAL_REGISTRY_IMAGE" >> dockerimage.env
    - echo current FINAL_REGISTRY_IMAGE ${FINAL_REGISTRY_IMAGE}
    - echo $WORKSPACE
    - |
      if [[ "$WORKSPACE" != "" ]]; then
        cd $WORKSPACE
      fi
    - docker build --pull -f $DOCKER_FILE -t "$FINAL_REGISTRY_IMAGE" -t "$REGISTRY_IMAGE_BASE" --cache-from "$REGISTRY_IMAGE_BASE"  --build-arg BUILDKIT_INLINE_CACHE=1 .
    - docker push "$REGISTRY_IMAGE_BASE" --all-tags
  artifacts:
    reports:
      dotenv: dockerimage.env

#-----------------------------------------------------------------------------------------------------------------------
docker-build-db-init:
#-----------------------------------------------------------------------------------------------------------------------
  stage: dockerimage
  extends: .docker-build-script
  variables:
    IMG_NAME: "elogbook-db-init"
    DOCKER_FILE: "Dockerfile"
    WORKSPACE: "./db/postgreSQL"
  needs: []


#-----------------------------------------------------------------------------------------------------------------------
# ...
#-----------------------------------------------------------------------------------------------------------------------
staging-external:
  stage: dockerimage-ext-job
  trigger: openkonsequenz/qa/elogbook.build.job
  variables:
    ARTIFACTS_DOWNLOAD_REF: $CI_COMMIT_BRANCH
    CI_COMMIT_SHORT_SHA_CHILD: $CI_COMMIT_SHORT_SHA
  except:
    - tags
