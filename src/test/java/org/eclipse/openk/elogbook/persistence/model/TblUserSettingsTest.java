/**
******************************************************************************
* Copyright © 2017-2018 PTA GmbH.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* 
*     http://www.eclipse.org/legal/epl-v10.html
* 
******************************************************************************
*/
package org.eclipse.openk.elogbook.persistence.model;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class TblUserSettingsTest {

	@Test
	public void TestGettersSetters() {
		TblUserSettings tblUserSettings = new TblUserSettings();
		tblUserSettings.setId(1);
		tblUserSettings.setValue("lari fari");
		tblUserSettings.setUsername("Bruno Haferkamp");
		tblUserSettings.setSettingType("Default");

		assertEquals((long)tblUserSettings.getId(), 1);
		assertEquals(tblUserSettings.getSettingType(), "Default");
		assertEquals(tblUserSettings.getUsername(), "Bruno Haferkamp");
		assertEquals(tblUserSettings.getValue(), "lari fari");
 	}
}
