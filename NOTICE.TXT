# Notices for Eclipse eLogbook@openk

This content is produced and maintained by the Eclipse eLogbook@openk project.

* Project home: https://projects.eclipse.org/projects/technology.elogbook

## Trademarks

Eclipse eLogbook@openk, and eLogbook@openk are trademarks of the Eclipse
Foundation.

## Copyright

All content is the property of the respective authors or their employers. For
more information regarding authorship of content, please consult the listed
source code repository logs.

## Declared Project Licenses

This program and the accompanying materials are made available under the terms
of the Eclipse Public License v. 1.0 which is available at
http://www.eclipse.org/legal/epl-v10.html.

SPDX-License-Identifier: EPL-1.0

## Source Code

The project maintains the following source code repositories:

* http://git.eclipse.org/c/elogbook/authandauth.git
* http://git.eclipse.org/c/elogbook/elogbook.git
* http://git.eclipse.org/c/elogbook/elogbookFE.git
* http://git.eclipse.org/c/elogbook/portalFE.git

## Third-party Content

This project leverages the following third party content.

angular animations (4.0.0)

* License: MIT

angular browser dynamic (4.0.0)

* License: MIT
* Source:
   https://github.com/angular/angular/tree/master/packages/platform-browser-dynamic

angular common (4.0.0)

* License: MIT
* Project: https://github.com/angular/
* Source: https://github.com/angular/angular/tree/master/packages/common

angular compiler (4.0.0)

* License: MIT
* Source: https://github.com/angular/angular/tree/master/packages/compiler

angular core (4.0.0)

* License: MIT
* Project: https://angular.io/
* Source: https://github.com/angular/angular/tree/master/packages/core

angular forms (4.0.0)

* License: MIT
* Source: https://github.com/angular/angular/tree/master/packages/forms

angular http (4.0.0)

* License: MIT
* Source: https://github.com/angular/angular/tree/master/packages/http

angular material (4.0.0)

* License: MIT
* Project: https://material.angular.io/
* Source: https://github.com/angular/material

angular platform browser (4.0.0)

* License: MIT
* Source:
   https://github.com/angular/angular/tree/master/packages/platform-browser

angular router (4.0.0)

* License: MIT
* Source: https://github.com/angular/angular/tree/master/packages/router

angular-oauth2-oidc (1.0.20)

* License: MIT
* Source:
   https://github.com/manfredsteyer/angular-oauth2-oidc/tree/master/angular-oauth2-oidc

angular2-jwt (0.2.3)

* License: MIT
* Source: https://github.com/auth0/angular2-jwt

angular2-uuid (1.1.1)

* License: MIT

Apache HttpClient (4.5.3)

* License: Apache-2.0

Apache Log4j (1.2.17)

* License: Apache License 2.0

Bootstrap (3.3.7)

* License: MIT License

classlist.js (1.1.20150312)

* License: Unlicense
* Source: https://github.com/eligrey/classList.js

commons-codec (1.11)

* License: Apache-2.0 AND BSD-3-Clause
* Source: https://mvnrepository.com/artifact/commons-codec/commons-codec/1.11

commons-io (2.5)

* License: Apache License, 2.0

core-js, (2.4.1)

* License: MIT

font-awesome (4.7.0)

* License: OFL-1.1 AND MIT

Google Collections (1.0)

* License: Apache License, 2.0

gson (2.8.0)

* License: Apache-2.0

jackson-annotations (2.5.4)

* License: Apache-2.0
* Source:
   https://mvnrepository.com/artifact/com.fasterxml.jackson.core/jackson-annotations/2.5.4

javax.servlet.servlet-api (3.0.1)

* License: (CDDL-1.1 OR GPL-2.0-only OR GPL-2.0-only WITH
   Classpath-exception-2.0) AND Apache-2.0
* Source: https://mvnrepository.com/artifact/javax.servlet/servlet-api

jawa-jwt (3.2.0)

* License: MIT

jersey-bundles-jaxrs-ri (jaxrs-ri-2.22.1.jar) (2.22.1)

* License: Common Development and Distribution License, Apache 2.0

jquery core (3.1.1)

* License: MIT

keycloak-adapter-core (3.2.0)

* License: Apache-2.0
* Project: http://www.keycloak.org/
* Source:
   https://github.com/keycloak/keycloak/tree/3.1.0.Final/adapters/oidc/adapter-core

keycloak-adapter-spi (3.2.0)

* License: Apache-2.0
* Project:
   https://mvnrepository.com/artifact/org.keycloak/keycloak-adapter-spi/3.2.0.Final

keycloak-common (3.2.0)

* License: Apache-2.0 AND LicenseRef-Public-Domain
* Source:
   https://mvnrepository.com/artifact/org.keycloak/keycloak-common/3.2.0.Final

keycloak-core (3.2.0)

* License: Apache-2.0
* Source: https://mvnrepository.com/artifact/org.keycloak/keycloak-core

keycloak-model-jpa (3.2.0)

* License: Apache-2.0
* Project:
   https://mvnrepository.com/artifact/org.keycloak/keycloak-model-jpa/3.2.0.Final

keycloak-services (3.2.0)

* License: Apache-2.0
* Source:
   https://mvnrepository.com/artifact/org.keycloak/keycloak-services/3.2.0.Final

keycloak-servlet-adapter-spi (3.2.0)

* License: Apache-2.0
* Source:
   https://mvnrepository.com/artifact/org.keycloak/keycloak-servlet-adapter-spi/3.2.0.Final

keycloak-servlet-filter-adapter (3.2.0)

* License: Apache-2.0
* Source:
   https://mvnrepository.com/artifact/org.keycloak/keycloak-servlet-filter-adapter/3.2.0.Final

mdn-polyfills (2.1.1)

* License: MIT
* Source: https://dev.eclipse.org/ipzilla/show_bug.cgi?id=15954

moment.js (2.18.1)

* License: MIT
* Source: https://github.com/moment/moment

ng2-daterangepicker (2.0.3)

* License: MIT AND Apache-2.0
* Project: https://www.npmjs.com/package/ng2-daterangepicker
* Source: https://github.com/evansmwendwa/ng2-daterangepicker

postgres.postgresql (9.1)

* License: BSD-3-Clause AND License-Ref-Public-Domain
* Project: http://jdbc.postgresql.org/

powermock-module-junit4-common (1.6.6)

* License: Apache-2.0
* Source:
   https://mvnrepository.com/artifact/org.powermock/powermock-module-junit4-common/1.6.6

rxjs, (5.1.0)

* License: Apache-2.0

types/jquery (2.0.41)

* License: MIT

web-animations (2.2.2)

* License: Apache-2.0 AND BSD-3-Clause AND MIT
* Source: https://github.com/web-animations/web-animations-js

zone.js (0.8.4)

* License: MIT
* Project: https://github.com/angular/zone.js
* Source: https://github.com/angular/zone.js

## Cryptography

Content may contain encryption software. The country in which you are currently
may have restrictions on the import, possession, and use, and/or re-export to
another country, of encryption software. BEFORE using any encryption software,
please check the country's laws, regulations and policies concerning the import,
possession, or use, and re-export of encryption software, to see if this is
permitted.