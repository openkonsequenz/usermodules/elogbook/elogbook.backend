/**
******************************************************************************
* Copyright © 2017-2018 PTA GmbH.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* 
*     http://www.eclipse.org/legal/epl-v10.html
* 
******************************************************************************
*/
package org.eclipse.openk.elogbook.viewmodel;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.openk.elogbook.common.NotificationStatus;

/**
 * The global search criteria model.
 */

public class GlobalSearchFilter {
	/** The string to search for. */
	private String searchString;

	/** The responsibility forwarding. */
	private String responsibilityForwarding;

	/** indicate whether to find notifications with status open */
	private Boolean statusOpenSelection;

	/** indicate whether to find notifications with status in work */
	private Boolean statusInWorkSelection;

	/** indicate whether to find notifications with status done */
	private Boolean statusDoneSelection;

	/** indicate whether to find notifications with status closed */
	private Boolean statusClosedSelection;

	/** key referring branch */
	private Integer fkRefBranch;

	/** key referring grid territory */
	private Integer fkRefGridTerritory;

	/** key referring priority */
	private Integer fkRefNotificationPriority;

	/** indicating if fast/classic search */
	private Boolean fastSearchSelected;

	/**************************************************************************
	 * Public methods
	 **************************************************************************/

	/**
	 * Obtain a list of the selected status ids.
	 * 
	 * @return an array list of Integers representing the selected status ids.
	 */
	public List<Integer> getSelectedStatusMappedToIds() {
		List<Integer> selectedStati = new ArrayList<>();
		if (isStatusOpenSelection()) {
			selectedStati.add(NotificationStatus.OPEN.id);
		}
		if (isStatusInWorkSelection()) {
			selectedStati.add(NotificationStatus.INPROGRESS.id);
		}
		if (isStatusDoneSelection()) {
			selectedStati.add(NotificationStatus.FINISHED.id);
		}
		if (isStatusClosedSelection()) {
			selectedStati.add(NotificationStatus.CLOSED.id);
		}
		return selectedStati;
	}

	public boolean isBranchSelected() {
		return fkRefBranch != null && fkRefBranch > 0;
	}

	public boolean isGridTerritorySelected() {
		return fkRefGridTerritory != null && fkRefGridTerritory > 0;
	}

	public boolean isPrioritySelected() {
		return fkRefNotificationPriority != null && fkRefNotificationPriority > 0;
	}

	/**************************************************************************
	 * Getters/Setters
	 **************************************************************************/

	public String getSearchString() {
		return searchString;
	}

	public void setSearchString(String searchString) {
		this.searchString = searchString;
	}

	public String getResponsibilityForwarding() {
		return responsibilityForwarding;
	}

	public void setResponsibilityForwarding(String responsibilityForwarding) {
		this.responsibilityForwarding = responsibilityForwarding;
	}

	public Boolean isStatusOpenSelection() {
		if (statusOpenSelection == null) {
			return false;
		}
		return statusOpenSelection;
	}

	public void setStatusOpenSelection(Boolean statusOpenSelection) {
		this.statusOpenSelection = statusOpenSelection;
	}

	public Boolean isStatusInWorkSelection() {
		if (statusInWorkSelection == null) {
			return false;
		}
		return statusInWorkSelection;
	}

	public void setStatusInWorkSelection(Boolean statusInWorkSelection) {
		this.statusInWorkSelection = statusInWorkSelection;
	}

	public Boolean isStatusDoneSelection() {
		if (statusDoneSelection == null) {
			return false;
		}
		return statusDoneSelection;
	}

	public void setStatusDoneSelection(Boolean statusDoneSelection) {
		this.statusDoneSelection = statusDoneSelection;
	}

	public Boolean isStatusClosedSelection() {
		if (statusClosedSelection == null) {
			return false;
		}
		return statusClosedSelection;
	}

	public void setStatusClosedSelection(Boolean statusClosedSelection) {
		this.statusClosedSelection = statusClosedSelection;
	}

	public Integer getFkRefBranch() {
		return fkRefBranch;
	}

	public void setFkRefBranch(Integer fkRefBranch) {
		this.fkRefBranch = fkRefBranch;
	}

	public Integer getFkRefGridTerritory() {
		return fkRefGridTerritory;
	}

	public void setFkRefGridTerritory(Integer fkRefGridTerritory) {
		this.fkRefGridTerritory = fkRefGridTerritory;
	}

	public Boolean isFastSearchSelected() {
		if (fastSearchSelected == null) {
			return false;
		}
		return fastSearchSelected;
	}

	public Integer getFkRefNotificationPriority() {
		return fkRefNotificationPriority;
	}

	public void setFkRefNotificationPriority(Integer fkRefNotificationPriority) {
		this.fkRefNotificationPriority = fkRefNotificationPriority;
	}

	public void setFastSearchSelected(Boolean fastSearchSelected) {
		this.fastSearchSelected = fastSearchSelected;
	}
}
