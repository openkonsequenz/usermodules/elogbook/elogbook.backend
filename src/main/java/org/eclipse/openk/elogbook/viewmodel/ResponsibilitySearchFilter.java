/**
******************************************************************************
* Copyright © 2017-2018 PTA GmbH.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* 
*     http://www.eclipse.org/legal/epl-v10.html
* 
******************************************************************************
*/
package org.eclipse.openk.elogbook.viewmodel;

import java.util.Date;

/**
 * ResponsibilitySearchFilter - Define criteria to limit the responsibilities
 * from the past.
 */
public class ResponsibilitySearchFilter {

	/**
	 * Filtering historical responsibilities by period (only deliver
	 * notifications starting at or after transferDateFrom).
	 */
	private Date transferDateFrom;

	/**
	 * Filtering historical responsibilities by period (only deliver
	 * notifications ending before or at transferDateTo).
	 */
	private Date transferDateTo;
	
	
	/** *************************************************************************
	 * Getters/Setters
	 ****************************************************************************/

	public Date getTransferDateFrom() {
		return transferDateFrom;
	}

	public void setTransferDateFrom(Date dateFrom) {
		this.transferDateFrom = dateFrom;
	}

	public Date getTransferDateTo() {
		return transferDateTo;
	}

	public void setTransferDateTo(Date dateTo) {
		this.transferDateTo = dateTo;
	}

}
