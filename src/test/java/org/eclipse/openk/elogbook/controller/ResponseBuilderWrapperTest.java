/**
******************************************************************************
* Copyright © 2017-2018 PTA GmbH.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* 
*     http://www.eclipse.org/legal/epl-v10.html
* 
******************************************************************************
*/
package org.eclipse.openk.elogbook.controller;


import org.apache.http.HttpStatus;
import org.eclipse.openk.elogbook.exceptions.BtbException;
import org.eclipse.openk.elogbook.exceptions.BtbInternalServerError;
import org.junit.Test;

import javax.ws.rs.core.Response;

import static org.junit.Assert.assertEquals;


public class ResponseBuilderWrapperTest {
    @Test
    public void testGetResponseBuilder() throws BtbInternalServerError {
        String json = "{ 'ret' : 'OK' }";
        Response.ResponseBuilder rb = ResponseBuilderWrapper.INSTANCE.getResponseBuilder( json );
        Response resp = rb.build();
        assertEquals(resp.getStatus(), HttpStatus.SC_OK );
    }

    @Test
    public void testBuildOkResponse() throws BtbException {
        String json = "{ 'test' : 'Value' }";
        Response resp = ResponseBuilderWrapper.INSTANCE.buildOKResponse( json );
        assertEquals( resp.getStatus(), HttpStatus.SC_OK );

        resp = ResponseBuilderWrapper.INSTANCE.buildOKResponse(json, "ssess");
        assertEquals( resp.getStatus(), HttpStatus.SC_OK);
    }
/*
    @Test( expected = BtbInternalServerError.class)
    public void testBuildOkResponse1_withException() {

        Response.ResponseBuilder mockedResponseBuilder = PowerMock.createNiceMock(Response.ResponseBuilder.class);
        expect( mockedResponseBuilder.build()).andThrow( new UnsupportedEncodingException());
        PowerMock.replay(mockedResponseBuilder);
        PowerMock.mockStatic(BaseWebService.class, "getResponseBuilder", );


    }*/
}
