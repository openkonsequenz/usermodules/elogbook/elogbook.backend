#!/bin/bash
set -e

psql -v ON_ERROR_STOP=1 --username "btbservice" --dbname "$APP_DBNAME" <<-EOSQL

ALTER TABLE tbl_notification add responsibility_forwarding_uuid character varying(50);

DROP VIEW public.VIEW_ACTIVE_NOTIFICATION;
CREATE VIEW public.VIEW_ACTIVE_NOTIFICATION AS
 SELECT s.id,
    s.incident_id,
    s.version,
    s.fk_ref_branch,
    s.fk_ref_notification_priority,
    s.notification_text,
    s.free_text,
    s.free_text_extended,
    s.fk_ref_notification_status,
    s.responsibility_forwarding,
    s.responsibility_forwarding_uuid,
    s.reminder_date,
    s.expected_finished_date,
    s.responsibility_control_point,
    s.begin_date,
    s.finished_date,
    s.create_user,
    s.create_date,
    s.mod_user,
    s.mod_date,
    s.fk_ref_grid_territory,
    s.admin_flag,
    s.type
   FROM ( SELECT tbl_notification.id,
            tbl_notification.incident_id,
            tbl_notification.version,
            tbl_notification.fk_ref_branch,
            tbl_notification.fk_ref_notification_priority,
            tbl_notification.notification_text,
            tbl_notification.free_text,
            tbl_notification.free_text_extended,
            tbl_notification.fk_ref_notification_status,
            tbl_notification.responsibility_forwarding,
            tbl_notification.responsibility_forwarding_uuid,
            tbl_notification.reminder_date,
            tbl_notification.expected_finished_date,
            tbl_notification.responsibility_control_point,
            tbl_notification.begin_date,
            tbl_notification.finished_date,
            tbl_notification.create_user,
            tbl_notification.create_date,
            tbl_notification.mod_user,
            tbl_notification.mod_date,
            tbl_notification.fk_ref_grid_territory,
            tbl_notification.admin_flag,
            tbl_notification.type,
            rank() OVER (PARTITION BY tbl_notification.incident_id ORDER BY tbl_notification.version DESC) AS rank
           FROM TBL_NOTIFICATION) s
  WHERE s.rank = 1;

ALTER TABLE public.VIEW_ACTIVE_NOTIFICATION
  OWNER TO btbservice;
GRANT ALL ON TABLE public.VIEW_ACTIVE_NOTIFICATION TO btbservice;

DELETE FROM REF_VERSION;
INSERT INTO REF_VERSION VALUES (1, '2.0.1_PG');

EOSQL
