/**
******************************************************************************
* Copyright © 2017-2018 PTA GmbH.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* 
*     http://www.eclipse.org/legal/epl-v10.html
* 
******************************************************************************
*/
package org.eclipse.openk.elogbook.common.mapper;

import org.eclipse.openk.elogbook.persistence.dao.RefBranchDao;
import org.eclipse.openk.elogbook.persistence.dao.RefGridTerritoryDao;
import org.eclipse.openk.elogbook.persistence.dao.RefNotificationPriorityDao;
import org.eclipse.openk.elogbook.persistence.dao.RefNotificationStatusDao;
import org.eclipse.openk.elogbook.persistence.model.AbstractNotification;
import org.eclipse.openk.elogbook.persistence.model.RefBranch;
import org.eclipse.openk.elogbook.persistence.model.RefGridTerritory;
import org.eclipse.openk.elogbook.persistence.model.RefNotificationPriority;
import org.eclipse.openk.elogbook.persistence.model.RefNotificationStatus;
import org.eclipse.openk.elogbook.persistence.model.TblNotification;
import org.eclipse.openk.elogbook.viewmodel.Notification;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

public class NotificationMapper {
    private Map<Integer, RefNotificationStatus> refStatusMap;
    private Map<Integer, RefNotificationPriority> refPriorityMap;
    private Map<Integer, RefBranch> refBranchMap;
    private Map<Integer, RefGridTerritory> refGridTerritoryMap;

	public NotificationMapper(RefNotificationStatusDao rnsDao, RefBranchDao rbDao,
			RefGridTerritoryDao refGridTerritoryDao, RefNotificationPriorityDao rnpDao) {
		createRelationsMaps(rnsDao, rbDao, refGridTerritoryDao, rnpDao);
	}

    public TblNotification mapFromVModel(Notification vmNot) {
        TblNotification trg = new TblNotification();
        trg.setId(vmNot.getId());
        trg.setIncidentId(vmNot.getIncidentId());
        trg.setVersion(vmNot.getVersion());
        if (vmNot.getCreateDate() != null) {
            trg.setCreateDate(new Timestamp(vmNot.getCreateDate().getTime()));
        }
        if (vmNot.getBeginDate() != null) {
        	trg.setBeginDate(new Timestamp(vmNot.getBeginDate().getTime()));
        }
        trg.setCreateUser(vmNot.getCreateUser());
        if (vmNot.getExpectedFinishDate() != null) {
            trg.setExpectedFinishedDate(new Timestamp(vmNot.getExpectedFinishDate().getTime()));
        }
        if (vmNot.getFinishedDate() != null) {
            trg.setFinishedDate(new Timestamp(vmNot.getFinishedDate().getTime()));
        }
        trg.setFreeText(vmNot.getFreeText());
        trg.setFreeTextExtended(vmNot.getFreeTextExtended());
        if (vmNot.getModDate() != null) {
            trg.setModDate(new Timestamp(vmNot.getModDate().getTime()));
        }
        trg.setModUser(vmNot.getModUser());
        trg.setNotificationText(vmNot.getNotificationText());
        if (vmNot.getReminderDate() != null) {
            trg.setReminderDate(new Timestamp(vmNot.getReminderDate().getTime()));
        }
        trg.setResponsibilityControlPoint(vmNot.getResponsibilityControlPoint());
        trg.setResponsibilityForwarding(vmNot.getResponsibilityForwarding());
        trg.setRefBranch(refBranchMap.get(vmNot.getFkRefBranch()));
        trg.setRefNotificationStatus(refStatusMap.get(vmNot.getFkRefNotificationStatus()));
        trg.setRefNotificationPriority(refPriorityMap.get(vmNot.getFkRefNotificationPriority()));
        trg.setRefGridTerritory(refGridTerritoryMap.get(vmNot.getFkRefGridTerritory()));
        trg.setAdminFlag(vmNot.isAdminFlag());
        trg.setType(vmNot.getType());
        trg.setResponsibilityForwardingUuid(vmNot.getResponsibilityForwardingUuid() != null ?
                                                vmNot.getResponsibilityForwardingUuid().toString() : null);
        return trg;
    }

    public Notification mapToVModel(AbstractNotification tblNot) {
    	if (tblNot == null) {
    		return null;
    	}
        Notification not = new Notification();
        not.setId(tblNot.getId());
        not.setIncidentId(tblNot.getIncidentId());
        not.setVersion(tblNot.getVersion());
        if (tblNot.getBeginDate() != null) {
        	not.setBeginDate(new Date(tblNot.getBeginDate().getTime()));
        }
        if (tblNot.getCreateDate() != null) {
            not.setCreateDate(new Date(tblNot.getCreateDate().getTime()));
        }
        not.setCreateUser(tblNot.getCreateUser());
        if (tblNot.getExpectedFinishedDate() != null) {
            not.setExpectedFinishDate(new Date(tblNot.getExpectedFinishedDate().getTime()));
        }
        if (tblNot.getFinishedDate() != null) {
            not.setFinishedDate(new Date(tblNot.getFinishedDate().getTime()));
        }
        not.setFreeText(tblNot.getFreeText());
        not.setFreeTextExtended(tblNot.getFreeTextExtended());
        if (tblNot.getModDate() != null) {
            not.setModDate(new Date(tblNot.getModDate().getTime()));
        }
        not.setModUser(tblNot.getModUser());
        not.setNotificationText(tblNot.getNotificationText());
        if (tblNot.getReminderDate() != null) {
            not.setReminderDate(new Date(tblNot.getReminderDate().getTime()));
        }
		not.setResponsibilityControlPoint(tblNot.getResponsibilityControlPoint());
		not.setResponsibilityForwarding(tblNot.getResponsibilityForwarding());
		if (tblNot.getRefBranch() != null) {
			not.setFkRefBranch(tblNot.getRefBranch().getId());
		}
		not.setFkRefNotificationStatus(tblNot.getRefNotificationStatus().getId());
		not.setStatus(tblNot.getRefNotificationStatus().getName());
		if (tblNot.getRefGridTerritory() != null) {
			not.setFkRefGridTerritory(tblNot.getRefGridTerritory().getId());
		}
		if (tblNot.getRefNotificationPriority() != null) {
            not.setFkRefNotificationPriority(tblNot.getRefNotificationPriority().getId());
        }
		not.getAdminFlag(tblNot.isAdminFlag());
		not.setType(tblNot.getType());
		not.setResponsibilityForwardingUuid(tblNot.getResponsibilityForwardingUuid() != null ?
                                        UUID.fromString(tblNot.getResponsibilityForwardingUuid()) : null);
		return not;
    }

    public List<Notification> mapListToVModel(List<? extends AbstractNotification> tnotlist) {
        List<Notification> retList = new ArrayList<>(tnotlist.size());
        for (AbstractNotification tnot : tnotlist) {
            retList.add(mapToVModel(tnot));
        }
        return retList;
    }
  

	/**
	 * Find the related branches, notification status and grid territories and put their ids into an internal map.
	 * @param rnsDao the dao for the notification status
	 * @param rbDao the dao for the branches
	 * @param refGridTerritoryDao the dao for the grid territories
	 */
	private void createRelationsMaps(RefNotificationStatusDao rnsDao, RefBranchDao rbDao,
			RefGridTerritoryDao refGridTerritoryDao, RefNotificationPriorityDao rnpDao) {
		List<RefNotificationStatus> rnsList = rnsDao.findInTx(true, -1, 0);
		refStatusMap = new HashMap<>(rnsList.size());
		for (RefNotificationStatus item : rnsList) {
			refStatusMap.put(item.getId(), item);
		}
        List<RefNotificationPriority> rnpList = rnpDao.findInTx(true, -1, 0);
        refPriorityMap = new HashMap<>(rnpList.size());
        for (RefNotificationPriority item : rnpList) {
            refPriorityMap.put(item.getId(), item);
        }
		List<RefBranch> rbList = rbDao.findInTx(true, -1, 0);
		refBranchMap = new HashMap<>(rbList.size());
		for (RefBranch item : rbList) {
			refBranchMap.put(item.getId(), item);
		}
		List<RefGridTerritory> refGridTerritories = refGridTerritoryDao.findInTx(true, -1, 0);
		refGridTerritoryMap = new HashMap<>(refGridTerritories.size());
		for (RefGridTerritory refGridTerritory : refGridTerritories) {
			refGridTerritoryMap.put(refGridTerritory.getId(), refGridTerritory);
		}
	}
}
