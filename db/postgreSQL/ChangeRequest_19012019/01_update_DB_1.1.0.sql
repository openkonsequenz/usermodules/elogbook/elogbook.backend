INSERT INTO REF_BRANCH ("id", "name", "description" ) VALUES ( 5, 'Z', 'ZSM' );

INSERT INTO REF_GRID_TERRITORY ("id", "name", "description", "fk_ref_master") VALUES ( 3, 'DR', 'Dreieich', 3);

INSERT INTO TBL_RESPONSIBILITY ("fk_ref_grid_territory", "fk_ref_branch", "responsible_user", "create_user", "create_date") VALUES (3, 1, 'admin','admin', CURRENT_TIMESTAMP);
INSERT INTO TBL_RESPONSIBILITY ("fk_ref_grid_territory", "fk_ref_branch", "responsible_user", "create_user", "create_date") VALUES (3, 2, 'admin','admin', CURRENT_TIMESTAMP);
INSERT INTO TBL_RESPONSIBILITY ("fk_ref_grid_territory", "fk_ref_branch", "responsible_user", "create_user", "create_date") VALUES (3, 3, 'admin','admin', CURRENT_TIMESTAMP);
INSERT INTO TBL_RESPONSIBILITY ("fk_ref_grid_territory", "fk_ref_branch", "responsible_user", "create_user", "create_date") VALUES (3, 4, 'admin','admin', CURRENT_TIMESTAMP);
INSERT INTO TBL_RESPONSIBILITY ("fk_ref_grid_territory", "fk_ref_branch", "responsible_user", "create_user", "create_date") VALUES (3, 5, 'admin','admin', CURRENT_TIMESTAMP);

INSERT INTO TBL_RESPONSIBILITY ("fk_ref_grid_territory", "fk_ref_branch", "responsible_user", "create_user", "create_date") VALUES (1, 5, 'admin','admin', CURRENT_TIMESTAMP);
INSERT INTO TBL_RESPONSIBILITY ("fk_ref_grid_territory", "fk_ref_branch", "responsible_user", "create_user", "create_date") VALUES (2, 5, 'admin','admin', CURRENT_TIMESTAMP);

UPDATE REF_VERSION SET version='1.1.0_PG' WHERE id=1;


