/**
******************************************************************************
* Copyright © 2017-2018 PTA GmbH.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* 
*     http://www.eclipse.org/legal/epl-v10.html
* 
******************************************************************************
*/
package org.eclipse.openk.elogbook.viewmodel;

import java.util.List;

public class TerritoryResponsibility {

  private String gridTerritoryDescription;
  private List<Responsibility> responsibilityList;

  public String getGridTerritoryDescription() {
    return gridTerritoryDescription;
  }

  public void setGridTerritoryDescription(String gridTerritoryDescription) {
    this.gridTerritoryDescription = gridTerritoryDescription;
  }

  public List<Responsibility> getResponsibilityList() {
    return responsibilityList;
  }

  public void setResponsibilityList(List<Responsibility> responsibilityList) {
    this.responsibilityList = responsibilityList;
  }
}
