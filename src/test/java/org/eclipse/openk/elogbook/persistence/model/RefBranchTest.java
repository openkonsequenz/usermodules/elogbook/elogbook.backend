/**
******************************************************************************
* Copyright © 2017-2018 PTA GmbH.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* 
*     http://www.eclipse.org/legal/epl-v10.html
* 
******************************************************************************
*/
package org.eclipse.openk.elogbook.persistence.model;

import static org.junit.Assert.assertEquals;

import org.eclipse.openk.elogbook.common.JsonGeneratorBase;
import org.eclipse.openk.elogbook.common.util.ResourceLoaderBase;
import org.junit.Test;

public class RefBranchTest extends ResourceLoaderBase {
	// IMPORTANT TEST!!!
	// Make sure, our Interface produces a DEFINED Json!
	// Changes in the interface will HOPEFULLY crash here!!!

	@Test
	public void TestStructureAgainstJson() {
		String json = super.loadStringFromResource("testRefBranch.json");
		RefBranch br = JsonGeneratorBase.getGson().fromJson(json, RefBranch.class);

		assertEquals((int) br.getId(), 3);
		assertEquals(br.getName(), "BD1");
		assertEquals(br.getDescription(), "branch description");
	}

	@Test
	public void TestSetters() {
		RefBranch branch = new RefBranch();
		branch.setId(1);
		branch.setDescription("desc_");
		branch.setName("bla bla");

		assertEquals((long) branch.getId(), 1);
		assertEquals(branch.getName(),"bla bla");
		assertEquals(branch.getDescription(),"desc_");

	}
}
