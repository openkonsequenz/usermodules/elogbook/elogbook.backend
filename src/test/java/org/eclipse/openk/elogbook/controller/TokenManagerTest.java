/**
******************************************************************************
* Copyright © 2017-2018 PTA GmbH.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* 
*     http://www.eclipse.org/legal/epl-v10.html
* 
******************************************************************************
*/
package org.eclipse.openk.elogbook.controller;

import org.eclipse.openk.elogbook.controller.BaseWebService.SecureType;
import org.eclipse.openk.elogbook.exceptions.BtbException;
import org.eclipse.openk.elogbook.exceptions.BtbForbidden;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

public class TokenManagerTest {
	private TokenManager tokenManager;
	private String payload = "eyJhbGciOiJSUzI1NiIsInR5cCIgOiAiSldUIiwia2lkIiA6ICJodVl0eVByUEVLQ1phY3FfMW5sOGZscENETnFHdmZEZHctYUxGQXNoWHZVIn0.eyJqdGkiOiI5MDVjMDYzMy03MzlkLTQ2MjYtYWFiZi0xMWNmZDc0Mzg2ZTQiLCJleHAiOjE1MDY1MDkwOTIsIm5iZiI6MCwiaWF0IjoxNTA2NTA4NzkyLCJpc3MiOiJodHRwOi8vZW50amF2YTAwMjo4MDgwL2F1dGgvcmVhbG1zL2Vsb2dib29rIiwiYXVkIjoiZWxvZ2Jvb2stYmFja2VuZCIsInN1YiI6ImMyZTlkN2FlLTJiZmEtNDU3OC1iMDllLWY1ZGM1ZjA5YTg3OSIsInR5cCI6IkJlYXJlciIsImF6cCI6ImVsb2dib29rLWJhY2tlbmQiLCJhdXRoX3RpbWUiOjAsInNlc3Npb25fc3RhdGUiOiI1M2Q2YjdlNi04NDgyLTRjNzEtYTBlYi05ZDUyYzgzNjg4ZDAiLCJhY3IiOiIxIiwiYWxsb3dlZC1vcmlnaW5zIjpbIioiXSwicmVhbG1fYWNjZXNzIjp7InJvbGVzIjpbImVsb2dib29rLW5vcm1hbHVzZXIiLCJ1bWFfYXV0aG9yaXphdGlvbiJdfSwicmVzb3VyY2VfYWNjZXNzIjp7ImFjY291bnQiOnsicm9sZXMiOlsibWFuYWdlLWFjY291bnQiLCJtYW5hZ2UtYWNjb3VudC1saW5rcyIsInZpZXctcHJvZmlsZSJdfX0sIm5hbWUiOiJPdHRvIE5vcm1hbHZlcmJyYXVjaGVyIiwicHJlZmVycmVkX3VzZXJuYW1lIjoib3R0byIsImdpdmVuX25hbWUiOiJPdHRvIiwiZmFtaWx5X25hbWUiOiJOb3JtYWx2ZXJicmF1Y2hlciIsInJvbGVzdGVzdCI6IltvZmZsaW5lX2FjY2VzcywgdW1hX2F1dGhvcml6YXRpb24sIGVsb2dib29rLW5vcm1hbHVzZXJdIn0.yTVAVWqm8RuENrDEnrYRJNT2CbeqQ19qMD2vLP0yhyR5IQ_IVxSPLYKwpo5mbQFJGadbPhQxdr5Vv7kflr1ciIbuV2lSGb2cT3NawdFuPQbrbXTCIVig5oo7q7GzI_pTLXw9J2eQ-AHNpuo-uui0khqHKHIlx9TT8HvbwTe6DZDC3HOFnQOcyaTjtO_F46AfLTmwQ1J91J-ng_A6KzbBNS5LBbY2NzluP1B8290M5nOjLGASjCEJ8BEpo37LWScxPwdxXr7i9JlKVqxRvGeetEm1fMtZBvIrY8n9X7K2VWItnUdSGcTwEwj4iCGEev8okj22vKjKtIkRGscKgyQRWw";

	@Before
	public void init() {
		this.tokenManager = TokenManager.getInstance();
	}

	@Ignore
	@Test(expected = BtbException.class)
	public void testLogout() throws BtbException {
		tokenManager.logout("bla");
	}

	@Ignore
	@Test(expected = BtbException.class)
	public void testCheckAuth() throws BtbException {
		tokenManager.checkAut("bla");
	}

	@Test
	public void testSecureTypeNormal() throws BtbException {
		tokenManager.checkAutLevel(payload, SecureType.NORMAL);
	}

	@Test(expected = BtbForbidden.class)
	public void testSecureTypeHigh() throws BtbException {
		tokenManager.checkAutLevel(payload, SecureType.HIGH);
	}
}
