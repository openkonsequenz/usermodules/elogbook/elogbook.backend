/*
 *******************************************************************************
 * Copyright (c) 2019 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
 */
package org.eclipse.openk.elogbook.viewmodel.contactbasedata;

import java.util.UUID;

public class VwDetailedContact {

    private Long fkContactId;
    private Long id;
    private UUID uuid;
    private String name;
    private String contactType;
    private String companyName;
    private String companyType;
    private UUID companyId;
    private UUID salutationUuid;
    private UUID personTypeUuid;
    private String firstName;
    private String lastName;
    private String department;
    private String note;
    private String salutationType;
    private String personType;
    private String mainAddress;
    private String email;
    private String searchfield;
    private Boolean anonymized;

    public Long getFkContactId() {
        return fkContactId;
    }

    public void setFkContactId(Long fkContactId) {
        this.fkContactId = fkContactId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public UUID getUuid() {
        return uuid;
    }

    public void setUuid(UUID uuid) {
        this.uuid = uuid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getContactType() {
        return contactType;
    }

    public void setContactType(String contactType) {
        this.contactType = contactType;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getCompanyType() {
        return companyType;
    }

    public void setCompanyType(String companyType) {
        this.companyType = companyType;
    }

    public UUID getCompanyId() {
        return companyId;
    }

    public void setCompanyId(UUID companyId) {
        this.companyId = companyId;
    }

    public UUID getSalutationUuid() {
        return salutationUuid;
    }

    public void setSalutationUuid(UUID salutationUuid) {
        this.salutationUuid = salutationUuid;
    }

    public UUID getPersonTypeUuid() {
        return personTypeUuid;
    }

    public void setPersonTypeUuid(UUID personTypeUuid) {
        this.personTypeUuid = personTypeUuid;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getSalutationType() {
        return salutationType;
    }

    public void setSalutationType(String salutationType) {
        this.salutationType = salutationType;
    }

    public String getPersonType() {
        return personType;
    }

    public void setPersonType(String personType) {
        this.personType = personType;
    }

    public String getMainAddress() {
        return mainAddress;
    }

    public void setMainAddress(String mainAddress) {
        this.mainAddress = mainAddress;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getSearchfield() {
        return searchfield;
    }

    public void setSearchfield(String searchfield) {
        this.searchfield = searchfield;
    }

    public Boolean getAnonymized() {
        return anonymized;
    }

    public void setAnonymized(Boolean anonymized) {
        this.anonymized = anonymized;
    }
}
