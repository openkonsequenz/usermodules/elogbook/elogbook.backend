/**
******************************************************************************
* Copyright © 2017-2018 PTA GmbH.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* 
*     http://www.eclipse.org/legal/epl-v10.html
* 
******************************************************************************
*/
package org.eclipse.openk.elogbook.persistence.model;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class RefGridTerritoryTest {

	@Test
	public void TestGettersSetters() {
	  RefGridTerritory refGridTerritory = new RefGridTerritory();
    refGridTerritory.setId(1);
    refGridTerritory.setDescription("desc_");
    refGridTerritory.setName("bla bla");

    assertEquals((long) refGridTerritory.getId(), 1);
    assertEquals(refGridTerritory.getName(),"bla bla");
    assertEquals(refGridTerritory.getDescription(),"desc_");

	}

	@Test
	public void TestRefMaster() {
	RefGridTerritory refGridTerritoryParent = new RefGridTerritory();
	refGridTerritoryParent.setId(1);
	refGridTerritoryParent.setName("Test");
	refGridTerritoryParent.setFkRefMaster(2);

	RefGridTerritory self = new RefGridTerritory();
	self.setId(2);
	refGridTerritoryParent.setRefMaster(self);

    assertEquals(refGridTerritoryParent.getRefMaster().getId(), self.getId());
 	}
}
