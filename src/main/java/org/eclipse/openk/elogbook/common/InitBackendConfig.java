/**
******************************************************************************
* Copyright © 2017-2018 PTA GmbH.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* 
*     http://www.eclipse.org/legal/epl-v10.html
* 
******************************************************************************
*/
package org.eclipse.openk.elogbook.common;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;

public class InitBackendConfig extends HttpServlet {

    private static final long serialVersionUID = -7882117179312471533L;

    private static final Logger LOGGER = LogManager.getLogger(InitBackendConfig.class.getName());
    private String backendConfigFile;

    @Override
    public void init() throws ServletException {
        String environment = getServletContext().getInitParameter("OK_ELOGBOOK_ENVIRONMENT");
        setConfigFiles(environment);
    }

    private void setConfigFiles(String environment ) {
        String env = (environment == null ? "Production": environment);

        switch (env){
            case "ExternalConfig":
                loadExternalConfig();
                break;
            case "DevLocal":
                backendConfigFile="backendConfigDevLocal.json";
                break;
            case "DevServer":
                backendConfigFile="backendConfigDevServer.json";
                break;
            case "Custom":
                backendConfigFile= "_backendConfigCustom.json";
                break;
            default:
                backendConfigFile="backendConfigProduction.json";
        }

        BackendConfig.setConfigFileName(backendConfigFile);
        LOGGER.info("Portal backendendenviroment is: " +environment+ ". Setting backendConfig accordingly to: "+ backendConfigFile);
    }

    private void loadExternalConfig() {
        Context ctx = null;
        try {
            ctx = new InitialContext();
            backendConfigFile = (String) ctx.lookup("java:comp/env/configurationPathBackend");
            LOGGER.info("backendConfigFile is: " + backendConfigFile);
        } catch (NamingException e) {
            LOGGER.error("Error while loadExternalConfig", e);
        }
    }
}
