/**
******************************************************************************
* Copyright © 2017-2018 PTA GmbH.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* 
*     http://www.eclipse.org/legal/epl-v10.html
* 
******************************************************************************
*/
package org.eclipse.openk.elogbook.controller;

import com.google.gson.reflect.TypeToken;
import org.eclipse.openk.elogbook.common.Globals;
import org.eclipse.openk.elogbook.common.JsonGeneratorBase;
import org.eclipse.openk.elogbook.exceptions.BtbException;
import org.eclipse.openk.elogbook.exceptions.BtbExceptionMapper;
import org.eclipse.openk.elogbook.persistence.model.RefBranch;
import org.eclipse.openk.elogbook.persistence.model.RefGridTerritory;
import org.eclipse.openk.elogbook.persistence.model.RefNotificationPriority;
import org.eclipse.openk.elogbook.persistence.model.RefNotificationStatus;
import org.eclipse.openk.elogbook.viewmodel.GlobalSearchFilter;
import org.eclipse.openk.elogbook.viewmodel.HistoricalShiftChanges;
import org.eclipse.openk.elogbook.viewmodel.ListTypeMapper;
import org.eclipse.openk.elogbook.viewmodel.Notification;
import org.eclipse.openk.elogbook.viewmodel.NotificationFile;
import org.eclipse.openk.elogbook.viewmodel.NotificationSearchFilter;
import org.eclipse.openk.elogbook.viewmodel.ReminderSearchFilter;
import org.eclipse.openk.elogbook.viewmodel.ResponsibilitySearchFilter;
import org.eclipse.openk.elogbook.viewmodel.TerritoryResponsibility;
import org.eclipse.openk.elogbook.viewmodel.UserAuthentication;
import org.eclipse.openk.elogbook.viewmodel.VersionInfo;
import org.eclipse.openk.elogbook.viewmodel.contactbasedata.ContactTupel;

import javax.ws.rs.core.Response;
import java.util.List;

import static org.eclipse.openk.elogbook.controller.BackendControllerResponsibility.POST_RESPONSIBILITY_MODE.FETCH;
import static org.eclipse.openk.elogbook.controller.BackendControllerResponsibility.POST_RESPONSIBILITY_MODE.PLAN;

public class ControllerImplementations {
	private ControllerImplementations() {
	}

	public static class GetVersionInfo extends BackendInvokable {

		private BackendControllerVersionInfo backendControllerVersionInfo;

		public GetVersionInfo(BackendControllerVersionInfo backendControllerVersionInfo) {
			this.backendControllerVersionInfo = backendControllerVersionInfo;
		}

		@Override
		public Response invoke() throws BtbException {
			VersionInfo vi = backendControllerVersionInfo.getVersionInfo();
			return ResponseBuilderWrapper.INSTANCE.buildOKResponse(JsonGeneratorBase.getGson().toJson(vi));
		}
	}

	public static class GetNotificationStatuses extends BackendInvokable {

		private BackendControllerRefInfo backendControllerRefInfo;

		public GetNotificationStatuses(BackendControllerRefInfo backendControllerRefInfo) {
			this.backendControllerRefInfo = backendControllerRefInfo;
		}

		@Override
		public Response invoke() throws BtbException {
			List<RefNotificationStatus> statusList = backendControllerRefInfo.getNotificationStatuses();
			return ResponseBuilderWrapper.INSTANCE.buildOKResponse(JsonGeneratorBase.getGson().toJson(statusList));
		}
	}

	public static class GetNotificationPriorities extends BackendInvokable {

		private BackendControllerRefInfo backendControllerRefInfo;

		public GetNotificationPriorities(BackendControllerRefInfo backendControllerRefInfo) {
			this.backendControllerRefInfo = backendControllerRefInfo;
		}

		@Override
		public Response invoke() throws BtbException {
			List<RefNotificationPriority> priorityList = backendControllerRefInfo.getNotificationPriorities();
			return ResponseBuilderWrapper.INSTANCE.buildOKResponse(JsonGeneratorBase.getGson().toJson(priorityList));
		}
	}

	public static class GetBranches extends BackendInvokable {
		
		private BackendControllerRefInfo backendControllerRefInfo;

		public GetBranches(BackendControllerRefInfo backendControllerRefInfo) {
			this.backendControllerRefInfo = backendControllerRefInfo;
		}
		
		@Override
		public Response invoke() throws BtbException {
			List<RefBranch> branches = backendControllerRefInfo.getBranches();
			return ResponseBuilderWrapper.INSTANCE.buildOKResponse(JsonGeneratorBase.getGson().toJson(branches));
		}
	}

	public static class GetGridTerritories extends BackendInvokable {

		private BackendControllerRefInfo backendControllerRefInfo;

		public GetGridTerritories(BackendControllerRefInfo backendControllerRefInfo) {
			this.backendControllerRefInfo = backendControllerRefInfo;
		}

		@Override
		public Response invoke() throws BtbException {
			List<RefGridTerritory> refGridTerritories = backendControllerRefInfo.getGridTerritories();
			return ResponseBuilderWrapper.INSTANCE
					.buildOKResponse(JsonGeneratorBase.getGson().toJson(refGridTerritories));
		}
	}

	public static class GetCurrentResponsibilities extends BackendInvokable {
		private BackendControllerResponsibility backendControllerResponsibility;

		public GetCurrentResponsibilities(BackendControllerResponsibility backendControllerResponsibility) {
			this.backendControllerResponsibility = backendControllerResponsibility;
		}

		@Override
		public Response invoke() throws BtbException {
			List<TerritoryResponsibility> responsibilities = backendControllerResponsibility
					.getCurrentResponsibilities(getModUser());
			return ResponseBuilderWrapper.INSTANCE
					.buildOKResponse(JsonGeneratorBase.getGson().toJson(responsibilities));
		}
	}

	public static class GetPlannedResponsibilities extends BackendInvokable {
		private BackendControllerResponsibility backendControllerResponsibility;

		public GetPlannedResponsibilities(BackendControllerResponsibility backendControllerResponsibility) {
			this.backendControllerResponsibility = backendControllerResponsibility;
		}

		@Override
		public Response invoke() throws BtbException {
			List<TerritoryResponsibility> responsibilities = backendControllerResponsibility
					.getPlannedResponsibilities(getModUser());
			return ResponseBuilderWrapper.INSTANCE
					.buildOKResponse(JsonGeneratorBase.getGson().toJson(responsibilities));
		}
	}

	public static class GetAllResponsibilities extends BackendInvokable {

		private BackendControllerResponsibility backendControllerResponsibility;

		public GetAllResponsibilities(BackendControllerResponsibility backendControllerResponsibility) {
			this.backendControllerResponsibility = backendControllerResponsibility;
		}

		@Override
		public Response invoke() throws BtbException {
			List<TerritoryResponsibility> responsibilities = backendControllerResponsibility.getAllResponsibilities();
			return ResponseBuilderWrapper.INSTANCE
					.buildOKResponse(JsonGeneratorBase.getGson().toJson(responsibilities));
		}
	}

	public static class GetHistoricalResponsibilitiesByTransactionId extends BackendInvokable {
		private String transactionId;
		private BackendControllerResponsibility backendControllerResponsibility;

		public GetHistoricalResponsibilitiesByTransactionId(String transactionId,
				BackendControllerResponsibility backendControllerResponsibility) {
			this.transactionId = transactionId;
			this.backendControllerResponsibility = backendControllerResponsibility;
		}

		@Override
		public Response invoke() throws BtbException {
			InputDataValuator.checkHistoricalResponsibilityId(transactionId);
			List<TerritoryResponsibility> territoryResponsibilities = backendControllerResponsibility
					.getHistoricalResponsibilitiesByTransactionId(Integer.valueOf(transactionId));
			return ResponseBuilderWrapper.INSTANCE
					.buildOKResponse(JsonGeneratorBase.getGson().toJson(territoryResponsibilities));
		}
	}

	public static class PostResponsibilities extends BackendInvokable {

		private String responsibilitiesJson;
		private BackendControllerResponsibility backendControllerResponsibility;
		private final boolean planOrFetchYN;

		public PostResponsibilities(String responsibilities,
									boolean planOrFetchYN,
									BackendControllerResponsibility backendControllerResponsibility
									) {
			this.responsibilitiesJson = responsibilities;
			this.backendControllerResponsibility = backendControllerResponsibility;
			this.planOrFetchYN = planOrFetchYN;
		}

		@Override
		public Response invoke() throws BtbException {
			new InputDataValuator().checkIncomingSearchFilter(responsibilitiesJson);
			List<TerritoryResponsibility> territoryResponsibilityList = JsonGeneratorBase.getGson()
					.fromJson(responsibilitiesJson, new TypeToken<List<TerritoryResponsibility>>() {
					}.getType());
			List<TerritoryResponsibility> newerResponsibilityList = backendControllerResponsibility
					.postResponsibilities(territoryResponsibilityList, getModUser(),
							planOrFetchYN ? PLAN : FETCH);
			if (newerResponsibilityList == null) {
				return ResponseBuilderWrapper.INSTANCE.buildOKResponse(BtbExceptionMapper.getGeneralOKJson());
			} else {
				return ResponseBuilderWrapper.INSTANCE
						.buildOKResponse(JsonGeneratorBase.getGson().toJson(newerResponsibilityList));
			}
		}
	}

	public static class PostResponsibilitiesConfirmation extends BackendInvokable {

		private String responsibilitiesJson;
		private BackendControllerResponsibility backendControllerResponsibility;

		public PostResponsibilitiesConfirmation(String responsibilities,
				BackendControllerResponsibility backendControllerResponsibility) {
			this.responsibilitiesJson = responsibilities;
			this.backendControllerResponsibility = backendControllerResponsibility;
		}

		@Override
		public Response invoke() throws BtbException {
			new InputDataValuator().checkIncomingSearchFilter(responsibilitiesJson);
			List<TerritoryResponsibility> territoryResponsiblityList = JsonGeneratorBase.getGson()
					.fromJson(responsibilitiesJson, new TypeToken<List<TerritoryResponsibility>>() {
					}.getType()); 
			// its unlikely but the same incident which can occur when PostPlanResponsibilities is called could arise here
			// aswell:
			// data could be outdated and has to be updated and to be sent to the FE (reload view)
			List<TerritoryResponsibility> newerResponsibilityList = backendControllerResponsibility
					.confirmResponsibilities(territoryResponsiblityList, getModUser());
			if (newerResponsibilityList == null) {
				return ResponseBuilderWrapper.INSTANCE.buildOKResponse(BtbExceptionMapper.getGeneralOKJson());
			} else {
				return ResponseBuilderWrapper.INSTANCE
						.buildOKResponse(JsonGeneratorBase.getGson().toJson(newerResponsibilityList));
			}
		}
	}

	public static class GetIsObserver extends BackendInvokable {
		private BackendControllerResponsibility backendControllerResponsibility;

		public GetIsObserver(BackendControllerResponsibility backendControllerResponsibility) {
			this.backendControllerResponsibility = backendControllerResponsibility;
		}

		@Override
		public Response invoke() throws BtbException {
			Boolean isObserver = backendControllerResponsibility.getIsObserver(getModUser());
			return ResponseBuilderWrapper.INSTANCE.buildOKResponse(JsonGeneratorBase.getGson().toJson(isObserver));
		}
	}

	public static class GetUsers extends BackendInvokable {
		BackendControllerUser backendControllerUser;
		String token;

		public GetUsers(BackendControllerUser backendControllerUser, String token) {
			this.backendControllerUser = backendControllerUser;
			this.token = token;
		}

		@Override
		public Response invoke() throws BtbException {
			List<UserAuthentication> userAuthenticationList = backendControllerUser.getUsers(token);
			return ResponseBuilderWrapper.INSTANCE
					.buildOKResponse(JsonGeneratorBase.getGson().toJson(userAuthenticationList));
		}
	}

	public static class GetNotifications extends BackendInvokable {
		private String listType;
		private String notificationSearchFilterJson;
		private BackendControllerNotification backendControllerNotification;

		public GetNotifications(String listType, String notSearchFilter,
				BackendControllerNotification backendControllerNotification) {
			this.listType = listType;
			this.notificationSearchFilterJson = notSearchFilter;
			this.backendControllerNotification = backendControllerNotification;
		}

		@Override
		public Response invoke() throws BtbException {
			new InputDataValuator().checkIncomingSearchFilter(notificationSearchFilterJson);
			NotificationSearchFilter nsf = JsonGeneratorBase.getGson().fromJson(notificationSearchFilterJson,
					NotificationSearchFilter.class);
			List<Notification> notifications = backendControllerNotification
					.getNotifications(ListTypeMapper.listTypeFromString(listType), nsf);
			return ResponseBuilderWrapper.INSTANCE.buildOKResponse(JsonGeneratorBase.getGson().toJson(notifications));
		}
	}

	/**
	 * Provide the search results for the search criteria defined in the filter for global search.
	 */
	public static class GetSearchResults extends BackendInvokable {
		private String globalSearchFilterJson;
		private BackendControllerNotification backendControllerNotification;

		/**
		 * Constructor.
		 * 
		 * @param globalSearchFilter
		 *            the {@link GlobalSearchFilter} containing the search criteria.
		 */
		public GetSearchResults(String globalSearchFilter,
				BackendControllerNotification backendControllerNotification) {
			this.globalSearchFilterJson = globalSearchFilter;
			this.backendControllerNotification = backendControllerNotification;
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see org.eclipse.openk.elogbook.controller.BaseWebService.Invokable#invoke()
		 */
		@Override
		public Response invoke() throws BtbException {
			new InputDataValuator().checkIncomingSearchFilter(globalSearchFilterJson);
			GlobalSearchFilter globalSearchFilter = JsonGeneratorBase.getGson().fromJson(globalSearchFilterJson,
					GlobalSearchFilter.class);
			List<Notification> notifications = backendControllerNotification.getSearchResults(globalSearchFilter);
			return ResponseBuilderWrapper.INSTANCE.buildOKResponse(JsonGeneratorBase.getGson().toJson(notifications));
		}
	}

	/**
	 * Store the settings for a given user
	 */
	public static class PostUserSettings extends BackendInvokable {
		private String userSettingsJson;
		private BackendControllerUser backendControllerUser;

		/**
		 * Constructor.
		 *
		 * @param userSettings
		 *            the {@link String} containing the user-settings.
		 */
		public PostUserSettings(String userSettings, BackendControllerUser backendControllerUser) {
			this.userSettingsJson = userSettings;
			this.backendControllerUser = backendControllerUser;
		}


		/*
		 * (non-Javadoc)
		 *
		 * @see org.eclipse.openk.elogbook.controller.BaseWebService.Invokable#invoke()
		 */
		@Override
		public Response invoke() throws BtbException {
			backendControllerUser.storeUserSettings( this.userSettingsJson, getModUser() );
			return ResponseBuilderWrapper.INSTANCE
					.buildOKResponse(JsonGeneratorBase.getGson().toJson(Globals.OK_STRING));
		}
	}

	/**
	 * Get the settings for a given user
	 */
	public static class GetUserSettings extends BackendInvokable {
		private BackendControllerUser backendControllerUser;

		public GetUserSettings(BackendControllerUser backendControllerUser) {
			this.backendControllerUser = backendControllerUser;
		}


		/*
		 * (non-Javadoc)
		 *
		 * @see org.eclipse.openk.elogbook.controller.BaseWebService.Invokable#invoke()
		 */
		@Override
		public Response invoke() throws BtbException {
			String resultString = backendControllerUser.getUserSettings( getModUser() );
			return ResponseBuilderWrapper.INSTANCE
					.buildOKResponse(resultString);
		}
	}

	public static class GetHistoricalShiftChangeList extends BackendInvokable {

		private String responsibilitySearchFilterJson;
		private BackendControllerResponsibility backendControllerResponsibility;

		public GetHistoricalShiftChangeList(String responsibilitySearchFilter,
				BackendControllerResponsibility backendControllerResponsibility) {
			this.responsibilitySearchFilterJson = responsibilitySearchFilter;
			this.backendControllerResponsibility = backendControllerResponsibility;
		}

		@Override
		public Response invoke() throws BtbException {
			ResponsibilitySearchFilter filter = JsonGeneratorBase.getGson().fromJson(responsibilitySearchFilterJson,
					ResponsibilitySearchFilter.class);
			HistoricalShiftChanges historicalShiftChanges = backendControllerResponsibility
					.getHistoricalShiftChanges(filter);
			return ResponseBuilderWrapper.INSTANCE
					.buildOKResponse(JsonGeneratorBase.getGson().toJson(historicalShiftChanges));
		}
	}

	public static class GetNotificationsByIncidentId extends BackendInvokable {
		private String incidentId;
		private BackendControllerNotification backendControllerNotification;

		public GetNotificationsByIncidentId(String incidentId,
				BackendControllerNotification backendControllerNotification) {
			this.incidentId = incidentId;
			this.backendControllerNotification = backendControllerNotification;
		}

		@Override
		public Response invoke() throws BtbException {
			InputDataValuator.checkNotificationId(incidentId);
			List<Notification> notifications = backendControllerNotification
					.getNotificationByIncidentId(Integer.valueOf(incidentId));
			return ResponseBuilderWrapper.INSTANCE.buildOKResponse(JsonGeneratorBase.getGson().toJson(notifications));
		}
	}

	public static class CreateNotification extends BackendInvokable {
		private String notificationJson;
		private String token;
		private BackendControllerNotification backendControllerNotification;

		public CreateNotification(String newNotification, BackendControllerNotification backendControllerNotification,
								  String token) {
			this.notificationJson = newNotification;
			this.backendControllerNotification = backendControllerNotification;
			this.token = token;
		}

		@Override
		public Response invoke() throws BtbException {
			new InputDataValuator().checkNotification(notificationJson);
			Notification notification = JsonGeneratorBase.getGson().fromJson(notificationJson, Notification.class);
			Notification persistedNotification = backendControllerNotification.createNotification(notification,
					getModUser(), token);
			return ResponseBuilderWrapper.INSTANCE
					.buildOKResponse(JsonGeneratorBase.getGson().toJson(persistedNotification));
		}
	}

	public static class GetNotificationById extends BackendInvokable {
		private String id;
		private BackendControllerNotification backendControllerNotification;

		public GetNotificationById(String id, BackendControllerNotification backendControllerNotification) {
			this.id = id;
			this.backendControllerNotification = backendControllerNotification;
		}

		@Override
		public Response invoke() throws BtbException {
			InputDataValuator.checkNotificationId(id);
			Notification persistedNotification = backendControllerNotification.getNotificationById(Integer.valueOf(id));
			return ResponseBuilderWrapper.INSTANCE
					.buildOKResponse(JsonGeneratorBase.getGson().toJson(persistedNotification));
		}
	}

	public static class GetActiveNotifications extends BackendInvokable {

		private BackendControllerNotification backendControllerNotification;

		public GetActiveNotifications(BackendControllerNotification backendControllerNotification) {
			this.backendControllerNotification = backendControllerNotification;
		}

		@Override
		public Response invoke() throws BtbException {
			List<Notification> notificationList = backendControllerNotification.getActiveNotifications();
			return ResponseBuilderWrapper.INSTANCE
					.buildOKResponse(JsonGeneratorBase.getGson().toJson(notificationList));
		}
	}

	public static class GetImportFiles extends BackendInvokable {

		private BackendControllerNotificationFile backendControllerNotificationFile;

		public GetImportFiles(BackendControllerNotificationFile backendControllerNotificationFile) {
			this.backendControllerNotificationFile = backendControllerNotificationFile;
		}

		@Override
		public Response invoke() throws BtbException {
			List<NotificationFile> notificationFileList = backendControllerNotificationFile.getNotificationsFiles("getImportFiles");
			return ResponseBuilderWrapper.INSTANCE
					.buildOKResponse(JsonGeneratorBase.getGson().toJson(notificationFileList));
		}
	}

	public static class ImportFile extends BackendInvokable {

		private BackendControllerNotificationFile backendControllerNotificationFile;

		public ImportFile(BackendControllerNotificationFile backendControllerNotificationFile) {
			this.backendControllerNotificationFile = backendControllerNotificationFile;
		}

		@Override
		public Response invoke() throws BtbException {
			List<NotificationFile> notificationFileList = backendControllerNotificationFile.getNotificationsFiles("importFile");
			return ResponseBuilderWrapper.INSTANCE
					.buildOKResponse(JsonGeneratorBase.getGson().toJson(notificationFileList));
		}
	}

	public static class DeleteImportedFiles extends BackendInvokable {

		private String fileName;
		private BackendControllerNotificationFile backendControllerNotificationFile;

		public DeleteImportedFiles(String fileName, BackendControllerNotificationFile backendControllerNotificationFile) {
			this.fileName = fileName;
			this.backendControllerNotificationFile = backendControllerNotificationFile;
		}

		@Override
		public Response invoke() throws BtbException {
			boolean deleteStatus = backendControllerNotificationFile.deleteImportedFile(fileName);
			return ResponseBuilderWrapper.INSTANCE
					.buildOKResponse(JsonGeneratorBase.getGson().toJson(deleteStatus));
		}
	}

	public static class GetCurrentReminders extends BackendInvokable {

		private String reminderSearchFilterJson;
		private BackendControllerNotification backendControllerNotification;

		public GetCurrentReminders(String reminderSearchFilter,
				BackendControllerNotification backendControllerNotification) {
			this.reminderSearchFilterJson = reminderSearchFilter;
			this.backendControllerNotification = backendControllerNotification;
		}

		@Override
		public Response invoke() throws BtbException {
			new InputDataValuator().checkIncomingSearchFilter(reminderSearchFilterJson);
			ReminderSearchFilter rsf = JsonGeneratorBase.getGson().fromJson(reminderSearchFilterJson,
					ReminderSearchFilter.class);
			List<Notification> notificationList = backendControllerNotification.getNotificationsWithReminder(rsf);
			return ResponseBuilderWrapper.INSTANCE
					.buildOKResponse(JsonGeneratorBase.getGson().toJson(notificationList));
		}
	}

	public static class GetUserSuggestions extends BackendInvokable {

		private BackendControllerUser backendControllerUser;
		private String token;

		public GetUserSuggestions(BackendControllerUser backendControllerUser, String token) {
			this.backendControllerUser = backendControllerUser;
			this.token = token;
		}

		@Override
		public Response invoke() throws BtbException {
			List<ContactTupel> userSuggestionsList = backendControllerUser.getUserSuggestionsFromContactdatabase(this.token);
			return ResponseBuilderWrapper.INSTANCE
					.buildOKResponse(JsonGeneratorBase.getGson().toJson(userSuggestionsList));
		}
	}


	public static class SynchronizeContacts extends BackendInvokable {

		private BackendControllerUser backendControllerUser;
		private String token;

		public SynchronizeContacts(BackendControllerUser backendControllerUser, String token) {
			this.backendControllerUser = backendControllerUser;
			this.token = token;
		}

		@Override
		public Response invoke() throws BtbException {
			backendControllerUser.synchronizeContacts();
			return ResponseBuilderWrapper.INSTANCE
					.buildOKResponse(JsonGeneratorBase.getGson().toJson(Globals.OK_STRING));
		}
	}
	public static class GetTestMail extends BackendInvokable {

		private BackendControllerUser backendControllerUser;

		public GetTestMail(BackendControllerUser backendControllerUser, String token) {
			this.backendControllerUser = backendControllerUser;
		}

		@Override
		public Response invoke() throws BtbException {
			backendControllerUser.sentTestMail();
			return ResponseBuilderWrapper.INSTANCE.buildOKResponse(BtbExceptionMapper.getGeneralOKJson());
		}
	}
}