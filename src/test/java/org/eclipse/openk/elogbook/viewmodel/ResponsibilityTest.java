/**
******************************************************************************
* Copyright © 2017-2018 PTA GmbH.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* 
*     http://www.eclipse.org/legal/epl-v10.html
* 
******************************************************************************
*/
package org.eclipse.openk.elogbook.viewmodel;


import static org.junit.Assert.assertTrue;

import org.eclipse.openk.elogbook.common.JsonGeneratorBase;
import org.eclipse.openk.elogbook.common.util.ResourceLoaderBase;
import org.junit.Test;

public class ResponsibilityTest extends ResourceLoaderBase {
    // IMPORTANT TEST!!!
    // Make sure, our Interface produces a DEFINED Json!
    // Changes in the interface will HOPEFULLY crash here!!!

    @Test
    public void testStructureAgainstJson() {
        String json = super.loadStringFromResource("testResponsibility.json");
        Responsibility resp = JsonGeneratorBase.getGson().fromJson(json, Responsibility.class);

        assertTrue(resp.getId() == 3);
        assertTrue(resp.getResponsibleUser().equals("currentResponsibleUser"));
        assertTrue(resp.getNewResponsibleUser().equals("newResponsibleUser"));
        assertTrue(resp.getBranchName().equals("W"));
        assertTrue(resp.isActive());
    }

    @Test
    public void testSetters() {
        Responsibility resp = new Responsibility();
        resp.setId(3);
        resp.setBranchName("S");
        resp.setIsActive(true);
    }
}
