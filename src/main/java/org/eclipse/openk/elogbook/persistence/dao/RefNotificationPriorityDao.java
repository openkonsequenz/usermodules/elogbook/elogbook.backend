/**
******************************************************************************
* Copyright © 2017-2018 PTA GmbH.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* 
*     http://www.eclipse.org/legal/epl-v10.html
* 
******************************************************************************
*/
package org.eclipse.openk.elogbook.persistence.dao;

import org.eclipse.openk.elogbook.persistence.model.RefNotificationPriority;

import javax.persistence.EntityManager;


public class RefNotificationPriorityDao extends GenericDaoJpa<RefNotificationPriority, Long> {
    public RefNotificationPriorityDao() {
        super();
    }

    public RefNotificationPriorityDao(EntityManager em) {
        super(em);
    }

}
