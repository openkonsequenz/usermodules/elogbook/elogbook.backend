/**
******************************************************************************
* Copyright © 2017-2018 PTA GmbH.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* 
*     http://www.eclipse.org/legal/epl-v10.html
* 
******************************************************************************
*/
package org.eclipse.openk.elogbook.viewmodel;

import java.util.Date;
import java.util.List;

/**
 * Define criteria whether to restrict the notifications to be delivered, e. g.
 * by time, responsibility...
 *
 */
public class ReminderSearchFilter {

	private Date reminderDate;

	/** Filter notifications depending on responsibilities. Empty list, if no responsibility */
	private List<Integer> responsibilityFilterList;

	private boolean dontFilterResponsibilities;

	/**************************************************************************
	 * Getters/Setters
	 **************************************************************************/

	public Date getReminderDate() {
		return reminderDate;
	}

	public void setReminderDate(Date reminderDate) {
		this.reminderDate = reminderDate;
	}

	public List<Integer> getResponsibilityFilterList() {
		return responsibilityFilterList;
	}

	public void setResponsibilityFilterList(List<Integer> responsibilityFilterList) {
		this.responsibilityFilterList = responsibilityFilterList;
	}

	public boolean isDontFilterResponsibilities() {
		return dontFilterResponsibilities;
	}

	public void setDontFilterResponsibilities(boolean dontFilterResponsibilities) {
		this.dontFilterResponsibilities = dontFilterResponsibilities;
	}
}
