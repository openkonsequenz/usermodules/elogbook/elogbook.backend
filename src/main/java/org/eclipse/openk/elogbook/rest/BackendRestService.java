/**
******************************************************************************
* Copyright © 2017-2018 PTA GmbH.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* 
*     http://www.eclipse.org/legal/epl-v10.html
* 
******************************************************************************
*/
package org.eclipse.openk.elogbook.rest;

import org.apache.http.HttpStatus;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.eclipse.openk.elogbook.common.Globals;
import org.eclipse.openk.elogbook.controller.BackendControllerNotification;
import org.eclipse.openk.elogbook.controller.BackendControllerNotificationFile;
import org.eclipse.openk.elogbook.controller.BackendControllerRefInfo;
import org.eclipse.openk.elogbook.controller.BackendControllerResponsibility;
import org.eclipse.openk.elogbook.controller.BackendControllerUser;
import org.eclipse.openk.elogbook.controller.BackendControllerVersionInfo;
import org.eclipse.openk.elogbook.controller.BaseWebService;
import org.eclipse.openk.elogbook.controller.ControllerImplementations;
import org.eclipse.openk.elogbook.controller.ControllerImplementations.GetCurrentResponsibilities;
import org.eclipse.openk.elogbook.controller.ResponseBuilderWrapper;
import org.eclipse.openk.elogbook.controller.TokenManager;
import org.eclipse.openk.elogbook.exceptions.BtbException;
import org.eclipse.openk.elogbook.exceptions.BtbExceptionMapper;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

@Path("/beservice")
public class BackendRestService extends BaseWebService {
	private static final Logger logger = LogManager.getLogger(BackendRestService.class.getName());

	@Context
	private UriInfo uriInfo;

	public BackendRestService() {
		super(logger);
	}


	@GET
	@Path("/logout")
	@Produces("application/json")
	public Response logout(@HeaderParam(value = Globals.KEYCLOAK_AUTH_TAG) String token) {

		try (AutoCloseable ignored = perform("logout()")) { // NOSONAR
			TokenManager.getInstance().logout(token);
			return ResponseBuilderWrapper.INSTANCE.buildOKResponse(BtbExceptionMapper.getGeneralOKJson());
		} catch (Exception e) {
			return responseFromException(e);
		}

	}

	@GET
	@Path("/versionInfo/")
	@Produces("application/json")
	public Response getVersionInfo() {
		return invoke(null, SecureType.NONE,
				new ControllerImplementations.GetVersionInfo(new BackendControllerVersionInfo()));
	}

	@GET
	@Path("/notificationStatuses/")
	@Produces("application/json")
	public Response getNotificationStatuses() {
		return invoke(null, SecureType.NONE,
				new ControllerImplementations.GetNotificationStatuses(new BackendControllerRefInfo()));
	}

	@GET
	@Path("/notificationPriorities/")
	@Produces("application/json")
	public Response getNotificationPriorities() {
		return invoke(null, SecureType.NONE,
				new ControllerImplementations.GetNotificationPriorities(new BackendControllerRefInfo()));
	}

	@GET
	@Path("/branches/")
	@Produces("application/json")
	public Response getBranches() {
		return invoke(null, SecureType.NONE,
				new ControllerImplementations.GetBranches(new BackendControllerRefInfo()));
	}

	@GET
	@Path("/gridTerritories/")
	@Produces("application/json")
	public Response getGridTerritories() {
		return invoke(null, SecureType.NONE,
				new ControllerImplementations.GetGridTerritories(new BackendControllerRefInfo()));
	}

	@GET
	@Path("/currentResponsibilities/")
	@Produces("application/json")
	public Response getCurrentResponsibilities(@HeaderParam(value = Globals.KEYCLOAK_AUTH_TAG) String token) {
		return invoke(token, SecureType.NORMAL,
				new GetCurrentResponsibilities(new BackendControllerResponsibility()));
	}

	@GET
	@Path("/plannedResponsibilities/")
	@Produces("application/json")
	public Response getPlannedResponsibilities(@HeaderParam(value = Globals.KEYCLOAK_AUTH_TAG) String token) {
		return invoke(token, SecureType.NORMAL,
				new ControllerImplementations.GetPlannedResponsibilities(new BackendControllerResponsibility()));
	}

	@POST
	@Path("/postResponsibilities/")
	@Consumes("application/json")
	@Produces("application/json")
	public Response postPlanResponsibilities(String responsibilities,
			@HeaderParam(value = Globals.KEYCLOAK_AUTH_TAG) String token) {
		return invoke(token, SecureType.NORMAL, new ControllerImplementations.PostResponsibilities(
				responsibilities, true, new BackendControllerResponsibility()));
	}

	@POST
	@Path("/fetchResponsibilities/")
	@Consumes("application/json")
	@Produces("application/json")
	public Response postFetchResponsibilities(String responsibilities,
										 @HeaderParam(value = Globals.KEYCLOAK_AUTH_TAG) String token) {
		return invoke(token, SecureType.NORMAL, new ControllerImplementations.PostResponsibilities(
				responsibilities, false, new BackendControllerResponsibility()));
	}

	@POST
	@Path("/confirmResponsibilities/")
	@Consumes("application/json")
	@Produces("application/json")
	public Response postResponsibilitiesConfirmation(String responsibilities,
			@HeaderParam(value = Globals.KEYCLOAK_AUTH_TAG) String token) {
		return invoke(token, SecureType.NORMAL,
				new ControllerImplementations.PostResponsibilitiesConfirmation(responsibilities,
						new BackendControllerResponsibility()));
	}

	@GET
	@Path("/allResponsibilities/")
	@Produces("application/json")
	public Response getAllResponsibilities(@HeaderParam(value = Globals.KEYCLOAK_AUTH_TAG) String token) {
		return invoke(token, SecureType.NORMAL,
				new ControllerImplementations.GetAllResponsibilities(new BackendControllerResponsibility()));
	}

	@GET
	@Path("/historicalResponsibilities/{transactionId}")
	@Produces("application/json")
	public Response getHistoricalResponsibilities(@PathParam("transactionId") String transactionId,
			@HeaderParam(value = Globals.KEYCLOAK_AUTH_TAG) String token) {

		return invoke(token, SecureType.NORMAL,
				new ControllerImplementations.GetHistoricalResponsibilitiesByTransactionId(transactionId,
						new BackendControllerResponsibility()));
	}

	@GET
	@Path("/isObserver/")
	@Produces("application/json")
	public Response getIsObserver(@HeaderParam(value = Globals.KEYCLOAK_AUTH_TAG) String token) {
		return invoke(token, SecureType.NORMAL,
				new ControllerImplementations.GetIsObserver(new BackendControllerResponsibility()));
	}

	@GET
	@Path("/users")
	@Produces("application/json")
	public Response getUsers(@HeaderParam(value = Globals.KEYCLOAK_AUTH_TAG) String accesstoken) {
		return invoke(accesstoken, SecureType.NORMAL,
				new ControllerImplementations.GetUsers(new BackendControllerUser(), accesstoken));
	}

	@POST
	@Path("/notifications/{listType}")
	@Produces("application/json")
	public Response getNotifications(String notificationSearchFilter, @PathParam("listType") String listType,
			@HeaderParam(value = Globals.KEYCLOAK_AUTH_TAG) String token) {
		return invoke(token, SecureType.NORMAL, new ControllerImplementations.GetNotifications(listType,
				notificationSearchFilter, new BackendControllerNotification()));
	}

	@GET
	@Path("/user-settings")
	@Produces("application/json")
	public Response getUserSettings(@HeaderParam(value = Globals.KEYCLOAK_AUTH_TAG) String token ) {
		return invoke(token, SecureType.NORMAL, new ControllerImplementations.GetUserSettings( new BackendControllerUser()));
	}

	@POST
	@Path("/user-settings")
	@Produces("application/json")
	public Response postUserSettings(String usersettings,
									 @HeaderParam(value = Globals.KEYCLOAK_AUTH_TAG) String token ) {
		return invoke(token, SecureType.NORMAL, new ControllerImplementations.PostUserSettings( usersettings,
				new BackendControllerUser()));
	}

	@POST
	@Path("/searchResults/")
	@Produces("application/json")
	public Response getSearchResults(String globalSearchFilter,
			@HeaderParam(value = Globals.KEYCLOAK_AUTH_TAG) String token) {
		return invoke(token, SecureType.NORMAL, new ControllerImplementations.GetSearchResults(
				globalSearchFilter, new BackendControllerNotification()));
	}

	@POST
	@Path("/shiftChangeList/")
	@Produces("application/json")
	public Response getHistoricalShiftChangeList(String responsibilitySearchFilter,
			@HeaderParam(value = Globals.KEYCLOAK_AUTH_TAG) String token) {
		return invoke(token, SecureType.NORMAL,
				new ControllerImplementations.GetHistoricalShiftChangeList(responsibilitySearchFilter,
						new BackendControllerResponsibility()));
	}

	@GET
	@Path("/notificationsByIncident/{incidentId}")
	@Produces("application/json")
	public Response getNotificationsByIncidentId(@PathParam("incidentId") String incidentId,
			@HeaderParam(value = Globals.KEYCLOAK_AUTH_TAG) String token) {
		return invoke(token, SecureType.NORMAL,
				new ControllerImplementations.GetNotificationsByIncidentId(incidentId,
						new BackendControllerNotification()));
	}

	@PUT
	@Path("/notifications/create")
	@Consumes("application/json")
	@Produces("application/json")
	public Response createNotification(String newNotification,
			@HeaderParam(value = Globals.KEYCLOAK_AUTH_TAG) String token) {

		return invoke(token, SecureType.NORMAL,
				new ControllerImplementations.CreateNotification(newNotification, new BackendControllerNotification(), token));
	}

	@POST
	@Path("/notifications/update")
	@Consumes("application/json")
	@Produces("application/json")
	public Response updateNotification(String aNotification,
			@HeaderParam(value = Globals.KEYCLOAK_AUTH_TAG) String token) {

		return invoke(token, SecureType.NORMAL,
				new ControllerImplementations.CreateNotification(aNotification, new BackendControllerNotification(), token));
	}

	@GET
	@Path("/notification/{id}")
	@Produces("application/json")
	public Response getNoticifationById(@PathParam("id") String id,
			@HeaderParam(value = Globals.KEYCLOAK_AUTH_TAG) String token) {

		return invoke(token, SecureType.NORMAL,
				new ControllerImplementations.GetNotificationById(id, new BackendControllerNotification()));
	}

	@GET
	@Path("/notifications/active")
	@Produces("application/json")
	public Response getActiveNotifications(@HeaderParam(value = Globals.KEYCLOAK_AUTH_TAG) String token) {
		return invoke(token, SecureType.NORMAL,
				new ControllerImplementations.GetActiveNotifications(new BackendControllerNotification()));
	}

	@GET
	@Path("/getImportFiles")
	@Produces("application/json")
	public Response getImportFiles(@HeaderParam(value = Globals.KEYCLOAK_AUTH_TAG) String sessionID) {
		return invoke(sessionID, SecureType.NORMAL,
				new ControllerImplementations.GetImportFiles(new BackendControllerNotificationFile()));
	}

	@GET
	@Path("/importFile")
	@Produces("application/json")
	public Response getImportFile(@HeaderParam(value = Globals.KEYCLOAK_AUTH_TAG) String sessionID) {
		return invoke(sessionID, SecureType.NORMAL,
				new ControllerImplementations.ImportFile(new BackendControllerNotificationFile()));
	}

	@DELETE
	@Path("/deleteImportedFiles/{fileName}")
	@Produces("application/json")
	public Response deleteImportedFiles(@PathParam("fileName") String fileName,
										@HeaderParam(value = Globals.KEYCLOAK_AUTH_TAG) String sessionID) {
		return invoke(sessionID, SecureType.NORMAL,
				new ControllerImplementations.DeleteImportedFiles(fileName, new BackendControllerNotificationFile()));
	}	

	@POST
	@Path("/currentReminders")
	@Produces("application/json")
	public Response getCurrentReminders(String reminderSearchFilter,
			@HeaderParam(value = Globals.KEYCLOAK_AUTH_TAG) String token) {
		return invoke(token, SecureType.NORMAL, new ControllerImplementations.GetCurrentReminders(
				reminderSearchFilter, new BackendControllerNotification()));
	}

	@GET
	@Path("/assignedUserSuggestions")
	@Produces("application/json")
	public Response getUserSuggestions(@HeaderParam(value = Globals.KEYCLOAK_AUTH_TAG) String token) {
		return invoke(token, SecureType.NORMAL,
				new ControllerImplementations.GetUserSuggestions(new BackendControllerUser(), token));
	}


	@GET
	@Path("/synchronizeContacts")
	@Produces("application/json")
	public Response synchronizeContacts(@HeaderParam(value = Globals.KEYCLOAK_AUTH_TAG) String token) {
		return invoke(token, SecureType.NORMAL,
				new ControllerImplementations.SynchronizeContacts(new BackendControllerUser(), token));
	}

	@GET
	@Path("/testmail")
	@Produces("application/json")
	public Response getTestMail(@HeaderParam(value = Globals.KEYCLOAK_AUTH_TAG) String token) {
		return invoke(token, SecureType.NORMAL,
				new ControllerImplementations.GetTestMail(new BackendControllerUser(), token));
	}
  
	@Override
	protected void assertAndRefreshToken(String token, SecureType secureType) throws BtbException {
		TokenManager.getInstance().checkAut(token);
		TokenManager.getInstance().checkAutLevel(token, secureType);
	}
  

	private Response responseFromException(Exception e) {
		int errcode;
		String retJson;

		if (e instanceof BtbException) {
			logger.error("Caught BackendException", e);
			errcode = ((BtbException) e).getHttpStatus();
			retJson = BtbExceptionMapper.toJson((BtbException) e);
			return Response.status(errcode).entity(retJson).build();
		} else {
			logger.error("Unexpected exception", e);
			return Response.status(HttpStatus.SC_INTERNAL_SERVER_ERROR)
					.entity(BtbExceptionMapper.getGeneralErrorJson()).build();
		}
	}
}
