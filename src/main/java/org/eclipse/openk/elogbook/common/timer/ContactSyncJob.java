package org.eclipse.openk.elogbook.common.timer;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.eclipse.openk.elogbook.controller.BackendControllerUser;
import org.eclipse.openk.elogbook.exceptions.BtbException;
import org.quartz.Job;
import org.quartz.JobExecutionContext;

public class ContactSyncJob implements Job {

    private static final Logger LOGGER = LogManager.getLogger(ContactSyncJob.class.getName());

    @Override
    public void execute(JobExecutionContext arg0) {
        LOGGER.debug("Quartz: ContactSyncJob called");

        try {
            new BackendControllerUser().synchronizeContacts();
        } catch (BtbException btbException) {
            LOGGER.error("Error in synchronizeContacts");
        }
        LOGGER.debug("Quartz: ContactSyncJob finished");

    }

}
