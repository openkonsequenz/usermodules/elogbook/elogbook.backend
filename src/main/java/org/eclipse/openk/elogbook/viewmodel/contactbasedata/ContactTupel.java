package org.eclipse.openk.elogbook.viewmodel.contactbasedata;

import java.util.UUID;

public class ContactTupel {
    private String contactName;
    private UUID contactUuid;

    public String getContactName() {
        return contactName;
    }

    public void setContactName(String contactName) {
        this.contactName = contactName;
    }

    public UUID getContactUuid() {
        return contactUuid;
    }

    public void setContactUuid(UUID contactUuid) {
        this.contactUuid = contactUuid;
    }
}
