/**
******************************************************************************
* Copyright © 2017-2018 PTA GmbH.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* 
*     http://www.eclipse.org/legal/epl-v10.html
* 
******************************************************************************
*/
package org.eclipse.openk.elogbook.viewmodel;


import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Date;
import org.eclipse.openk.elogbook.common.JsonGeneratorBase;
import org.eclipse.openk.elogbook.common.util.ResourceLoaderBase;
import org.junit.Test;

public class ReminderSearchFilterTest extends ResourceLoaderBase {
    // IMPORTANT TEST!!!
    // Make sure, our Interface produces a DEFINED Json!
    // Changes in the interface will HOPEFULLY crash here!!!

    @Test
    public void TestStructureAgainstJson() {
        String json = super.loadStringFromResource("testReminderSearchFilter.json");
        Date earlyDate = new Date(1000L * 60 * 60 * 24 * 365 * 5);
        ReminderSearchFilter rsf = JsonGeneratorBase.getGson().fromJson(json, ReminderSearchFilter.class);
        assertNotNull(rsf);
        assertTrue(earlyDate.before(rsf.getReminderDate()));
        assertNotNull(rsf.getResponsibilityFilterList());
        assertTrue(rsf.getResponsibilityFilterList().size() == 2);
    }

    @Test
    public void testSetters() {
        Date earlyDate = new Date(1000L * 60 * 60 * 24 * 365 * 5);
        Date now = new Date(System.currentTimeMillis());
        ReminderSearchFilter nsf = new ReminderSearchFilter();
        nsf.setReminderDate(earlyDate);
        assertEquals(earlyDate, nsf.getReminderDate());

        nsf.setResponsibilityFilterList(new ArrayList<>());
        nsf.getResponsibilityFilterList().add(3);
        nsf.getResponsibilityFilterList().add(19);
        nsf.getResponsibilityFilterList().add(22);
        nsf.setReminderDate(now);

        assertEquals(Integer.valueOf(3), nsf.getResponsibilityFilterList().get(0));
        assertEquals(Integer.valueOf(19), nsf.getResponsibilityFilterList().get(1));
        assertEquals(Integer.valueOf(22), nsf.getResponsibilityFilterList().get(2));
        assertEquals(now, nsf.getReminderDate());
    }
}
