/**
******************************************************************************
* Copyright © 2017-2018 PTA GmbH.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* 
*     http://www.eclipse.org/legal/epl-v10.html
* 
******************************************************************************
*/
package org.eclipse.openk.elogbook.persistence.util;

import org.junit.Test;

import java.sql.Timestamp;

import static junit.framework.TestCase.assertTrue;

public class TimestampConverterTest {

    @Test
    public void testGetTimestampWithTime() {
        Timestamp ts = new Timestamp(System.currentTimeMillis());
        Timestamp tsRatherNow = TimestampConverter.getTimestampWithTime(ts, 14, 04, 29);
        Timestamp tsBefore1 = TimestampConverter.getTimestampWithTime(ts, 14, 04, 28);
        Timestamp tsBefore2 = TimestampConverter.getTimestampWithTime(ts, 14, 03, 29);
        Timestamp tsBefore3 = TimestampConverter.getTimestampWithTime(ts, 13, 04, 29);
        Timestamp tsAfter1 = TimestampConverter.getTimestampWithTime(ts, 14, 04, 30);
        Timestamp tsAfter2 = TimestampConverter.getTimestampWithTime(ts, 14, 05, 29);
        Timestamp tsAfter3 = TimestampConverter.getTimestampWithTime(ts, 15, 04, 29);

        assertTrue( tsBefore1.before(tsRatherNow) );
        assertTrue( tsBefore2.before(tsBefore1) );
        assertTrue( tsBefore3.before(tsBefore2) );

        assertTrue( tsAfter1.after(tsRatherNow));
        assertTrue( tsAfter2.after(tsAfter1));
        assertTrue( tsAfter3.after(tsAfter2));

    }
}
