/**
******************************************************************************
* Copyright © 2017-2018 PTA GmbH.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* 
*     http://www.eclipse.org/legal/epl-v10.html
* 
******************************************************************************
*/
package org.eclipse.openk.elogbook.common;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.util.Date;

public class JsonGeneratorBase {
    private JsonGeneratorBase() {}
    public static Gson getGson() {
        return new GsonBuilder()
                .registerTypeAdapter(Date.class, new GsonUTCDateAdapter())
                .disableHtmlEscaping()
                .create();
    }
}
