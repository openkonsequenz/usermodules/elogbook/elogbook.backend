/**
******************************************************************************
* Copyright © 2017-2018 PTA GmbH.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* 
*     http://www.eclipse.org/legal/epl-v10.html
* 
******************************************************************************
*/
package org.eclipse.openk.elogbook.viewmodel;


import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import org.eclipse.openk.elogbook.exceptions.BtbException;
import org.junit.Test;

public class TestTypeMapper {

    @Test
    public void listTypeFromString() throws BtbException {
        assertEquals(ListTypeMapper.listTypeFromString( "CuRRent" ), Notification.ListType.CURRENT);
        assertEquals(ListTypeMapper.listTypeFromString( "futuRe" ), Notification.ListType.FUTURE);
        assertEquals(ListTypeMapper.listTypeFromString( "past" ), Notification.ListType.PAST);
        assertEquals(ListTypeMapper.listTypeFromString( "OPEN" ), Notification.ListType.OPEN);
    }

    @Test
    public void testListTypeFromInvalidString () {
    	try {
    		ListTypeMapper.listTypeFromString( "WeissDerGeierWas" );
			fail("Expected BtbBadRequest(\"Unknown parameter value: \"" + "WeissDerGeierWas");
		} catch (BtbException e) {
			assertEquals(e.getMessage(), "Unknown parameter value: WeissDerGeierWas");
		}
	}

    @Test
    public void testListTypeFromNullString () {
    	try {
    		ListTypeMapper.listTypeFromString( null );
			fail("Expected BtbBadRequest(\"Unknown parameter value: \"" + "null");
		} catch (BtbException e) {
			assertEquals(e.getMessage(), "Unknown parameter value: null");
		}
	}
}

