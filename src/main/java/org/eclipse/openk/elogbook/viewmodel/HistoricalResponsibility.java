/**
******************************************************************************
* Copyright © 2017-2018 PTA GmbH.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* 
*     http://www.eclipse.org/legal/epl-v10.html
* 
******************************************************************************
*/
package org.eclipse.openk.elogbook.viewmodel;


import org.eclipse.openk.elogbook.persistence.model.RefBranch;
import org.eclipse.openk.elogbook.persistence.model.RefGridTerritory;

import java.util.Date;

public class HistoricalResponsibility {
    private Integer id;
    private String responsibleUser;
    private String formerResponsibleUser;
    private Date transferDate;
    private Integer transactionId;
    private Date createDate;
    private String createUser;
    private Date modDate;
    private String modUser;
    private RefGridTerritory refGridTerritory;
    private RefBranch refBranch;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getResponsibleUser() {
        return responsibleUser;
    }

    public void setResponsibleUser(String responsibleUser) {
        this.responsibleUser = responsibleUser;
    }

    public String getFormerResponsibleUser() {
        return formerResponsibleUser;
    }

    public void setFormerResponsibleUser(String newResponsibleUser) {
        this.formerResponsibleUser = newResponsibleUser;
    }

    public Date getTransferDate() { return transferDate; }

    public void setTransferDate(Date transferDate) { this.transferDate = transferDate; }

    public Integer getTransactionId() { return transactionId; }

    public void setTransactionId(Integer transactionId) { this.transactionId = transactionId; }

    public Date getCreateDate() { return createDate; }

    public void setCreateDate(Date createDate) { this.createDate = createDate; }

    public String getCreateUser() { return createUser; }

    public void setCreateUser(String createUser) { this.createUser = createUser; }

    public Date getModDate() { return modDate; }

    public void setModDate(Date modDate) { this.modDate = modDate; }

    public String getModUser() { return modUser; }

    public void setModUser(String modUser) { this.modUser = modUser; }

    public RefGridTerritory getRefGridTerritory() { return refGridTerritory; }

    public void setRefGridTerritory(RefGridTerritory refGridTerritory) { this.refGridTerritory = refGridTerritory; }

    public RefBranch getRefBranch() { return refBranch; }

    public void setRefBranch(RefBranch refBranch) { this.refBranch = refBranch; }
}