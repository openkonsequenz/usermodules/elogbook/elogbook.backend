/**
******************************************************************************
* Copyright © 2017-2018 PTA GmbH.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* 
*     http://www.eclipse.org/legal/epl-v10.html
* 
******************************************************************************
*/
package org.eclipse.openk.elogbook.controller;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.slf4j.LoggerFactory;
import org.eclipse.openk.elogbook.common.Globals;
import org.eclipse.openk.elogbook.common.JsonGeneratorBase;
import org.eclipse.openk.elogbook.exceptions.BtbBadRequest;
import org.eclipse.openk.elogbook.exceptions.BtbUnauthorized;
import org.eclipse.openk.elogbook.viewmodel.LoginCredentials;

public class InputDataValuator {
    private static final Logger LOGGER = LogManager.getLogger(InputDataValuator.class.getName());

    private static final String WHITELIST = "[^a-zA-ZäÄöÖüÜß?0-9().,-:;_+=!%§&/'#<>\" ]";

    private void checkCredentialsRaw(String credentials) throws BtbUnauthorized {
        if (credentials == null || credentials.isEmpty()) {
            throw new BtbUnauthorized("No credentials provided");
        }
        if (credentials.length() > Globals.MAX_CREDENTIALS_LENGTH) {
            LOGGER.warn("MaxLength of credentials exceeded");

            throw new BtbUnauthorized("Invalid credentials");
        }
    }

    public void checkCredentials(String credentials) throws BtbUnauthorized {
        checkCredentialsRaw(credentials);

        LoginCredentials obj;
        try {
            obj = JsonGeneratorBase.getGson().fromJson(credentials, LoginCredentials.class);
            checkWhitelistChars(obj.getPassword());
            checkWhitelistChars(obj.getPassword());
        } catch (Exception e) { // NOSONAR
            obj = null;
        }
        if (obj == null || obj.getUserName() == null || obj.getUserName().isEmpty()) {
            LOGGER.warn("Invalid credentials provided. ");
            throw new BtbUnauthorized("Invalid credentials");
        }
    }

    public void checkNotification(String notification) throws BtbBadRequest {
        if (notification == null || notification.isEmpty()) {
            throw new BtbBadRequest("No notification provided");
        }
        if (notification.length() > Globals.MAX_NOTIFICATION_LENGTH) {
            LOGGER.warn("MaxLength of notification exceeded");

            throw new BtbBadRequest("MaxLength of notification exceeded");
        }
    }


    public void checkIncomingSearchFilter(String searchFilter) throws BtbBadRequest {
        if (searchFilter == null || searchFilter.isEmpty()) {
            return;
        }
        if (searchFilter.length() > Globals.MAX_NOTIFICATION_LENGTH) {
            LOGGER.warn("MaxLength of notification search filter exceeded");

            throw new BtbBadRequest("MaxLength of notification search filter exceeded");
        }

    }

    public static void checkNotificationId(String id) throws BtbBadRequest {
        if (id == null || id.isEmpty()) {
            throw new BtbBadRequest("No id provided");
        }
        try {
			Integer.parseInt(id); //NOSONAR only because of possible expected exception
		} catch (NumberFormatException e) {
			throw new BtbBadRequest("Invalid id for notification");
		}
    }

    public static void checkHistoricalResponsibilityId(String id) throws BtbBadRequest {
        if (id == null || id.isEmpty()) {
            throw new BtbBadRequest("No id provided");
        }
        try {

            Integer.parseInt(id); //NOSONAR  only because of possible expected exception
        } catch (NumberFormatException e) {
            throw new BtbBadRequest("Invalid id for responsibility");
        }
    }

    private void checkWhitelistChars(String txt) throws BtbBadRequest {
        checkWhitelistChars(txt, false);
    }

    private void checkWhitelistChars(String txt, boolean logTextOnError) throws BtbBadRequest {
        // empty String is ok
        if (txt == null || txt.isEmpty()) {
            return;
        }
        String tx2 = txt.replaceAll(WHITELIST, "");
        if (!tx2.equals(txt)) {
            LOGGER.warn("Invalid text not matching whitelist" + (logTextOnError ? ":" + txt : ""));
            throw new BtbBadRequest("Invalid text data");
        }
    }
}
