﻿////
******************************************************************************
* Copyright © 2017-2018 PTA GmbH.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
*
*     http://www.eclipse.org/legal/epl-v10.html
*
******************************************************************************
////

openKonsequenz - Architecture of the module "eLogbook@openK"
============================================================
:Author: Frank Dietrich
:Email: frank.dietrich@pta.de
:Date: 2017-07-12
:Revision: 1
:icons:
:source-highlighter: highlightjs
:highlightjs-theme: solarized_dark

This documentation bases on ARC42-Template (v7.0):

*About arc42*

arc42, the Template for documentation of software and system architecture.

By Dr. Gernot Starke, Dr. Peter Hruschka and contributors.

Template Revision: 7.0 EN (based on asciidoc), January 2017

© We acknowledge that this document uses material from the arc 42 architecture template, http://www.arc42.de.
Created by Dr. Peter Hruschka & Dr. Gernot Starke.

<<<

== Introduction and Goals

=== Requirements Overview

The digital logbook (eLogbook@openK) is the main source of information in the central network control unit (CNCU)
for the manual logging of events and operations, which are not recorded automatically in the control system. In
addition to the use for the structured transfer of information during the change of shifts, the company's digital
logbook is to be used as an expanded resource for the work organization in the CNCU and as an information medium for
the well-directed transfer of information from supervisors to employees.

The full requirements of the module eLogbook@openK (in German: Modul Betriebstagebuch) is described in the document

* "Anfragespezifikation Modul Betriebstagebuch" from 15-09-2016.

=== Quality Goals

The eLogbook represents a user module that bases on the architecture platform of openKONSEQUENZ. The main quality
goals of the platform are:

* *Flexibility* The reference platform shall grant that different systems and modules from different vendors/developers can interact and interoperate, and may be exchanged or recombined.
* *Availability* All platform modules that are running on the platform can only be as available as the platform same for user modules that are based on platform modules.
* *Maintainability* (and testability as part of maintainability)  The platform and its platform modules shall be used longer than 15 years.
* *Integration performance* New implemented functionality of oK own modules and external modules shall be included fast / automatically.
* *Security* The platform and its modules need to underly security-by-design

The main quality goals of the user module eLogbook are:

* *Functionality* The user module must fulfil the functional requirements mentioned in the section before
* *Integration performance* The user module must be easy integratable in different production environments. * Modifiability (and testability as part of modifiability) Good documentation (i.e. code and architecture documentation) makes code changes easier and automatic tests facilitate rigorous verification. * Ergonomics The web interface must be realized according to oK-GUI-Styleguide.

The following documents contain the quality goals in detail:

* "Architecture Committee Handbook" v1.2 from 14-09-2016
* "Quality Committee Handbook" v1.1 from 18-08-2016

The architecture is based on the AC-Handbook. The
quality demands are described in the QC-Handbook.
Both specifications were fully complied with in the project, so that a high quality is given.

The code quality regarding static code analysis and unit test code coverage on the backend side is ensured by the
use of *sonarqube*. The rule set and the qualtity gate are defined by the default, the so called "sonar way".

The project eLogbook@openK bases on the Eclipse Public Licence 1.0.

=== Stakeholders

.Stakeholders
[options="header,footer"]
|=========================================================
|Role/Name|Contact|Expectations
|Product Owner (represents the Distribution System Operators)|Gordon Pickford, Oliver Tantu|The software must fulfil their functional and nonfunctional Requirements.
|Module Developer|Michel Allessandrini, Jonas Tewolde, Frank Dietrich|All relevant business and technical information must be available for implementing the software.
|External Reviewer (represents the AC/QC)|Martin Jung, Anja Berschneider|The software and the documentation is realized according the Quality and Architecture Handbook of openKONSEQUENZ.
|System Integrator||A documentation for the integration of the module in the DSO specific environments must be available.
|=========================================================

== Architecture Constraints

The main architecture constraints are:

* *Public License* The module must be available under the “Eclipse Public License 1.0”.
* *Availability* The source code of the module must be accessible to any interested person/company. Therefore the project is published at https://projects.eclipse.org/projects/technology.elogbook
* *Standardization* The module must use standardized data structures (CIM) [if available] and the reference platform.

=== Technical Constraints

The following technical constraints are given:

.Technical Contraints
[options="header,footer"]
|========================================================
|Component|Constraints
|Basis components of the reference platform|
- Application Server Tomcat
- JPA EclipseLink
- Database PostgreSQL

|Enterprise Service Bus|
* ESB Talend
* Communication via RESTful Webservices

|Programming Language Frontend
a|* Angular
* Bootstrap
* jQuery
* REST/JSON Interfaces

|GUI design
a|* According to oK-GUI-Styleguide

|Java QA environment|Sonarqube 5.6.6

|Programming Language
a|* Backend: Java 1.8
* Frontend: Angular 4.0.0 (Javascript, Typescript, HTML5, CSS3)

|IDE
a|* Not restricted (Eclipse, IntelliJ, Microsoft Developer Studio, Microsoft Visual Code ...)

|Build system
a|* Backend: Maven
* Frontend: NodeJS + Angular/cli

|Libraries, Frameworks,Components
a|* Used Libraries/Frameworks have to be compatible to the Eclipse Public License
|Architecture Documentation|* According ARC42-Template
|========================================================


=== Technical Dependencies

The following libraries are used:

.Libraries
[options="header,footer"]
|=========================================================
|Name of the library|Version|Artefact-id|Usage|License|Tier
|Node.js|6.10.0 LTS||JavaScript Runtime (server side)|MIT License|Frontend
|npm (in Node.js enthalten)|3.10.10||Node Package Manager (package manager for libraries)|Artistic License 2.0|Frontend
|Angular|4.0.0||UI-Framework|MIT License|Frontend
|Angular Material|2.0.0-beta.2||Angular Material Design Components|MIT License|Frontend
|Bootstrap|3.3.7||CSS-Framework |MIT License|Frontend
|jQuery|3.1.1||JavaScript Bibliothek|MIT License|Frontend
|ng2-daterangepicker|1.0.4||Angular 2 Daterangepicker Component|MIT License|Frontend
|Moment.js|2.16.0||JavaScript library for date and time processing|MIT License|Frontend
|core-js|2.4.1||JavaScript Polyfill for angular support within older browsers|MIT License|Frontend
|rxjs|5.0.1||JavaScript Polyfill for Observables|Apache License 2.0|Frontend
|TS-helpers|1.1.1||Helper library for compiling typescript|MIT License|Frontend
|zone.js|0.7.2||JavaScript Polyfill for asynchronous data binding|MIT License|Frontend
|org.apache.httpcomponents.httpclient|4.5.3
a|
[source,xml]
----
<dependency>
    <groupId>org.apache.httpcomponents</groupId>
    <artifactId>httpclient</artifactId>
    <version>4.5.3</version>
</dependency>
----
|Backend, Http-Client|Apache 2.0|Backend

|org.json.json|20160810

a|
[source,xml]
----
<dependency>
    <groupId>org.json</groupId>
    <artifactId>json</artifactId>
    <version>20160810</version>
</dependency>
----
|Backend - Json functionality|Json|Backend

|org.jboss.resteasy.resteasy-jaxrs|3.0.21_Final
a|
[source,xml]
----
<dependency>
    <groupId>org.jboss.resteasy</groupId>
    <artifactId>resteasy-jaxrs</artifactId>
    <version>3.0.21.Final</version>
</dependency>
----
|Backend - RestServer|Apache 2.0 / CC0.1.0/ Public|Backend

|org.jboss.resteasy.jaxrs-api|3.0.12.Final
a|
[source,xml]
----
<dependency>
    <groupId>org.jboss.resteasy</groupId>
    <artifactId>jaxrs-api</artifactId>
    <version>3.0.12.Final</version>
</dependency>
----
|Rest-Server|Apache 2.0|Backend

|javax.servlet.servlet-api|3.0.1
a|
[source,xml]
----
<dependency>
    <groupId>javax.servlet</groupId>
    <artifactId>servlet-api</artifactId>
    <version>3.0.1</version>
</dependency>
----
|Backend - Logging Servlet |CDDL GLP 2.0|Backend

|com.google.code.gson.gson|2.8.0
a|
[source,xml]
----
<dependency>
    <groupId>com.google.code.gson</groupId>
    <artifactId>gson</artifactId>
    <version>2.8.0</version>
</dependency>
----
|Backend Json de-serialization|Apache 2.0|Backend

|log4j.log4j|1.2.17
a|
[source,xml]
----
<dependency>
    <groupId>log4j</groupId>
    <artifactId>log4j</artifactId>
    <version>1.2.17</version>
</dependency>
----
|Backend logging|Apache 2.0|Backend

|commons-io|2.5
a|
[source,xml]
----
<dependency>
    <groupId>commons-io</groupId>
    <artifactId>commons-io</artifactId>
    <version>2.5</version>
</dependency>
----
|IO utils|Apache 2.0

|org.eclipse.persistence.eclipselink|2.6.4|Backend
a|
[source,xml]
----
<dependency>
    <groupId>org.eclipse.persistence</groupId>
    <artifactId>eclipselink</artifactId>
    <version>2.6.4</version>
</dependency>
----
|JPA implementation|EDL 1.0 EPL 1.0|Backend

|postgresql.postgresql|9.1-901-1.jdbc4
a|
[source,xml]
----
<dependency>
    <groupId>postgresql</groupId>
    <artifactId>postgresql</artifactId>
    <version>9.1-901-1.jdbc4</version>
</dependency>
----
|DB driver|BSD|Backend

|junit.junit|4.12

a|
[source,xml]
----
<dependency>
    <groupId>junit</groupId>
    <artifactId>junit</artifactId>
    <version>4.12</version>
</dependency>
----
|Unit testing|EPL 1.0|Backend

|org.easymock.easymock|3.4
a|
[source,xml]
----
<dependency>
    <groupId>org.easymock</groupId>
    <artifactId>easymock</artifactId>
    <version>3.4</version>
</dependency>
----
|Unit testing|Apache 2.0|Backend

|org.powermock.powermock-api-easymock|1.6.6
a|
[source,xml]
----
<dependency>
    <groupId>org.powermock</groupId>
    <artifactId>powermock-api-easymock</artifactId>
    <version>1.6.6</version>
</dependency>
----
|Unit testing|Apache 2.0|Backend

|org.jacoco.jacoco-maven-plugin|0.7.9
a|
[source,xml]
----
<dependency>
    <groupId>org.jacoco</groupId>
    <artifactId>jacoco-maven-plugin</artifactId>
    <version>0.7.9</version>
</dependency>
----
|Test coverage|EPL 1.0|Backend
|=========================================================

== System Scope and Context

=== Business Context

The user module eLogbook communicates via Restful Webservices and the filesystem with other modules and systems (see figure 1):

* *Core Module "Auth & Auth"* The eLogbook can only be used by authorized users. Therefore, it is essential to invoke the module “Auth & Auth” for authorization and authentication purposes.
* *Source System "SCADA"* The eLogbook needs information from the system “SCADA”. Therefore, it must provide an interface for receiving the according data.

.System-Context of eLogbook
[plantuml]
----
     Interface iaNa [
        A&A-API
     ]

     Interface fs [
        File system
     ]
    Component scada [
        SCADA
     ]

    Component el [
        eLogbook
     ]

    Component kc [
        keycloak
     ]
    Component aNa [
        Auth & Auth
     ]
     Interface ikc [
        KC-API
     ]


     kc--ikc
     aNa->ikc
     aNa--iaNa
     el->iaNa
     scada->fs
     el->fs


----

=== Technical Context

The following aspects have to be taken into account for external communication of the module eLogbook:

* As interface-technology RESTful web services are used.
* Each external interface (interfaces between modules or external systems) has to be documented.
* Dependencies of modules to services realized by other modules have to be specified and documented explicitly.
* When CIM is not appropriate (like access management), other standards in their respective domain shall be taken into account first to avoid proprietary and inaccurate interfaces. The interface has also be documented in the overall openKONSEQUENZ interface profile and it should use REST & XML.

The interfaces of the module eLogbook are described in the documentation *"elogbook_interfaceDocumentation"*.
 
=== Solution Strategy

The module eLogbook bases on a three-tier architecture:

. *Frontend* - The GUI is implemented as a web-frontend with rich-client functionalities.
. *Backend* - The business functionalities are implemented in the backend tier. It provides the business functions via RESTful Webservices.
. *Database* - The database stores all module specific data.

== Building Block View

=== Whitebox Overall System

The module eLogbook contains two components (see figure 2):

. *UI* - Represents the graphical user interface and consumes the services from the Business logic component via RESTful webservices.
. *Business Logic* - Realizes the business functionality and the data storage of the module.

.Module components
[options="header,footer"]
[plantuml]
----
node Module {
    rectangle UI
    rectangle BusinessLogic

    interface REST

    UI -> REST
    REST -- BusinessLogic
}
----


Bases on the abstract concept mentioned above, the following figure shows the concrete realization of the main components.
The mapping from the abstract to the concrete view is as follows:

. UI
 - elogbookFE-SPA
. Business Logic
 - elogbook.war (Rest-Service)
 - elogbook-DB (in figure called: Betriebstagebuch Datenbank)

.Distribution of components
[options="header,footer"]
[plantuml]
----
node ClientComputer {
    node WebBrowser {
        component elogbookFE_SPA
    }
}

node openKonsequenz_LAN {
    node ApacheTomcat {
        component elogbook_war_RESTService
    }
    node PostgresDMBS {
        component elogbook_Database
    }
}

WebBrowser -(0- ApacheTomcat
ApacheTomcat -(0- PostgresDMBS
----

The communication between WebBrowser and Apache Tomcat is established via HTTP/HTTPS.
ApacheTomcat is connected to the data source (PostgresDBMS) via TCP/IP.


==== elogbook.war (Backend tier)

This component implements the business functionality of the digital logbook. And it provides services, that the
elogbookFE – SPA can use the functions in the frontend.

The elogbook.war runs on the Apache Tomcat in the DSO specific environments (in the figures shown as openKONSEQUENZ - LAN)
The backend is build in form of a WAR-File. This kind of Binary can be deployed on many java based webservers. Because Apache Tomcat is
a demand of the architecture committee handbook, the deployment description has been evaluated for this specific web server.


==== elogbook-DB (Database tier)

This component stores the data of the digital logbook. It provides an interface to the elogbook.war to create or
change data in the database.

The elogbook-DB runs on a Postgres DBMS.
(The decision to use the Postgres DBMS was made by the openKONSEQUENCE archtecture committee)

The eLogbook needs information from the system "SCADA". Therefore, it must provide an interface for receiving the according data, see figure 1.
(see below "Import functionality")

=== Level 2

==== elogbookFE – SPA (Frontend tier)

The frontend component implements the concept of a single-page application (SPA). The framework used is Angular2/4.
(The decision was made on this framework because the "AngularJS" required in the  "Architecture Committee Handbook" was already out of date at the time the project started)
Because the frontend is only connected to the backend via REST-services, changing to another technology is rather easy.


It divides the elogbookFE into three layers:

. *Components* - The components (Pages, Lists, Dialogs, Common Comp.) represent the presentation layer and the control layer. A component contains the control logic (.ts-file), an HTML-fragment as presentation description (.html-file) and a style definition (.css-file).
. *Services* - The service component communicates with the interfaces of the backend via HTTP requests by using the model component.
. *Model* - The model corresponds to the View-Model of the backend tier.

.Frontend tier
[options="header,footer"]
image::FrontendTier.png[]

==== elogbook.war (Backend tier)

The backend tier contains five components which can be summarized in three layers:

. *Presentation layer* - Represented by
 .. REST-Srv
 .. View Model
. *Controller layer* - Represented by
 ..	Controller
.	*Model layer* - Represented by
 ..	DAO
 ..	Model


.Backend tier
[options="header,footer"]
image::BackendTier.png[]

==== elogbook-DB (Database tier)

The elogbook-DB is realized as a relational database system.

.Database tier
[options="header,footer"]
image::DatabaseTier.png[]


== Runtime View

=== Login / Authentication

There is no login page, since the openK-Portal-Application is responsible for Authentication and
the whole SSO (single sign on) process.
Therefore the elogbook application has to be started by providing a valid authentication token.
This token is a JWT (JSON Web Token).

.eLogbook application is called by the *portal* application. The User is already logged in
[plantuml]
....
actor User
participant PortalFrontend
participant PortalBackend
participant eLogbookFrontend
entity eLogbookFEStorage
participant eLogbookBackend


User->PortalFrontend: Start eLogbook(JWT)
PortalFrontend->eLogbookFrontend: nav. to frontend-URL with JWT
eLogbookFrontend->eLogbookFEStorage: Extract JWT and store token in session
... some delay ...
eLogbookFrontend->eLogbookBackend: Call any secured service with JWT
group Call secured service

    eLogbookBackend->PortalBackend: "/checkAut(JWT)"
    group Authorization succeeded
        eLogbookBackend->eLogbookBackend: run service
        eLogbookBackend->eLogbookFrontend: return service result
    end
    group Authorization failed
        eLogbookBackend->eLogbookFrontend: return HTTP Code 401
    end
end
....


=== Shift Change

The precondition for performing a shift change is, that one or more users already have
the responsibility for a set of grid territory/branch combinations.

In the following example the user 'Max' wants to quit his shift and therefore needs to
transfer his responsibilities to other users (in our case to 'Pete' and 'Hugo').
He presses the Button "Schicht übergeben" (-> Transfer Shift) on the main overview page.

First the frontend reads all users from the backend. After that the backend reads all current
responsibilities of Max (The backend determines "Max" from the given session info) and returns them.

On the dialog "Schichtübergabe" (->Shift change) Max assigns all of his current responsibilities to
'Hugo' and 'Pete'. Both users are now *planned* for responsibilities.


.Max initiates a shift change
[plantuml]
....
actor Max
participant Frontend
participant Backend
entity Database

Max->Frontend: press "Schicht übergeben"


Frontend->Backend: Service "users"
Backend-->Frontend: all users

Frontend->Backend: Service "currentResponsibilities" [SessionInfo]
Backend->Database: read current Resp. for [SessionInfo.Max]
Database-->Backend
Backend-->Frontend:

Frontend-->Max: Show dialog "Schichtübergabe"

Max->Frontend: Select users
Frontend-->Max

Max->Frontend: assign Pete and Hugo
Frontend-->Max

Max->Frontend: press "übergeben"
Frontend->Backend: Service "planResponsibilities"
Backend->Database: store 'Pete' and 'Hugo' as planned responsibilities
Database-->Backend:
Backend-->Frontend:
Frontend-->Max:
....

After the dialog "Schichtübergabe" 'Pete' and 'Hugo' are stored as
*planned responsibilies* for different combinations of grid territory/branch.


.Pete logs in
[plantuml]
....
actor Pete
participant Frontend
participant Backend
entity Database

Pete->Frontend: Login
Frontend->Backend: plannedResponsibilities [SessionInfo]
Backend->Database: read planned resps for [SessionInfo.Pete]

Database-->Backend: result
Backend-->Frontend: result
Frontend->Frontend: check if result not empty
Frontend-->Pete: (then) Show "Schichtübergabe Protokoll"

Pete->Frontend: Select/Deselect responsibilities
Frontend-->Pete:

Pete->Frontend: press "Übernehmen"
Frontend->Backend: Service "confirmResponsibilities"
Backend->Database: Switch the selected resps. from "planned" to "current"
Database-->Backend:
Backend->Database: Store as historical responsibility
Database-->Backend:
Backend-->Frontend:
Frontend-->Pete:
....

When 'Pete' presses "Übernehmen" (->"Take Responsibilities") all the selected
responsibilities for 'Pete' (regarding grid territory and branch) are switched
from 'planned' to 'current' in a single transaction. That means, that 'Pete'
is no longer planned for a set of responsibilities. From now on he is responsible
for them. In the same transaction the data of this process is stored as the historical data

When 'Hugo' logs in, it is the same process as for 'Pete'.

If 'Hugo' or 'Pete' decide to deselect a responsibility they are planned for,
then that one will be kept with 'Max' as responsible person. 'Max' will then
need to plan it for another person.

=== Import functionality

Informations can be imported into the elogbook system. Therefore an exchange file directory can be defined (see below "Configuration of the backend").
If there exist files in that directory, then the "Import"-Button changes its colour from blue to orange. An import file has to
have to following structure:
[source, text]
----
1:                                                           PROTOKOLL_STROM
2:                                             08.09.2017 10:10:51  -  08.09.2017 10:22:04
3:
4:Betriebsart: Prozessführung
5:Modus      : Spalten-Filtern
6:Verknüpfung: --------
7:
8:.............OW.........................................................................................................................
9:16:12:51,425 OW Test text         Offenbach Wasser
10:ENDE
11:
12:08.09.2017 10:23:34
----

Only Line 9 will be imported. This line number can be configured in the backend:

* "OW" means "Offenbach/Water" ("O"=Offenbach, "M"=Mannheim; "E" in the file means "S" in the branches, "W", "FW", "G" are mapped directly to the branches).
* The text right to "OW" is imported as the notification text.

== Deployment View

The elogbook application consists of 3 parts regarding the deployment.

. Frontend: "elogbookFE"-Directory
. Backend: "elogbook.war"
. Database: The scripts are found in the "db" folder within the backend sources.

=== Deployment of the application components

==== Deployment of the frontend

The Frontend SPA is built as a folder, that contains all the required files for
the application. When deploying the frontend application, the content of the "dist"-folder
within the Frontend development-directory has to be copied into the
target-directory of the apache-tomcat:

 <Apache-Tomcat>/webapps/elogbookFE

If the target folder does not exist, it has to be created manually.

==== Deployment of the backend
The backend REST-services are built in form of a "WAR"-file (to be found
in the "target"-directory of the MAVEN-directory structure).
For deployment, this file has to be copied to the directory

 <Apache-Tomcat>/webapps

==== Deployment of the database
Currently there is no automatic mechanism for distributing structural
or content related changes to the database.
DB related changes are deployed using database scripts directly in suitable
DB management applications like *pgAdminIII* or directly in the *postgres-console*.
These script can be found in the backend project in the directory

 <elogbook backend project directory>/db/postgreSQL


. If needed, create the Database and the access role "btbservice" with *"01_add_DB.sql"*
. The script *"01a_drop_DB_1.0.0.sql"* is only needed for dropping all objects in the db!
. Use the script *"02_create_DB_1.0.0.sql"* to create all database objects
. You will probably need to modify the last script *"03_config_DB_1.0.0.sql"* before you run it. Configurate
your branches, grid territories and your responsibilities here!

TIP: If you want to install the system on an oracle dbms, please take the db scripts from
the */db/oracle* subfolder.

==== Configuration of the system

===== DB based configuration

. Grid territories - The grid-territories have to be configured in the DB-Table *ref_grid_territory*

Please modify the "03_config_DB_*.sql" script (in "<elogbook backend project dir>\db\postgreSQL" ) to change the
configuration. (This file is contained in the *oracle* folder as well.

===== Configuration of the webserver

There exists the file *context.xml* in the "conf" subdirectory (*<TOMCAT>/conf*) of the target apache tomcat installation.
Add the following parameter and resource in order to access the correct backend configuration and to
gain access to the database:

.context.xml
[source,xml]
----
[...]
    <!-- Uncomment this to disable session persistence across Tomcat restarts -->

    <!--Manager pathname=""/>


    <Parameter name="environment" override="false" value="Development"/-->

    <Parameter name="OK_ELOGBOOK_ENVIRONMENT" override="false" value="Production"/>

    <Resource name="jdbc/okBetriebstagebuchDS"
              auth="Container"
              type="javax.sql.DataSource"
              driverClassName="org.postgresql.Driver"
              url="jdbc:postgresql://{dbserver}:5432/{dbname}"
              username="{dbuser}"
              password="{dbpassword}"/>


    <!-- Uncomment this to enable Comet connection tacking (provides events
         on session expiration as well as webapp lifecycle) -->
[...]
----

For oracle please use...

.contextOracle.xml
[source,xml]
----
[...]
    <!-- Uncomment this to disable session persistence across Tomcat restarts -->

    <!--Manager pathname=""/>


    <Parameter name="environment" override="false" value="Development"/-->

    <Parameter name="OK_ELOGBOOK_ENVIRONMENT" override="false" value="Production"/>

    <Resource name="jdbc/okBetriebstagebuchDSO"
              auth="Container"
              type="javax.sql.DataSource"
              driverClassName="oracle.jdbc.driver.OracleDriver"
              url="jdbc:oracle:thin:@{orasvr}:1521:{dbname}"
              username="{dbuser}"
              password="{dbpassword}"/>


    <!-- Uncomment this to enable Comet connection tacking (provides events
         on session expiration as well as webapp lifecycle) -->
[...]
----

*{dbserver}*, *{dbname}*, *{dbuser}* and *{dbpassword}* need to be replaced by the actual values.

CAUTION: The postgres database drivers (postgresql-42.0.0.jar or ojdbc7_g.jar} referenced in the *context.xml* needs to be copied from the lib folder within the
backend source files to the *<TOMCAT>/lib* folder of the target tomcat installation.

(The jar is located in *deploy/lib/*)

CAUTION: If you want to use an oracle db then you have to change
the file backendConfigProduction: Set "activePersistencyUnit" to "betriebstagebuchORA".
(see next chapter "Configuration of the backend").


===== Configuration of the backend

After the backend war file has been deployed and unpacked inside of the *<TOMCAT>/webapps* folder there are different
 backend config files to be found in the folder *<TOMCAT>/webapps/elogbook/WEB-INF/classes*

* backendConfigCustom.json
* backendConfigDevLocal.json
* backendConfigDevServer.json
* backendConfigProduction.json

The active configuration is chosen by parameter *OK_ELOGBOOK_ENVIRONMENT* (see context.xml above).
Possible values are:

* *Custom* (for backendConfigCustom.json)
* *DevLocal* (for backendConfigDevLocal.json)
* *DevServer* (for backendConfigDevServer.json)
* *Production* (for backendConfigProduction.json)

After choosing an environment the corresponding json file has to be configured:

.backendConfigXXX.json
[source,json]
----
{
  "portalBaseURL" : "http://{hostname}:{port}/portal/rest/beservice",
  "contactBaseDataApiUrl" : "http://localhost:9155",
  "fileRowToRead": "9",
  "importFilesFolderPath": "/home/btbservice/importFiles",
  "activePersistencyUnit": "betriebstagebuch",
  "applicationAccessRole": "elogbook-access",
  "emailSmtpHost": "entdockergss",
  "emailSmtpPort": "1025",
  "emailSender": "testSender@test.de",
  "isHtmlEmail": true,
  "isUseHtmlEmailTemplate": true,
  "emailPresenceCheckRecipient": "testerRecipient33@test.de",
  "emailPresenceCheckSubject": "openKONSEQUENZ Betriebstagebuch Türkontaktmeldung/Anlagenbegehung Erinnerung",
  "isOnlySendSameDayReminderEmail": true,
  "cronEmailPresenceCheck": "* 0/1 * * * ?",
  "enableEmailPresenceCheckJob": true
}
----
* *portalBaseURL* - The portal base url should point the the address where the portal backend (the *Auth&Auth*-Server)
                    can be accessed. This is important for the authorization of the backend services.
* *contactBaseDataApiUrl* - Base url of the *Contact Base Data*-Module.
* *fileRowToRead* - Defines the line number inside the import-file of the relevant text line.
* *importFilesFolderPath* - Defines the exchange directory for the import functionality
* *activePersistencyUnit* - Defines the active part of the file persistenc.xml. Choose *"betriebstagebuchORA"* for the
                    usage of OracleDB and *"betriebstagebuch"* to use PostgreSQL.
* *applicationAccessRole* - Defines the role for which users are fetched (For the selection in shift-change-dialog).
* *emailSmtpHost* - Smtp host used for the sending of e-mails
* *emailSmtpPort* - Port on the Smtp host
* *emailSender* - Sender of the automatically generated emails
* *isHtmlEmail* - Config parameter if html e-mail should be used
* *isUseHtmlEmailTemplate* - Config parameter if hte html e-mail template (emails/emailContentCheckPresence.html) should be used
* *emailPresenceCheckRecipient* - Recipient of the presence check e-mail
* *emailPresenceCheckSubject* Mail subject of the presence check e-mail
* *isOnlySendSameDayReminderEmail* - Flag the tells the system if a presence check e-mail is only sent at the same day of the reminder date
* *cronEmailPresenceCheck* - Cron-Expression that tells the system how often the notifications are checked for potentially existing presence check mails.
Examples: "0 0/1 * * * ?" once a minute, "0/10 * * * * ?" every 10 Seconds, "30 0 16 * * ?" every day at 16:00:30 o'clock
* *enableEmailPresenceCheckJob* - Select true or false to determine whether the presence check job should run or not.
* *cronSyncContacts*: Cron-Expression telling how often the contact for the responsibility forwarding should be updated from the ContactBaseData module
* *enableContactSyncJob*: Flag that tells the system, if the contacts (responsibility forwarding) have to be synchronized with the ContactBaseData module (true/false),
* *contactServiceTechUser*: Technical User for accessing the ContactBaseData Module. (For the synchronisation of the contacts a KeyCloak-User is needed, having at least the rolen "KON-READER")
* *contactServiceTechUserPwd*: Password for the technical user above

=== CI- and CD-Components

==== GIT-Repository

Frontend Repository:
http://git.eclipse.org/c/elogbook/elogbookFE.git/

Backend Repository (containing this documentation and the db creation scripts)
http://git.eclipse.org/c/elogbook/elogbook.git/


=== Continuous Deployment

The continuous deployment is realized on two platforms:

 * the development platform (Dev-Environment)
 * the quality platform (Q-Environment)

The automatic deployment on both of the environments is
directly linked to the branches on the GIT-repositories:

. "SNAPSHOT" or "DEVELOP"
. "MASTER" or "TRUNC"

The running development is exclusively made on the Snapshot-Branch. Every time
a developer checks in (pushes) code to the repository, an automatic build
starts on the hudson ci-server. If the Snapshot-build is successful, then
outcome of that build is directly deployed on the Dev-Environment.

At the end of a scrum sprint or when a big userstory is realized, all
the code changes are merged from the *Snapshot*-Branch to the *Trunc*.
This automatically triggers the build and the deployment on the
Q-Environment.

CAUTION: Changes on the database are not deployed automatically!

== Design Decisions

All architecture decisions based on the Architecture Committee Handbook. There are no deviations.

== Risks and Technical Debts

(Currently there aren't any known issues)

<<<

== Glossary

.Abbreviations and glossary terms
[options="header,footer"]
|========================================================
|Short|Long|German|Description
|AC|Architecture Committee|Architektur-Komittee|Gives Framework and Constraints according to architecture for oK projects.
|CNCU|Central Network Control Unit||
|DAO|Data Access Objects||
|DSO|Distribution System Operator|Verteilnetz-betreiber (VNB)|Manages the distribution network for energy, gas or water.
|EPL|Eclipse Public License||Underlying license model for Eclipse projects like elogbook@openK
|ESB|Enterprise Service Bus||Central instance for exchange of data to overcome point-to-point connections.
|oK|openKONSEQUENZ|openKONSEQUENZ|Name of the consortium of DSOs
|QC|Quality Committee|Qualitätskomitee|Gives framework and constraints according to quality for oK projects.
|SCADA|Supervisory Control and Data Acquisition|Netzleitsystem|System, that allows DSOs view/control actual parameters of their power grid.
|========================================================

