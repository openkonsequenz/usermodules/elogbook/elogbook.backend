/**
******************************************************************************
* Copyright © 2017-2018 PTA GmbH.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* 
*     http://www.eclipse.org/legal/epl-v10.html
* 
******************************************************************************
*/
package org.eclipse.openk.elogbook.exceptions;

import org.eclipse.openk.elogbook.viewmodel.ErrorReturn;

public class BtbNestedException extends BtbException {
    private static final long serialVersionUID = 6771249960550163561L;
    private final ErrorReturn errorReturn;

    public BtbNestedException(ErrorReturn er) {
        super(er.getErrorText());
        errorReturn = er;
    }

    public BtbNestedException(ErrorReturn er, Throwable t) {
        super(er.getErrorText(), t);
        errorReturn = er;
    }

    public ErrorReturn getErrorReturn() {
        return errorReturn;
    }

    @Override
    public int getHttpStatus() {
        return errorReturn.getErrorCode();
    }
}
