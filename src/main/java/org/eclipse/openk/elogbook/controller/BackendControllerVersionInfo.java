/**
******************************************************************************
* Copyright © 2017-2018 PTA GmbH.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* 
*     http://www.eclipse.org/legal/epl-v10.html
* 
******************************************************************************
*/

package org.eclipse.openk.elogbook.controller;

import javax.persistence.EntityManager;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import org.eclipse.openk.elogbook.persistence.dao.AutoCloseEntityManager;
import org.eclipse.openk.elogbook.persistence.dao.EntityHelper;
import org.eclipse.openk.elogbook.persistence.dao.RefVersionDao;
import org.eclipse.openk.elogbook.persistence.model.RefVersion;
import org.eclipse.openk.elogbook.viewmodel.VersionInfo;

public class BackendControllerVersionInfo {
	
	private static final Logger LOGGER = LogManager.getLogger(BackendControllerVersionInfo.class.getName());

	public VersionInfo getVersionInfo() {
		LOGGER.debug("getVersionInfo is called");

		EntityManager emOrg = EntityHelper.getEMF().createEntityManager();
		try (AutoCloseEntityManager em = new AutoCloseEntityManager(emOrg)) {
			return getVersionInfoImpl(new RefVersionDao(em), getClass().getPackage().getImplementationVersion());
		} finally {
			LOGGER.debug("getVersionInfo is finished");
		}
	}

	private VersionInfo getVersionInfoImpl(RefVersionDao dao, String pomVersion) {
		RefVersion dbVersion = dao.getVersionInTx();
		VersionInfo vi = new VersionInfo();
		vi.setBackendVersion(pomVersion);

		if (dbVersion == null) {
			vi.setDbVersion("NO_DB");
		} else {
			vi.setDbVersion(dbVersion.getVersion());
		}
		return vi;
	}
}
