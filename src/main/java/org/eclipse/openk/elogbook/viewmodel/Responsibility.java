/**
******************************************************************************
* Copyright © 2017-2018 PTA GmbH.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* 
*     http://www.eclipse.org/legal/epl-v10.html
* 
******************************************************************************
*/
package org.eclipse.openk.elogbook.viewmodel;


public class Responsibility {
    private Integer id;
    private String responsibleUser;
    private String newResponsibleUser;
    private String branchName;
    private Boolean isActive;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getResponsibleUser() { return responsibleUser; }

    public void setResponsibleUser(String responsibleUser) {
        this.responsibleUser = responsibleUser;
    }

    public String getNewResponsibleUser() {
        return newResponsibleUser;
    }

    public void setNewResponsibleUser(String newResponsibleUser) {
        this.newResponsibleUser = newResponsibleUser;
    }

    public String getBranchName() {
    return branchName;
  }

    public void setBranchName(String branchName) {
    this.branchName = branchName;
  }

    public Boolean isActive() {
        return isActive;
    }

    public void setIsActive(Boolean active) {
        isActive = active;
    }
}