/**
******************************************************************************
* Copyright © 2017-2018 PTA GmbH.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* 
*     http://www.eclipse.org/legal/epl-v10.html
* 
******************************************************************************
*/
package org.eclipse.openk.elogbook.viewmodel;


import org.eclipse.openk.elogbook.common.JsonGeneratorBase;
import org.eclipse.openk.elogbook.common.util.ResourceLoaderBase;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Date;

import static org.junit.Assert.*;

public class NotificationTest extends ResourceLoaderBase {
    // IMPORTANT TEST!!!
    // Make sure, our Interface produces a DEFINED Json!
    // Changes in the interface will HOPEFULLY crash here!!!

    @Test
    public void testStructureAgainstJson() {
        String json = super.loadStringFromResource("testNotification.json");

        Notification noti[] = JsonGeneratorBase.getGson().fromJson(json, Notification[].class);

        Date earlyDate = new Date(1000L * 60 * 60 * 24 * 365 * 5);

        assertNotNull( noti );
        assertEquals( noti.length, 3);
        assertEquals((int) noti[0].getId(), 1);
        assertEquals((int) noti[0].getIncidentId(), 23);
        assertFalse( noti[0].isSelected() );
        assertTrue( noti[0].getStatus().equals("offen") );
        assertTrue( noti[1].getNotificationText().equals("Fehler Bereich D") );
        assertTrue( noti[1].getFreeText().equals("free Willi"));
        assertTrue( noti[0].getFreeText().equals(""));
        assertTrue(noti[1].getFreeTextExtended().equals("aa"));
        assertTrue( noti[0].getResponsibilityForwarding().equals(""));
        assertTrue( noti[0].getResponsibilityControlPoint().equals("Abteilung 4"));
        assertTrue( earlyDate.before(noti[1].getReminderDate()));
        assertNull( noti[0].getReminderDate());
        assertNull( noti[0].getFutureDate());
        assertTrue( earlyDate.before(noti[0].getCreateDate()));
        assertNull( noti[0].getExpectedFinishDate());
        assertNull( noti[0].getFinishedDate());
        assertTrue( noti[1].getCreateUser().equals("MasterAndServant"));
        assertTrue(earlyDate.before((noti[0].getModDate())));
        assertTrue(noti[0].getModUser().equals("_fd"));
        assertEquals((int) noti[0].getVersion(), 33);
        assertEquals(noti[0].getFkRefBranch().intValue(), 44);
        assertEquals(noti[0].getFkRefNotificationStatus().intValue(), 55);
        assertEquals(noti[0].getFkRefGridTerritory().intValue(),1);
    }

    @Test
    public void testSetters() {
        Date aDate = new Date(1000L * 60 * 60 * 24 * 365 * 5);
        Notification noti = new Notification();
        noti.setId(1);
        assertEquals((int) noti.getId(), 1);

        noti.setIncidentId(2);
        assertEquals(2, (int) noti.getIncidentId());

        noti.setSelected(false);
        assertFalse(noti.isSelected());

        noti.setStatus("offen");
        assertTrue(noti.getStatus().equals("offen"));

        noti.setNotificationText("Fehler Bereich D");
        assertTrue(noti.getNotificationText().equals("Fehler Bereich D"));

        noti.setFreeText("free Willi");
        assertTrue(noti.getFreeText().equals("free Willi"));

        noti.setResponsibilityForwarding("fff");
        assertTrue(noti.getResponsibilityForwarding().equals("fff"));

        noti.setResponsibilityControlPoint("Abteilung 4");
        assertTrue(noti.getResponsibilityControlPoint().equals("Abteilung 4"));

        noti.setReminderDate(aDate);
        assertEquals(aDate, noti.getReminderDate());

        Date now = new Date(System.currentTimeMillis());
        noti.setFutureDate(now);
        assertEquals(noti.getFutureDate(), now);

        noti.setCreateDate(aDate);
        assertEquals(aDate, noti.getCreateDate());

        noti.setExpectedFinishDate(now);
        assertEquals(noti.getExpectedFinishDate(), now);

        noti.setFinishedDate(aDate);
        assertEquals(aDate, noti.getFinishedDate());

        noti.setCreateUser("MasterAndServant");
        assertTrue(noti.getCreateUser().equals("MasterAndServant"));

        noti.setModDate(now);
        assertEquals(noti.getModDate(), now);

        noti.setModUser("Modmod");
        assertEquals("Modmod", noti.getModUser());

        noti.setVersion(77);
        assertEquals((int) noti.getVersion(), 77);

        noti.setFkRefBranch(555);
        assertEquals(555, noti.getFkRefBranch().intValue());

        noti.setFkRefNotificationStatus(666);
        assertEquals(666, noti.getFkRefNotificationStatus().intValue());

        noti.setBeginDate(aDate);
        assertEquals(aDate, noti.getBeginDate());

        noti.setNotificationList(new ArrayList<Notification>());
        assertTrue(noti.getNotificationList().isEmpty());

    }
}
