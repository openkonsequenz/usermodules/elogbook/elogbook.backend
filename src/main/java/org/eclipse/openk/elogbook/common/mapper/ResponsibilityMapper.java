/**
******************************************************************************
* Copyright © 2017-2018 PTA GmbH.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* 
*     http://www.eclipse.org/legal/epl-v10.html
* 
******************************************************************************
*/
package org.eclipse.openk.elogbook.common.mapper;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.eclipse.openk.elogbook.common.Globals;
import org.eclipse.openk.elogbook.exceptions.BtbGone;
import org.eclipse.openk.elogbook.persistence.model.TblResponsibility;
import org.eclipse.openk.elogbook.viewmodel.Responsibility;
import org.eclipse.openk.elogbook.viewmodel.TerritoryResponsibility;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class ResponsibilityMapper {

  private static final Logger LOGGER = LogManager.getLogger(ResponsibilityMapper.class.getName());
  public enum PROCESS_MODE {PLAN, CONFIRM, FETCH}

  public Responsibility mapToVModelForContainer(TblResponsibility tblResponsibility) {

         Responsibility responsibility = new Responsibility();
         responsibility.setId(tblResponsibility.getId());
         responsibility.setResponsibleUser(tblResponsibility.getResponsibleUser());
         responsibility.setNewResponsibleUser(tblResponsibility.getNewResponsibleUser());
         responsibility.setBranchName(tblResponsibility.getRefBranch().getName());
         responsibility.setIsActive(true);

         return responsibility;
  }


  public TerritoryResponsibility mapToContainerVModel(TblResponsibility tblResponsibility, TerritoryResponsibility existingResponsibilityCont) {
    TerritoryResponsibility vmResponsibilityRet;

    if (existingResponsibilityCont == null) {
      vmResponsibilityRet = new TerritoryResponsibility();
      vmResponsibilityRet.setGridTerritoryDescription(tblResponsibility.getRefGridTerritory().getDescription());
      List<Responsibility> responsibilityList = new ArrayList<>();
      responsibilityList.add(mapToVModelForContainer(tblResponsibility));
      vmResponsibilityRet.setResponsibilityList(responsibilityList);
    } else {
      vmResponsibilityRet = existingResponsibilityCont;
      vmResponsibilityRet.getResponsibilityList().add(mapToVModelForContainer(tblResponsibility));
    }

    return vmResponsibilityRet;
  }

  public List<TerritoryResponsibility> mapToContainerVModelList(List<TblResponsibility> tblResponsibilityList) {
    LOGGER.debug("mapToContainerVModelList() is called");

    Map<String, TerritoryResponsibility> responsibilityHashMap = new LinkedHashMap<>();

    for (TblResponsibility tblResponsibility : tblResponsibilityList) {
      String tblRespLocation = tblResponsibility.getRefGridTerritory().getDescription();
      if (responsibilityHashMap.containsKey(tblRespLocation)) {
        TerritoryResponsibility existingResponsibility = responsibilityHashMap.get(tblRespLocation);
        mapToContainerVModel(tblResponsibility, existingResponsibility);
      } else {
        responsibilityHashMap.put(tblRespLocation, mapToContainerVModel(tblResponsibility, null));
      }
    }

    List<TerritoryResponsibility> responsibilityList = new ArrayList<>(responsibilityHashMap.values());

    LOGGER.debug("mapToContainerVModelList() is finished");
    return responsibilityList;
  }

  private boolean plannedResponsibilitiesCheck(TblResponsibility tblResponsibility, Responsibility responsibility, String gridLocationJson){
       return (!tblResponsibility.getRefGridTerritory().getDescription().equals(gridLocationJson) ||
        !tblResponsibility.getRefBranch().getName().equals(responsibility.getBranchName()) ||
        !tblResponsibility.getResponsibleUser().equals(responsibility.getResponsibleUser()));
  }

  private boolean confirmResponsibilitiesCheck(TblResponsibility tblResponsibility, Responsibility responsibility, String gridLocationJson){
    return (!tblResponsibility.getRefGridTerritory().getDescription().equals(gridLocationJson) ||
        !tblResponsibility.getRefBranch().getName().equals(responsibility.getBranchName()));
  }

  private TblResponsibility processResponsibilitiesFetch(Responsibility viewModelResp, Map<Integer,
          TblResponsibility> id2RespMap, String moduser) {

    TblResponsibility tblResponsibility = id2RespMap.get(viewModelResp.getId());

    String newResponsibleUser;
    if(isResponsibilityModifiedDuringFetch(viewModelResp, tblResponsibility, moduser)){

      newResponsibleUser = viewModelResp.getNewResponsibleUser();
      tblResponsibility.setNewResponsibleUser(newResponsibleUser);

      Timestamp now = new Timestamp(System.currentTimeMillis());
      tblResponsibility.setModDate(now);
      tblResponsibility.setModUser(moduser);

      return tblResponsibility;
    }
    return null;
  }

  private boolean isResponsibilityModifiedDuringFetch(Responsibility viewModelResp,
                                                      TblResponsibility tblResponsibility, String moduser) {
    if( tblResponsibility == null) {
      return false;
    }

    boolean ret = true;

    String viewModelNewRespUser = viewModelResp.getNewResponsibleUser() == null ? "" : viewModelResp.getNewResponsibleUser();
    String tblNewRespUser = tblResponsibility.getNewResponsibleUser() == null ? "" : tblResponsibility.getNewResponsibleUser();
    String tblResponsibleUser = tblResponsibility.getResponsibleUser() == null ? "" : tblResponsibility.getResponsibleUser();

    ret &= (viewModelNewRespUser.isEmpty() || viewModelNewRespUser.equalsIgnoreCase( moduser ));
    ret &= !viewModelNewRespUser.equalsIgnoreCase(tblResponsibleUser); // the new user must be different to the current responsibility
    ret &= !viewModelNewRespUser.equalsIgnoreCase(tblNewRespUser);  // new user must be different to the current planned one

    return ret;
  }

  private TblResponsibility processResponsibilities(Responsibility viewModelResp, Map<Integer, TblResponsibility> id2RespMap , PROCESS_MODE mode, String gridLocationJson, String moduser)
          throws BtbGone {
    TblResponsibility tblResponsibility = id2RespMap.get(viewModelResp.getId());

    if (tblResponsibility == null ) {
        // In fetch mode it is normal, that the user does not have the new responsibility
        throw new BtbGone(Globals.DATA_OUTDATED);
    }

    switch( mode ) {
      case CONFIRM:
        processResponsibilityConfirm(viewModelResp, gridLocationJson, tblResponsibility);
        break;
      case PLAN:
      default:
        processResponsibilityPlan(viewModelResp, gridLocationJson, tblResponsibility);
        break;
    }

    Timestamp now = new Timestamp(System.currentTimeMillis());
    tblResponsibility.setModDate(now);
    tblResponsibility.setModUser(moduser);

    return tblResponsibility;
  }

  private void processResponsibilityConfirm(Responsibility viewModelResp, String gridLocationJson, TblResponsibility tblResponsibility) throws BtbGone {
    if (confirmResponsibilitiesCheck(tblResponsibility, viewModelResp, gridLocationJson)){
      //Data is outdated, we  have to reload and fill the form with the newer, recent data
      throw new BtbGone(Globals.DATA_OUTDATED);
    }

    if (viewModelResp.isActive()) {
      tblResponsibility.setResponsibleUser(viewModelResp.getNewResponsibleUser());
    }
    // else : user declined to take over the responsibility, old user is still responsible

    tblResponsibility.setNewResponsibleUser(null);
  }

  private void processResponsibilityPlan(Responsibility viewModelResp, String gridLocationJson, TblResponsibility tblResponsibility) throws BtbGone {
    if (plannedResponsibilitiesCheck(tblResponsibility, viewModelResp, gridLocationJson)){
      //Data is outdated, we  have to reload and fill the form with the newer, recent data
      throw new BtbGone(Globals.DATA_OUTDATED);
    }
    String newResponsibleUser;
    if(viewModelResp.getNewResponsibleUser()!=null && viewModelResp.getNewResponsibleUser().isEmpty()){
      newResponsibleUser = null;
    } else {
      newResponsibleUser = viewModelResp.getNewResponsibleUser();
    }
    tblResponsibility.setNewResponsibleUser(newResponsibleUser);
  }

  public List<TblResponsibility> mapFromVModelList(List<TerritoryResponsibility> viewModelTerrRespList,
                                                   List<TblResponsibility> dbRespListForCurrUser,
                                                   PROCESS_MODE mode,
                                                   String moduser) throws BtbGone {
    LOGGER.debug("mapFromVModelList() is called");

    Map<Integer, TblResponsibility> id2RespMap = new LinkedHashMap<>();
    for (TblResponsibility currTblRespIt : dbRespListForCurrUser) {
      id2RespMap.put(currTblRespIt.getId(),currTblRespIt);
    }

    List<TblResponsibility> retTblRespList = new ArrayList<>();

    for (TerritoryResponsibility viewModelTerrIt : viewModelTerrRespList) {
      String gridLocationJson = viewModelTerrIt.getGridTerritoryDescription();

      List<Responsibility> vmRespList = viewModelTerrIt.getResponsibilityList();
      for (Responsibility vmRespIt: vmRespList) {
        TblResponsibility newItem = processResponsibilities(vmRespIt, id2RespMap, mode, gridLocationJson, moduser);
        if( newItem != null ) { // NOSONAR
          retTblRespList.add(newItem);
        }
      }
    }

    if (id2RespMap.size() != retTblRespList.size()) {
      //current user has gained or has lost responsibilities -> outdated
      throw new BtbGone(Globals.DATA_OUTDATED);
    }

    LOGGER.debug("mapFromVModelList() is finished");
    return retTblRespList;
  }


  public List<TblResponsibility> mapFetchFromVModelList(List<TerritoryResponsibility> viewModelTerrRespList,
                                                   List<TblResponsibility> dbAllResp,
                                                   String moduser) {
    LOGGER.debug("mapFetchFromVModelList() is called");

    Map<Integer, TblResponsibility> id2RespMap = new LinkedHashMap<>();
    for (TblResponsibility currTblRespIt : dbAllResp) {
      id2RespMap.put(currTblRespIt.getId(),currTblRespIt);
    }

    List<TblResponsibility> retTblRespList = new ArrayList<>();

    for (TerritoryResponsibility viewModelTerrIt : viewModelTerrRespList) {

      List<Responsibility> vmRespList = viewModelTerrIt.getResponsibilityList();
      for (Responsibility vmRespIt: vmRespList) {
        TblResponsibility newItem = processResponsibilitiesFetch(vmRespIt, id2RespMap, moduser);
        if( newItem != null ) {
          retTblRespList.add(newItem);
        }
      }
    }

    LOGGER.debug("mapFetchFromVModelList() is finished");
    return retTblRespList;
  }

}
