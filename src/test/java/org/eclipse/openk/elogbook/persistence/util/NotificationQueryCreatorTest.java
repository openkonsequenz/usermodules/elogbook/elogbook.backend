/**
******************************************************************************
* Copyright © 2017-2018 PTA GmbH.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* 
*     http://www.eclipse.org/legal/epl-v10.html
* 
******************************************************************************
*/
package org.eclipse.openk.elogbook.persistence.util;

import org.easymock.EasyMock;
import org.eclipse.openk.elogbook.persistence.model.*;
import org.eclipse.openk.elogbook.viewmodel.GlobalSearchFilter;
import org.eclipse.openk.elogbook.viewmodel.Notification.ListType;
import org.eclipse.openk.elogbook.viewmodel.NotificationSearchFilter;
import org.eclipse.openk.elogbook.viewmodel.ReminderSearchFilter;
import org.junit.Before;
import org.junit.Test;
import org.powermock.api.easymock.PowerMock;
import org.powermock.reflect.Whitebox;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import java.sql.Timestamp;
import java.util.*;

import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.assertFalse;
import static org.easymock.EasyMock.expect;
import static org.easymock.EasyMock.replay;
import static org.junit.Assert.assertTrue;

public class NotificationQueryCreatorTest {
	Query mockedQuery;
	TypedQuery<TblNotification> mockedTypedQuery;
	EntityManager mockedEm;
	NotificationQueryCreator notificationQueryCreator;
	Map<String, Object> propertyMap = new HashMap<>();
	
	@Before
	public  void initialize(){
        mockedQuery = PowerMock.createNiceMock(Query.class);
        mockedTypedQuery = PowerMock.createNiceMock(TypedQuery.class);
        mockedEm = PowerMock.createNiceMock(EntityManager.class);
	}

	@Test
    public void testgenerateNotificationQueryListTypeNull() {
    
        expect(mockedQuery.setParameter(EasyMock.anyInt(), EasyMock.anyObject())).andReturn(mockedQuery);
        expect(mockedEm.createNativeQuery(EasyMock.anyString(), EasyMock.eq(TblNotification.class))).andReturn(mockedQuery).anyTimes();
        replay(mockedQuery);
        replay(mockedEm);
		notificationQueryCreator = new NotificationQueryCreator(mockedEm, "");
        NotificationSearchFilter nsf = new NotificationSearchFilter();
        nsf.setDateFrom(new Date(System.currentTimeMillis()));
        nsf.setDateTo(new Date(System.currentTimeMillis()));
        notificationQueryCreator.generateNotificationQuery(nsf, "AAA", new ArrayList<TblResponsibility>());
    }
	
	@Test
    public void testgenerateNotificationQuery_firstTwoArgsEmpty() {
		   
        expect(mockedQuery.setParameter(EasyMock.anyInt(), EasyMock.anyObject())).andReturn(mockedQuery);
        expect(mockedEm.createNativeQuery(EasyMock.anyString(), EasyMock.eq(TblNotification.class))).andReturn(mockedQuery).anyTimes();
        replay(mockedQuery);
        replay(mockedEm);
		notificationQueryCreator = new NotificationQueryCreator(mockedEm, "v");
        NotificationSearchFilter nsf = new NotificationSearchFilter();
        notificationQueryCreator.generateNotificationQuery(nsf, "", new ArrayList<TblResponsibility>());
    }
	
	@Test
    public void testgenerateNotificationQuery_firstTwoArgsNull() {
		   
        expect(mockedQuery.setParameter(EasyMock.anyInt(), EasyMock.anyObject())).andReturn(mockedQuery);
        expect(mockedEm.createNativeQuery(EasyMock.anyString(), EasyMock.eq(TblNotification.class))).andReturn(mockedQuery).anyTimes();
        replay(mockedQuery);
        replay(mockedEm);
		notificationQueryCreator = new NotificationQueryCreator(mockedEm, null);
         notificationQueryCreator.generateNotificationQuery(null, null, new ArrayList<TblResponsibility>());
    }

	@Test
	public void testgenerateNotificationQueryWithReminder() {

		expect(mockedQuery.setParameter(EasyMock.anyInt(), EasyMock.anyObject())).andReturn(mockedQuery);
		expect(mockedEm.createNativeQuery(EasyMock.anyString(), EasyMock.eq(TblNotification.class))).andReturn(mockedQuery).anyTimes();
		replay(mockedQuery);
		replay(mockedEm);
		notificationQueryCreator = new NotificationQueryCreator(mockedEm, "");
		ReminderSearchFilter rsf = new ReminderSearchFilter();
		rsf.setReminderDate(new Date(System.currentTimeMillis()));
		notificationQueryCreator.generateNotificationQueryWithReminder(rsf, "AAA", new ArrayList<TblResponsibility>());
	}

	@Test
	public void testgenerateNotificationQueryWithReminder_firstTwoArgsEmpty() {

		expect(mockedQuery.setParameter(EasyMock.anyInt(), EasyMock.anyObject())).andReturn(mockedQuery);
		expect(mockedEm.createNativeQuery(EasyMock.anyString(), EasyMock.eq(TblNotification.class))).andReturn(mockedQuery).anyTimes();
		replay(mockedQuery);
		replay(mockedEm);
		notificationQueryCreator = new NotificationQueryCreator(mockedEm, "v");
		ReminderSearchFilter rsf = new ReminderSearchFilter();
		notificationQueryCreator.generateNotificationQueryWithReminder(rsf, "", new ArrayList<TblResponsibility>());
	}

	@Test
	public void testgenerateNotificationQueryWithReminder_firstTwoArgsNull() {

		expect(mockedQuery.setParameter(EasyMock.anyInt(), EasyMock.anyObject())).andReturn(mockedQuery);
		expect(mockedEm.createNativeQuery(EasyMock.anyString(), EasyMock.eq(TblNotification.class))).andReturn(mockedQuery).anyTimes();
		replay(mockedQuery);
		replay(mockedEm);
		notificationQueryCreator = new NotificationQueryCreator(mockedEm, null);
		notificationQueryCreator.generateNotificationQueryWithReminder(null, null, new ArrayList<TblResponsibility>());
	}

	@Test
	public void testCreateWhereClauseFromSearchFilter_allEmpty() throws Exception {

		NotificationQueryCreator queryCreator = new NotificationQueryCreator(null, "");
		Map<Integer, Object> paramMap = new HashMap<>();

		NotificationSearchFilter notificationSearchFilter = new NotificationSearchFilter();
		notificationSearchFilter.setDateFrom(null);
		notificationSearchFilter.setDateTo(null);

		String ret = Whitebox.invokeMethod(queryCreator, "extendWhereClauseApplyingSearchFilter", notificationSearchFilter,
				new ArrayList<TblResponsibility>());
		assertEquals(paramMap.size(), 0);
		assertFalse(ret.contains("."));
		assertFalse(ret.contains(">="));
		assertFalse(ret.contains("<="));
	}

	@Test
	public void testCreateWhereClauseFromSearchFilter_allSet() throws Exception {
		NotificationQueryCreator queryCreator = new NotificationQueryCreator(null, "w");

		NotificationSearchFilter nsf = new NotificationSearchFilter();
		nsf.setDateFrom(new Date(System.currentTimeMillis()));
		nsf.setDateTo(new Date(System.currentTimeMillis()));

		String ret = Whitebox.invokeMethod(queryCreator, "extendWhereClauseApplyingSearchFilter", nsf,
				new ArrayList<TblResponsibility>());
		assertTrue(!ret.isEmpty());
	}
	
	@Test
	public void testCreateWhereClauseFromSearchFilter_ListTypePAST() throws Exception {
		NotificationQueryCreator queryCreator = new NotificationQueryCreator(null, "w");

		NotificationSearchFilter nsf = new NotificationSearchFilter();
		nsf.setDateFrom(null);
		nsf.setDateTo(null);

		String ret = Whitebox.invokeMethod(queryCreator, "extendWhereClauseApplyingSearchFilter", nsf,
				new ArrayList<TblResponsibility>());
		assertTrue(! ret.isEmpty());
	}
	
	@Test
	public void testCreateWhereClauseFromSearchFilter_ListTypeFUTURE() throws Exception {
		NotificationQueryCreator queryCreator = new NotificationQueryCreator(null, "w");

		NotificationSearchFilter nsf = new NotificationSearchFilter();
		nsf.setDateFrom(createDate(2017, 3, 8));
		nsf.setDateTo(createDate(2017,4,2));

		String ret = Whitebox.invokeMethod(queryCreator, "extendWhereClauseApplyingSearchFilter", nsf,
				new ArrayList<TblResponsibility>());
		assertTrue("AND w.begin_date >= ? AND w.begin_date <= ? AND (fk_ref_branch IS NULL AND fk_ref_grid_territory IS NULL)".equalsIgnoreCase(ret.trim()));
	}

	/**
	 * Notification Filter - dateFrom and dateTo must not be used for ListType OPEN
	 * 
	 * @throws Exception
	 *             if an error occurs
	 */
	@Test
	public void testExtendWhereClauseApplyingSearchFilterListTypeOPEN() throws Exception {
		NotificationQueryCreator queryCreator = new NotificationQueryCreator(null, "w");
		NotificationSearchFilter nsf = new NotificationSearchFilter();
		nsf.setDateFrom(createDate(2017, Calendar.JANUARY, 1));
		nsf.setDateTo(createDate(2017, Calendar.JUNE, 1));

		List<TblResponsibility> tblResponsibilities = new ArrayList<>();
		tblResponsibilities.add(createTblResponsibility(1, 1, 1, 1));
		String ret = Whitebox.invokeMethod(queryCreator, "extendWhereClauseApplyingSearchFilter", nsf,
				tblResponsibilities);
		assertTrue(" AND w.begin_date >= ? AND w.begin_date <= ? AND (fk_ref_branch IS NULL AND fk_ref_grid_territory IS NULL OR ((fk_ref_branch IS NULL OR fk_ref_branch = 1) AND (fk_ref_grid_territory IS NULL OR fk_ref_grid_territory = 1)))".equals(ret));
	}

	/**
	 * Notification Filter - dateFrom and dateTo must  be used for ListType PAST
	 * @throws Exception if an error occurs
	 */
	@Test
	public void testExtendWhereClauseApplyingSearchFilterListTypePAST() throws Exception {
		NotificationQueryCreator queryCreator = new NotificationQueryCreator(null, "w");
		NotificationSearchFilter nsf = new NotificationSearchFilter();
		nsf.setDateFrom(createDate(2017, Calendar.JANUARY, 1));
		nsf.setDateTo(createDate(2017, Calendar.JUNE, 1));

		String ret = Whitebox.invokeMethod(queryCreator, "extendWhereClauseApplyingSearchFilter", nsf,
				new ArrayList<TblResponsibility>());
		assertTrue(" AND w.begin_date >= ? AND w.begin_date <= ? AND (fk_ref_branch IS NULL AND fk_ref_grid_territory IS NULL)".equals(ret));
	}

	/**
	 * Notification Filter - dateFrom and dateTo must  be used for ListType PAST
	 * @throws Exception if an error occurs
	 */
	@Test
	public void testExtendWhereClauseApplyingSearchFilterListWithResponsibilities() throws Exception {
		NotificationQueryCreator queryCreator = new NotificationQueryCreator(null, "w");
		NotificationSearchFilter nsf = new NotificationSearchFilter();
		List<TblResponsibility> tblResponsibilities = new ArrayList<>();
		tblResponsibilities.add(createTblResponsibility(1, 7, 9, 1));
		tblResponsibilities.add(createTblResponsibility(2, 7, 13, 2));
		tblResponsibilities.add(createTblResponsibility(3, 7, 14, 2));

		String ret = Whitebox.invokeMethod(queryCreator, "extendWhereClauseApplyingSearchFilter", nsf,
				tblResponsibilities);
		StringBuilder sb = new StringBuilder("( AND fk_ref_branch IS NULL AND fk_ref_grid_territory IS NULL OR ");
			sb.append("((fk_ref_branch IS NULL OR fk_ref_branch = 7) AND (fk_ref_grid_territory IS NULL OR fk_ref_grid_territory = 1) OR ")
			  .append("(fk_ref_branch IS NULL OR fk_ref_branch = 7) AND (fk_ref_grid_territory IS NULL OR fk_ref_grid_territory = 2) OR ")
			  .append("(fk_ref_branch IS NULL OR fk_ref_branch = 7) AND (fk_ref_grid_territory IS NULL OR fk_ref_grid_territory = 2)))");
		assertTrue((sb.toString()).length() == ret.length());
			
	}

	@Test
	public void testGenerateFindHistoricalNotificationsByResponsibilityQueryOPEN() {
		HTblResponsibility hTblResponsibility = createHTblResponsibility(1, 2);
		List<HTblResponsibility> hTblResponsibilities = new ArrayList<>();
		hTblResponsibilities.add(hTblResponsibility);

		expect(mockedQuery.setParameter(EasyMock.anyInt(), EasyMock.anyObject())).andReturn(mockedQuery);
		expect(mockedEm.createNativeQuery(EasyMock.anyString(), EasyMock.eq(TblNotification.class))).andReturn(mockedQuery).anyTimes();
		replay(mockedQuery);
		replay(mockedEm);
		notificationQueryCreator = new NotificationQueryCreator(mockedEm, "");
		notificationQueryCreator.generateFindHistoricalNotificationsByResponsibilityQuery(hTblResponsibilities,
				ListType.OPEN);
	}
	
	@Test
	public void testGenerateFindHistoricalNotificationsByResponsibilityQueryPAST() {
		HTblResponsibility hTblResponsibility = createHTblResponsibility(1, 2);
		List<HTblResponsibility> hTblResponsibilities = new ArrayList<>();
		hTblResponsibilities.add(hTblResponsibility);

		expect(mockedQuery.setParameter(EasyMock.anyInt(), EasyMock.anyObject())).andReturn(mockedQuery);
		expect(mockedEm.createNativeQuery(EasyMock.anyString(), EasyMock.eq(TblNotification.class))).andReturn(mockedQuery).anyTimes();
		replay(mockedQuery);
		replay(mockedEm);
		notificationQueryCreator = new NotificationQueryCreator(mockedEm, "");
		notificationQueryCreator.generateFindHistoricalNotificationsByResponsibilityQuery(hTblResponsibilities,
				ListType.PAST);
	}
	
	@Test
	public void testGenerateFindHistoricalNotificationsByResponsibilityQueryFUTURE() {
		HTblResponsibility hTblResponsibility = createHTblResponsibility(1, 2);
		List<HTblResponsibility> hTblResponsibilities = new ArrayList<>();
		hTblResponsibilities.add(hTblResponsibility);

		expect(mockedQuery.setParameter(EasyMock.anyInt(), EasyMock.anyObject())).andReturn(mockedQuery);
		expect(mockedEm.createNativeQuery(EasyMock.anyString(), EasyMock.eq(TblNotification.class))).andReturn(mockedQuery).anyTimes();
		replay(mockedQuery);
		replay(mockedEm);
		notificationQueryCreator = new NotificationQueryCreator(mockedEm, "");
		notificationQueryCreator.generateFindHistoricalNotificationsByResponsibilityQuery(hTblResponsibilities,
				ListType.FUTURE);
	}
	
	@Test
	public void testGenerateFindHistoricalNotificationsByResponsibilityQueryDefault() {
		HTblResponsibility hTblResponsibility = createHTblResponsibility(1, 2);
		List<HTblResponsibility> hTblResponsibilities = new ArrayList<>();
		hTblResponsibilities.add(hTblResponsibility);

		expect(mockedQuery.setParameter(EasyMock.anyInt(), EasyMock.anyObject())).andReturn(mockedQuery);
		expect(mockedEm.createNativeQuery(EasyMock.anyString(), EasyMock.eq(TblNotification.class))).andReturn(mockedQuery).anyTimes();
		replay(mockedQuery);
		replay(mockedEm);
		notificationQueryCreator = new NotificationQueryCreator(mockedEm, "");
		notificationQueryCreator.generateFindHistoricalNotificationsByResponsibilityQuery(hTblResponsibilities,
				ListType.ALL);
	}
	
/** Testing  generateFindNotificationsMatchingSearchCriteriaQuery  - Begin -*/	
	
	@Test
	public void testGsfWithDefaultValues() {
		GlobalSearchFilter gsf = new GlobalSearchFilter();
		mockTypedQuery(gsf);
	}
	
	@Test
	public void testGsfWithInitialValues() {
		GlobalSearchFilter gsf = new GlobalSearchFilter();
		gsf.setFastSearchSelected(Boolean.TRUE);
		gsf.setFkRefBranch(null);
		gsf.setFkRefGridTerritory(null);
		gsf.setResponsibilityForwarding(null);
		gsf.setSearchString(null);
		gsf.setStatusClosedSelection(Boolean.TRUE);
		gsf.setStatusDoneSelection(Boolean.TRUE);
		gsf.setStatusInWorkSelection(Boolean.TRUE);
		gsf.setStatusOpenSelection(Boolean.TRUE);
		mockTypedQuery(gsf);
	}
	
	@Test
	public void testGsfWithRandomSelection() {
		GlobalSearchFilter gsf = new GlobalSearchFilter();
		gsf.setFastSearchSelected(Boolean.FALSE);
		gsf.setFkRefBranch(1);
		gsf.setFkRefGridTerritory(2);
		gsf.setResponsibilityForwarding("Maier");
		gsf.setSearchString("Tag");
		gsf.setStatusClosedSelection(Boolean.FALSE);
		gsf.setStatusDoneSelection(Boolean.FALSE);
		gsf.setStatusInWorkSelection(Boolean.FALSE);
		gsf.setStatusOpenSelection(Boolean.TRUE);
		mockTypedQuery(gsf);
	}
	
	private void mockTypedQuery (GlobalSearchFilter gsf) {
		expect(mockedTypedQuery.setParameter(EasyMock.anyInt(), EasyMock.anyObject())).andReturn(mockedTypedQuery);
		expect(mockedEm.createQuery(EasyMock.anyString(), EasyMock.eq(TblNotification.class))).andReturn(mockedTypedQuery).anyTimes();
		replay(mockedTypedQuery);
		replay(mockedEm);
		notificationQueryCreator = new NotificationQueryCreator(mockedEm, "");
		notificationQueryCreator.generateFindNotificationsMatchingSearchCriteriaQuery(gsf);
	}

	/** Testing generateFindNotificationsMatchingSearchCriteriaQuery  - End - */	
 
	private Date createDate(int year, int month, int day) {
		 Calendar calendar = Calendar.getInstance();
		 calendar.set(year,  month, day);
		 return calendar.getTime();
	}
	
	private Timestamp createTimestamp(int year, int month, int day, int hour, int min, int sec) {

		Calendar calendar = Calendar.getInstance();
		calendar.set(Calendar.YEAR, year);
		calendar.set(Calendar.MONTH, month);
		calendar.set(Calendar.DAY_OF_MONTH, day);
		calendar.set(Calendar.HOUR, hour);
		calendar.set(Calendar.MINUTE, min);
		calendar.set(Calendar.SECOND, 0);
		calendar.set(Calendar.MILLISECOND, sec);
		Date date = calendar.getTime();
		return new Timestamp(date.getTime());
	}
	
	private HTblResponsibility createHTblResponsibility(int fkBranch, int fkGridTerritory) {
		 RefBranch refBranch = new RefBranch();
		 refBranch.setId(fkBranch);
		 RefGridTerritory refGridTerritory = new RefGridTerritory();
		 refGridTerritory.setId(fkGridTerritory);
		 HTblResponsibility hTblResponsibility = new HTblResponsibility();
		 hTblResponsibility.setRefBranch(refBranch);
		 hTblResponsibility.setRefGridTerritory(refGridTerritory);
		 hTblResponsibility.setTransferDate(createTimestamp(2017,8,17,7,0,0));
		 return hTblResponsibility;
	}

	private TblResponsibility createTblResponsibility(Integer id, Integer branchId, Integer gridTerritoryId, Integer fkMaster) {
		 TblResponsibility tblResponsibility = new TblResponsibility();
		 tblResponsibility.setId(id);
		 RefBranch branch = new RefBranch();
		 branch.setId(branchId);
		 tblResponsibility.setRefBranch(branch);
		 RefGridTerritory gridTerritory = new RefGridTerritory();
		 gridTerritory.setId(gridTerritoryId);
		 Whitebox.setInternalState(gridTerritory, "fkRefMaster", fkMaster);
		 tblResponsibility.setRefGridTerritory(gridTerritory);
		 return tblResponsibility;
	}

}
