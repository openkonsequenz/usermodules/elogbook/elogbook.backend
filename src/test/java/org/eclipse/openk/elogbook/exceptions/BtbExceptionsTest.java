/**
******************************************************************************
* Copyright © 2017-2018 PTA GmbH.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* 
*     http://www.eclipse.org/legal/epl-v10.html
* 
******************************************************************************
*/
package org.eclipse.openk.elogbook.exceptions;

import org.apache.http.HttpStatus;
import org.eclipse.openk.elogbook.viewmodel.ErrorReturn;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class BtbExceptionsTest {

    @Test
    public void testConstructors() {
        assertEquals(new BtbBadRequest().getHttpStatus(), HttpStatus.SC_BAD_REQUEST);
        assertEquals(new BtbConflict().getHttpStatus(), HttpStatus.SC_CONFLICT);
        assertEquals(new BtbForbidden().getHttpStatus(), HttpStatus.SC_FORBIDDEN);
        assertEquals(new BtbGone().getHttpStatus(), HttpStatus.SC_GONE);
        assertEquals(new BtbInternalServerError(null, null).getHttpStatus(), HttpStatus.SC_INTERNAL_SERVER_ERROR);
        assertEquals(new BtbLocked().getHttpStatus(), HttpStatus.SC_LOCKED);
        assertEquals(new BtbNotFound().getHttpStatus(), HttpStatus.SC_NOT_FOUND);
        assertEquals(new BtbPolicyNotFulfilled().getHttpStatus(), HttpStatus.SC_METHOD_FAILURE);
        assertEquals(new BtbServiceUnavailable().getHttpStatus(), HttpStatus.SC_SERVICE_UNAVAILABLE);
        assertEquals(new BtbUnauthorized().getHttpStatus(), HttpStatus.SC_UNAUTHORIZED);
    }


    @Test
    public void testConstructors2() {
        final String extext = "ExText";
        assertEquals(new BtbBadRequest(extext).getMessage(), extext);
        assertEquals(new BtbConflict(extext).getMessage(), extext);
        assertEquals(new BtbForbidden(extext).getMessage(), extext);
        assertEquals(new BtbGone(extext).getMessage(), extext);
        assertEquals(new BtbInternalServerError(extext).getMessage(), extext);
        assertEquals(new BtbLocked(extext).getMessage(), extext);
        assertEquals(new BtbNotFound(extext).getMessage(), extext);
        assertEquals(new BtbPolicyNotFulfilled(extext).getMessage(), extext);
        assertEquals(new BtbServiceUnavailable(extext).getMessage(), extext);
        assertEquals(new BtbUnauthorized(extext).getMessage(), extext);
    }
    
    @Test
    public void testBtBNestedExceptions() {
    	 ErrorReturn errorReturn = new ErrorReturn();
         errorReturn.setErrorText("this is an error");
         errorReturn.setErrorCode(404);
         assertEquals(new BtbNestedException(errorReturn).getMessage(), "this is an error");
         assertEquals(new BtbNestedException(errorReturn, new Throwable()).getMessage(), "this is an error");
         BtbNestedException exception = new BtbNestedException(errorReturn);
         assertEquals(exception.getHttpStatus(), 404);
    }
}
