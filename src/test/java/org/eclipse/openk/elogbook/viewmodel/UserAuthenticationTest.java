/**
******************************************************************************
* Copyright © 2017-2018 PTA GmbH.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* 
*     http://www.eclipse.org/legal/epl-v10.html
* 
******************************************************************************
*/
package org.eclipse.openk.elogbook.viewmodel;


import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.eclipse.openk.elogbook.common.JsonGeneratorBase;
import org.eclipse.openk.elogbook.common.util.ResourceLoaderBase;
import org.junit.Test;

public class UserAuthenticationTest extends ResourceLoaderBase {

    @Test
    public void testStructureAgainstJson() {
        String json = super.loadStringFromResource("testUserAuthentication.json");
        UserAuthentication ua = JsonGeneratorBase.getGson().fromJson(json, UserAuthentication.class);
        assertEquals("1", ua.getId());
        assertEquals("Pedro", ua.getUsername());
        assertEquals("pwd", ua.getPassword());
        assertEquals("Pedro Pepito Sanchez", ua.getName());
        assertFalse(ua.isSpecialUser());
        assertTrue(ua.isSelected());
    }

    @Test
    public void testSetters() {
        UserAuthentication ua = new UserAuthentication();
        ua.setId("1");
        ua.setName("Vamos-Namos");
        ua.setPassword("passoporto");
        ua.setUsername("VNAMOS");
        ua.setSpecialUser(true);
        ua.setSelected(true);
    }

}
