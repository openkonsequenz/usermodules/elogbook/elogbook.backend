/**
******************************************************************************
* Copyright © 2017-2018 PTA GmbH.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* 
*     http://www.eclipse.org/legal/epl-v10.html
* 
******************************************************************************
*/
package org.eclipse.openk.elogbook.persistence.dao;

import org.eclipse.openk.elogbook.common.BackendConfig;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class EntityHelper {
    private static EntityManagerFactory entityManagerFactory;
    private static EntityHelper instance;
    private boolean useQuotes = false;

    private EntityHelper() {
    }

    public static EntityHelper instance() {
        if( instance == null ) {
            instance = new EntityHelper();

            try {
                String param = (String) getEMF().getProperties().get("hibernate.globally_quoted_identifiers");
                instance.useQuotes = "TRUE".equalsIgnoreCase(param);
            }
            catch( Exception e) {
                instance.useQuotes = false; // NOSONAR : This behavior is be design _fd
            }
        }
        return instance;
    }

    public static synchronized EntityManagerFactory getEMF() {
        if (entityManagerFactory == null) {
            entityManagerFactory = Persistence.createEntityManagerFactory(BackendConfig.getInstance().getActivePersistencyUnit());
        }
        return entityManagerFactory;
    }

    public String makeIdentifier( String ident ) {
        if (useQuotes) {
            return "\"" + ident + "\"";
        } else {
            return ident;
        }
    }
}
