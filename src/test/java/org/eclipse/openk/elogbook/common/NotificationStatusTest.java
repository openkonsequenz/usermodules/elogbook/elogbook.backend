/**
******************************************************************************
* Copyright © 2017-2018 PTA GmbH.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* 
*     http://www.eclipse.org/legal/epl-v10.html
* 
******************************************************************************
*/
package org.eclipse.openk.elogbook.common;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class NotificationStatusTest {

	@Test
	public void createNotificationStatus() throws Exception {

        assertEquals(0, NotificationStatus.UNKNOWN.id);
        assertEquals(1, NotificationStatus.OPEN.id);
        assertEquals(2, NotificationStatus.INPROGRESS.id);
        assertEquals(3, NotificationStatus.FINISHED.id);
        assertEquals(4, NotificationStatus.CLOSED.id);

    }

}
