package org.eclipse.openk.elogbook.common.timer;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.eclipse.openk.elogbook.common.BackendConfig;
import org.quartz.CronScheduleBuilder;
import org.quartz.JobBuilder;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.Trigger;
import org.quartz.TriggerBuilder;
import org.quartz.impl.StdSchedulerFactory;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;

public class InitContactSyncJobServlet extends HttpServlet {

    private static final long serialVersionUID = -7802116179801471578L;

    private static final Logger LOGGER = LogManager.getLogger(InitContactSyncJobServlet.class.getName());

    @Override
    public void init() throws ServletException {
        String cronExpression = BackendConfig.getInstance().getCronSyncContacts();
        boolean isJobEnabled = BackendConfig.getInstance().isEnableContactSyncJob();
        if (isJobEnabled) {
            LOGGER.info("Init timer: 'Check contact sync job' triggered with cronExpression: " + cronExpression);

            Trigger trigger = TriggerBuilder.newTrigger().withIdentity("CheckContactSyncTrigger", "CheckContactSyncTriggerGroup1")
                    .withSchedule(CronScheduleBuilder.cronSchedule(cronExpression)).build();

            Scheduler scheduler;

            try {
                JobDetail contactSyncJob = JobBuilder.newJob(ContactSyncJob.class)
                        .withIdentity("contactSyncJob", "contactSyncJobGroup").build();
                scheduler = new StdSchedulerFactory().getScheduler();
                scheduler.start();
                scheduler.scheduleJob(contactSyncJob, trigger);
            } catch (SchedulerException e) {
                LOGGER.error("SchedulerException caught", e);
            }
        } else {
            LOGGER.info("'Check contact sync job' is disabled");
        }

    }

}
