/**
******************************************************************************
* Copyright © 2017-2018 PTA GmbH.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* 
*     http://www.eclipse.org/legal/epl-v10.html
* 
******************************************************************************
*/
package org.eclipse.openk.elogbook.viewmodel;


import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.Calendar;
import java.util.Date;

import org.eclipse.openk.elogbook.common.JsonGeneratorBase;
import org.eclipse.openk.elogbook.common.util.ResourceLoaderBase;
import org.junit.Test;

public class ResponsibilitySearchFilterTest extends ResourceLoaderBase {

    @Test
   
    public void TestStructureAgainstJson() {
        String json = super.loadStringFromResource("testResponsibilitySearchFilter.json");

        ResponsibilitySearchFilter filter = JsonGeneratorBase.getGson().fromJson(json, ResponsibilitySearchFilter.class);
        Calendar calendar = Calendar.getInstance();
        calendar.set(2017, Calendar.JULY, 17);
        Date compareDate = calendar.getTime();
       
        assertNotNull(filter);
        assertTrue(compareDate.after(filter.getTransferDateFrom()));
        assertTrue(compareDate.before(filter.getTransferDateTo()));
       //assertTrue(calendar.get(Calendar.MONTH).equals(filter.getTransferDateTo().get(Calendar.MONTH)));
 
    }

    @Test
    public void testSetters() {
        Date now = new Date();
        ResponsibilitySearchFilter filter = new ResponsibilitySearchFilter();
        filter.setTransferDateFrom(now);
        filter.setTransferDateTo(now);
        
        assertEquals(now, filter.getTransferDateFrom());
        assertEquals(now, filter.getTransferDateTo());
    }
}
