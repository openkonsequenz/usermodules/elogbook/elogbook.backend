/**
******************************************************************************
* Copyright © 2017-2018 PTA GmbH.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* 
*     http://www.eclipse.org/legal/epl-v10.html
* 
******************************************************************************
*/
package org.eclipse.openk.elogbook.controller;

import org.apache.commons.io.FilenameUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.eclipse.openk.elogbook.common.BackendConfig;
import org.eclipse.openk.elogbook.exceptions.BtbInternalServerError;
import org.eclipse.openk.elogbook.viewmodel.NotificationFile;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.attribute.BasicFileAttributes;
import java.nio.file.attribute.FileOwnerAttributeView;
import java.nio.file.attribute.UserPrincipal;
import java.util.ArrayList;
import java.util.List;

public class BackendControllerNotificationFile {

	private static final Logger LOGGER = LogManager.getLogger(BackendControllerNotification.class.getName());
	private static String pathName = BackendConfig.getInstance().getImportFilesFolderPath();
	private static int fileRowToRead = BackendConfig.getInstance().getFileRowToRead();

	public List<NotificationFile> getNotificationsFiles(String choice) throws BtbInternalServerError {
		LOGGER.debug("getNotificationsFile() is called");
		List<NotificationFile> notificationFiles = new ArrayList<>();

		File folder = new File(pathName);
		File[] listOfFiles = folder.listFiles();

		return processChoiceGetNotificationFilesWithChoice(choice, notificationFiles, listOfFiles);
	}

	private List<NotificationFile> processChoiceGetNotificationFilesWithChoice(String choice, List<NotificationFile> notificationFiles,
			File[] listOfFiles) throws BtbInternalServerError {
		try {
			if ("getImportFiles".equals(choice)) {
				processFileListForGetImportFiles(notificationFiles, listOfFiles);
			}
			else if ("importFile".equals(choice)) {
				processFileListForImportFile(notificationFiles, listOfFiles);
			}
			else {
				LOGGER.debug("False parameter");
			}

			return notificationFiles;

		} catch (IOException e) {
			LOGGER.error("Fehler beim Einlesen einer Datei aus dem Importverzeichnis", e);
			throw new BtbInternalServerError("Fehler beim Einlesen einer Datei aus dem Importverzeichnis");
		}

	}


	private void processFileListForGetImportFiles(List<NotificationFile> notificationFiles, File[] listOfFiles) throws IOException {
		if (listOfFiles != null) {
			for (File file : listOfFiles) {

				if (file.isFile()) {
					Path filePath = Paths.get(file.toString());

					NotificationFile notificationFile = new NotificationFile();
					BasicFileAttributes attr = Files.readAttributes(filePath, BasicFileAttributes.class);

					String fileName = file.getName();
					notificationFile.setFileName(fileName);

					String fileCreationDate = attr.creationTime().toString();
					notificationFile.setCreationDate(fileCreationDate);

					FileOwnerAttributeView ownerAttributeView = Files.getFileAttributeView(filePath, FileOwnerAttributeView.class);
					UserPrincipal owner = ownerAttributeView.getOwner();
					String fileCreator = owner.getName();
					notificationFile.setCreator(fileCreator);

					String fileType = FilenameUtils.getExtension(file.getName());
					notificationFile.setType(fileType);

					Long fileSize = attr.size();
					notificationFile.setSize(fileSize);

					notificationFiles.add(notificationFile);

					LOGGER.info(
							"File " + fileName + " Date: " + fileCreationDate + " Creator: " + fileCreator
									+ " Type: " + fileType + " Size: " + fileSize);

				} else if (file.isDirectory()) {
					LOGGER.info("Directory " + file.getName());
				}
			}
        }
	}


	private void processFileListForImportFile(List<NotificationFile> notificationFiles, File[] listOfFiles)
			throws IOException {
		if (listOfFiles != null) {

			// 17.01.2019 "Leitsystem" is beeing updated and we are waiting for more information to continue here and adjust the import process
			// see also BackendControllerNotificationFile_ImportFilenameAdjusted (reiss)

			for (File file : listOfFiles) {

				if (file.isFile()) {
					Path filePath = Paths.get(file.toString());

					NotificationFile notificationFile = new NotificationFile();
					String fileName = file.getName();

					List<String> lines = Files.readAllLines(filePath, StandardCharsets.ISO_8859_1);
					extractFileData(notificationFile, lines);

					notificationFiles.add(notificationFile);

					LOGGER.info("File " + fileName + " loaded");

				} else if (file.isDirectory()) {
					LOGGER.info("Directory " + file.getName());
				}
			}
		}
	}

	private void extractFileData(NotificationFile notificationFile, List<String> lines) {
		String wholeLine = lines.get(fileRowToRead - 1);

		String[] splitedLine = wholeLine.split("\\s+", 3);

		String tmpBranchAndTerritory = splitedLine[1];
		setBranchAndTerritory(tmpBranchAndTerritory, notificationFile);

		String notificationText = splitedLine[2];
		notificationFile.setNotificationText(notificationText);
	}

	private void setBranchAndTerritory(String input, NotificationFile notificationFile) {

		String tmpTerritory = Character.toString(input.charAt(0));
		String tmpBranch = Character.toString(input.charAt(1));
		String territory = "";
		String branch = "";

		switch (tmpTerritory) {
		case "M":
			territory = "MA";
			break;
		case "O":
			territory = "OF";
			break;
		case "D":
			territory = "DR";
			break;
		default:
			territory = tmpTerritory;
		}

		if ("E".equals(tmpBranch)) {
			branch = "S";
		} else {
			branch = tmpBranch;
		}

		notificationFile.setGridTerritoryName(territory);
		notificationFile.setBranchName(branch);
	}

	public boolean deleteImportedFile(String fileName) {

		File folder = new File(pathName);
		File[] listOfFiles = folder.listFiles();
		boolean deleteStatus = false;

		if (listOfFiles == null) {
			return false;
		}

		for (File listOfFile : listOfFiles) {

			if (listOfFile.getName().equals(fileName)) {
				if (!listOfFile.delete()) {
					LOGGER.warn("Die Datei " + listOfFile.getName() + " konnte nicht gelöscht werden.");
				}
				deleteStatus = true;
			}
		}
		return deleteStatus;
	}

}
