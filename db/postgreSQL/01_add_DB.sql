-- DROP DATABASE betriebstagebuch;

CREATE DATABASE betriebstagebuch
    WITH
    OWNER = postgres
    ENCODING = 'UTF8'
    LC_COLLATE = 'German_Germany.1252'
    LC_CTYPE = 'German_Germany.1252'
    TABLESPACE = pg_default
    CONNECTION LIMIT = -1;

-- Role: btbservice

-- DROP ROLE btbservice;
CREATE ROLE btbservice LOGIN
  ENCRYPTED PASSWORD 'md5014d35e4f8632560130b265acb21413f'
  NOSUPERUSER INHERIT NOCREATEDB NOCREATEROLE NOREPLICATION;
