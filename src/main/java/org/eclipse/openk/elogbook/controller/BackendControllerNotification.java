/**
******************************************************************************
* Copyright © 2017-2018 PTA GmbH.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* 
*     http://www.eclipse.org/legal/epl-v10.html
* 
******************************************************************************
*/

package org.eclipse.openk.elogbook.controller;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.eclipse.openk.elogbook.common.BackendConfig;
import org.eclipse.openk.elogbook.common.JsonGeneratorBase;
import org.eclipse.openk.elogbook.common.NotificationStatus;
import org.eclipse.openk.elogbook.common.mapper.NotificationMapper;
import org.eclipse.openk.elogbook.exceptions.BtbException;
import org.eclipse.openk.elogbook.exceptions.BtbInternalServerError;
import org.eclipse.openk.elogbook.exceptions.BtbLocked;
import org.eclipse.openk.elogbook.persistence.dao.AutoCloseEntityManager;
import org.eclipse.openk.elogbook.persistence.dao.EntityHelper;
import org.eclipse.openk.elogbook.persistence.dao.HTblResponsibilityDao;
import org.eclipse.openk.elogbook.persistence.dao.RefBranchDao;
import org.eclipse.openk.elogbook.persistence.dao.RefGridTerritoryDao;
import org.eclipse.openk.elogbook.persistence.dao.RefNotificationPriorityDao;
import org.eclipse.openk.elogbook.persistence.dao.RefNotificationStatusDao;
import org.eclipse.openk.elogbook.persistence.dao.TblNotificationDao;
import org.eclipse.openk.elogbook.persistence.dao.TblResponsibilityDao;
import org.eclipse.openk.elogbook.persistence.model.HTblResponsibility;
import org.eclipse.openk.elogbook.persistence.model.TblNotification;
import org.eclipse.openk.elogbook.persistence.model.TblResponsibility;
import org.eclipse.openk.elogbook.viewmodel.GlobalSearchFilter;
import org.eclipse.openk.elogbook.viewmodel.Notification;
import org.eclipse.openk.elogbook.viewmodel.NotificationSearchFilter;
import org.eclipse.openk.elogbook.viewmodel.ReminderSearchFilter;
import org.eclipse.openk.elogbook.viewmodel.contactbasedata.AssignmentModulContactDto;

import javax.persistence.EntityManager;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

public class BackendControllerNotification {

	private static final Logger LOGGER = LogManager.getLogger(BackendControllerNotification.class.getName());

	private static NotificationMapper notificationMapper = null;

	/**
	 * Get Notifications. If historical flag in the notification search filter is not set, the active notifications are
	 * processed and returned. If the historial flag is set, the notifications active at the given shift change date
	 * (must also be set) are processed and returned.
	 *
	 * @param listType
	 *            the {@link Notification.ListType}
	 * @param nsf
	 *            the {@link NotificationSearchFilter}
	 * @return the list of notifications matching the nfs
	 * @throws BtbException
	 *             if an error occurs.
	 */
	public List<Notification> getNotifications(Notification.ListType listType, NotificationSearchFilter nsf)
			throws BtbException {
		LOGGER.debug("getNotifications(listType) is called");
		LOGGER.debug("-> Type=" + listType.toString());
		LOGGER.debug("-> NotificationSearchFilter=" + JsonGeneratorBase.getGson().toJson(nsf));
		LOGGER.debug("-> Request for Historical Notifications = " + (nsf != null && nsf.isHistoricalFlag()));

		EntityManager emOrg = EntityHelper.getEMF().createEntityManager();
		try (AutoCloseEntityManager em = new AutoCloseEntityManager(emOrg)) {
			TblNotificationDao notificationDao = new TblNotificationDao(em);
			List<TblNotification> notList;
			if (nsf != null && nsf.isHistoricalFlag()) {
				HTblResponsibilityDao historicalResponsibilityDao = new HTblResponsibilityDao(em);
				notList = getHistoricalNotificationListByType(historicalResponsibilityDao, notificationDao, listType,
						nsf);
			} else {
				TblResponsibilityDao responsibilityDao = new TblResponsibilityDao(em);
				notList = getNotificationListByType(responsibilityDao, notificationDao, listType, nsf);
			}
			return getNotificationMapper(em).mapListToVModel(notList);
		} finally {
			LOGGER.debug("getNotifications() is finished");
		}
	}

	/**
	 * Returns only the highest version for each notification.
	 * 
	 * @return List of Active Notifications
	 * @throws BtbException
	 *             Exception of Type BtbException
	 */
	public List<Notification> getActiveNotifications() throws BtbException {
		LOGGER.debug("getActiveNotifications() is called");

		EntityManager emOrg = EntityHelper.getEMF().createEntityManager();
		try (AutoCloseEntityManager em = new AutoCloseEntityManager(emOrg)) {
			TblNotificationDao dao = new TblNotificationDao(em);
			List<TblNotification> notList = dao.getActiveNotifications();
			NotificationMapper mapper = getNotificationMapper(em);
			List<Notification> retList = new ArrayList<>();
			for (TblNotification tblNotification : notList) {
				retList.add(mapper.mapToVModel(tblNotification));
			}
			return retList;
		} finally {
			LOGGER.debug("getActiveNotifications() is finished");
		}

	}

	public List<Notification> getNotificationsWithReminder(ReminderSearchFilter rsf) throws BtbException {
		LOGGER.debug("getNotificationsWithReminder() is called");
		LOGGER.debug("-> NotificationSearchFilter=" + JsonGeneratorBase.getGson().toJson(rsf));

		EntityManager emOrg = EntityHelper.getEMF().createEntityManager();
		try (AutoCloseEntityManager em = new AutoCloseEntityManager(emOrg)) {
			TblNotificationDao dao = new TblNotificationDao(em);

			TblResponsibilityDao responsibilityDao = new TblResponsibilityDao(em);
			List<TblNotification> tblNotifications = getNotificationListWithReminderFilter(responsibilityDao, dao, rsf);

			return getNotificationMapper(em).mapListToVModel(tblNotifications);
		} finally {
			LOGGER.debug("getNotificationsWithReminder() is finished");
		}

	}



	public Notification createNotification(Notification newNotification, String modUser, String token) throws BtbException {
		LOGGER.debug("createNotification is called");

		EntityManager emOrg = EntityHelper.getEMF().createEntityManager();
		try (AutoCloseEntityManager em = new AutoCloseEntityManager(emOrg)) {
			em.getTransaction().begin();

			NotificationMapper mapper = getNotificationMapper(em);
			TblNotificationDao dao = new TblNotificationDao(em);
			if (newNotification.getId() != null) {
				checkBlockedNotification(dao.findByIdInTx(TblNotification.class, newNotification.getId()));
			}
			Notification ret = storeNotificationInDB(newNotification, dao, mapper, modUser);
			em.getTransaction().commit();
			linkUserToModulInContactbaseData(newNotification, token);
			return ret;
		} finally {
			LOGGER.debug("createNotification is finished");
		}


	}

	private void linkUserToModulInContactbaseData(Notification newNotification, String token) throws BtbException {
		UUID responsibilityForwardingUuid = newNotification.getResponsibilityForwardingUuid();
		if (responsibilityForwardingUuid != null) {
			List<AssignmentModulContactDto> modulContactDtoList = ContactBaseDataManager.getInstance().getAssignedModulsForContact(token, responsibilityForwardingUuid);

			AssignmentModulContactDto assignmentModulContactDto =
					modulContactDtoList.stream().filter(modulContactDto -> BackendConfig.getInstance().getElogbookModuleName().equals(modulContactDto.getModulName())).findFirst().orElse(null);

			if (assignmentModulContactDto == null){
				LOGGER.debug("New link with responsibilityForwardingUuid: " + responsibilityForwardingUuid + " to module: " + BackendConfig.getInstance().getElogbookModuleName());
				ContactBaseDataManager.getInstance().postAssignedModulsForContact(token, responsibilityForwardingUuid);
			}
		}
	}

	/**
	 * Loads a Notification from the DB.
	 *
	 * @param id
	 *            - the Primary key of the notification
	 * @return Notification with the given Id
	 */
	public Notification getNotificationById(Integer id) {
		LOGGER.debug("getNotificationById is called");

		EntityManager emOrg = EntityHelper.getEMF().createEntityManager();
		try (AutoCloseEntityManager em = new AutoCloseEntityManager(emOrg)) {
			TblNotificationDao dao = new TblNotificationDao(em);
			return getNotificationMapper(em).mapToVModel(dao.findById(TblNotification.class, id));
		} finally {
			LOGGER.debug("getNotificationById is finished");
		}
	}

	public List<Notification> getNotificationByIncidentId(Integer incidentId) throws BtbInternalServerError {
		LOGGER.debug("getNotificationByIncidentId is called");

		EntityManager emOrg = EntityHelper.getEMF().createEntityManager();
		try (AutoCloseEntityManager em = new AutoCloseEntityManager(emOrg)) {
			TblNotificationDao dao = new TblNotificationDao(em);
			return getNotificationMapper(em).mapListToVModel(dao.getByIncidentId(incidentId));
		} finally {
			LOGGER.debug("getNotificationByIncidentId is finished");
		}
	}

	/**
	 * Get the search results. The search results consists of those notification who match the search criteria in the
	 * given {@link GlobalSearchFilter}.
	 * 
	 * @param gsf
	 *            the {@link GlobalSearchFilter} to define the search criteria.
	 * @return the list of notifications matching the global search filter.
	 * @throws BtbException
	 *             if an error occurs.
	 */
	public List<Notification> getSearchResults(GlobalSearchFilter gsf) throws BtbException {
		LOGGER.debug("getSearchResults(globalSearchFilter) is called");
		LOGGER.debug("-> GlobalSearchFilter=" + JsonGeneratorBase.getGson().toJson(gsf));
		LOGGER.debug("-> Request for Global Search in all available notifications");

		if (gsf == null || (!isSearchStringAvailable(gsf) && !isResponsibilityForwardingAvailable(gsf))) {
			return new ArrayList<>();
		}

		EntityManager emOrg = EntityHelper.getEMF().createEntityManager();
		try (AutoCloseEntityManager em = new AutoCloseEntityManager(emOrg)) {
			return getNotificationMapper(em)
					.mapListToVModel(new TblNotificationDao().findNotificationsMatchingSearchCriteria(gsf));
		} finally {
			LOGGER.debug("getSearchResults() is finished");
		}
	}

	private boolean isSearchStringAvailable(GlobalSearchFilter gsf){
		return gsf.getSearchString() != null && !gsf.getSearchString().isEmpty();
	}

	private boolean isResponsibilityForwardingAvailable(GlobalSearchFilter gsf){
		return gsf.getResponsibilityForwarding() != null && !gsf.getResponsibilityForwarding().isEmpty();
	}


	synchronized NotificationMapper getNotificationMapper(EntityManager em) {
		if (notificationMapper == null) {
			LOGGER.debug("Notificationmapper initializing");
			notificationMapper = new NotificationMapper(new RefNotificationStatusDao(em), new RefBranchDao(em),
					new RefGridTerritoryDao(em), new RefNotificationPriorityDao(em));
		}
		return notificationMapper;
	}

	private List<TblNotification> getNotificationListByType(TblResponsibilityDao responsibilityDao,
			TblNotificationDao notificationDao, Notification.ListType listType,
			NotificationSearchFilter notificationSearchFilter) throws BtbException {

		List<TblResponsibility> tblResponsibilities = new ArrayList<>();
		if (notificationSearchFilter != null && notificationSearchFilter.getResponsibilityFilterList() != null) {
			tblResponsibilities = responsibilityDao
					.findResponsibilitiesByIdList(notificationSearchFilter.getResponsibilityFilterList());
		}

		switch (listType) {
		case PAST:
			return notificationDao.getPastNotifications(notificationSearchFilter, tblResponsibilities);
		case OPEN:
			return notificationDao.getOpenNotifications(notificationSearchFilter, tblResponsibilities);
		case FUTURE:
			return notificationDao.getFutureNotifications(notificationSearchFilter, tblResponsibilities);
		default:
			return Collections.emptyList();
		}
	}

	/**
	 * Get the historical notifications for a given list type and shift transaction id.
	 *
	 * @param hTblResponsibilityDao
	 *            the {@link HTblResponsibilityDao} to obtain responsibility data.
	 * @param notificationDao
	 *            the {@link TblNotificationDao} to access notifications and obtain the result
	 * @param listType
	 *            the {@link Notification.ListType} to define the type of historical notifications
	 * @param notificationSearchFilter
	 *            the filter with historical flag and shift transaction id
	 * @return the historical notifications at shift change with the given list type and transaction id
	 * @throws BtbException
	 *             if an error occurs
	 */
	private List<TblNotification> getHistoricalNotificationListByType(HTblResponsibilityDao hTblResponsibilityDao,
			TblNotificationDao notificationDao, Notification.ListType listType,
			NotificationSearchFilter notificationSearchFilter) throws BtbException {

		List<HTblResponsibility> hTblResponsibilities = hTblResponsibilityDao
				.findResponsibilitiesByTransactionId(notificationSearchFilter.getShiftChangeTransactionId());
		if (hTblResponsibilities.isEmpty()) {
			return new ArrayList<>();
		}
		return notificationDao.findHistoricalNotificationsByResponsibility(hTblResponsibilities, listType);
	}

	public List<TblNotification> getNotificationListWithReminderFilter(TblResponsibilityDao responsibilityDao,
			TblNotificationDao notificationDao, ReminderSearchFilter rsf) throws BtbException {

		List<TblResponsibility> tblResponsibilities = new ArrayList<>();
		if (rsf != null && rsf.getResponsibilityFilterList() != null) {
			tblResponsibilities = responsibilityDao.findResponsibilitiesByIdList(rsf.getResponsibilityFilterList());
		}
		return notificationDao.getNotificationsWithReminder(rsf, tblResponsibilities);
	}

	/**
	 * Implements the storage of the notification and returns the persisted copy...covered by Unittest Note: we do not
	 * update existing entries in tbl_notification. Instead we add 1 to "version" and insert a new entry.
	 *
	 * @param newNotification
	 *            New Notification to store
	 * @param ndao
	 *            NotificationDao bound to EM
	 * @param notificationMapper
	 *            NotificaionMapper Objekt
	 * @return persistedNotification Persisted Notification reloaded from db
	 * @throws BtbInternalServerError
	 *             Exception of Type BtbException
	 */
	private static Notification storeNotificationInDB(Notification newNotification, TblNotificationDao ndao,
			NotificationMapper notificationMapper, String modUser) throws BtbInternalServerError {
		TblNotification storable = notificationMapper.mapFromVModel(newNotification);
		storable.setId(null);
		if (newNotification.getIncidentId() == null) {
			Timestamp ts = new Timestamp(System.currentTimeMillis());
			storable.setVersion(1);
			storable.setCreateDate(ts);
			storable.setModDate(ts);
		} else {
			storable.setModDate(new Timestamp(System.currentTimeMillis()));
			storable.setModUser(modUser);
			storable.setVersion(storable.getVersion() + 1);
		}
		try {
			ndao.persistInTx(storable);
		} catch (Exception e) {
			LOGGER.error("Error storing Notification", e);
			throw new BtbInternalServerError("Error storing Notification, e");
		}

		return notificationMapper.mapToVModel(storable);
	}

	private void checkBlockedNotification(TblNotification existingNotification) throws BtbLocked {
		if (existingNotification != null
				&& existingNotification.getRefNotificationStatus().getId() == NotificationStatus.CLOSED.id) {
			throw new BtbLocked();
		}
	}

}
