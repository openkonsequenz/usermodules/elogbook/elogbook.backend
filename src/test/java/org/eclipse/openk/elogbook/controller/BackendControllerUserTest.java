/**
******************************************************************************
* Copyright © 2017-2018 PTA GmbH.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* 
*     http://www.eclipse.org/legal/epl-v10.html
* 
******************************************************************************
*/
package org.eclipse.openk.elogbook.controller;

import org.eclipse.openk.elogbook.persistence.dao.EntityHelper;
import org.eclipse.openk.elogbook.persistence.dao.TblUserSettingsDao;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.easymock.PowerMock;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.reflect.Whitebox;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import java.util.Optional;

import static org.easymock.EasyMock.anyObject;
import static org.easymock.EasyMock.anyString;
import static org.easymock.EasyMock.expect;
import static org.junit.Assert.assertEquals;

@RunWith(PowerMockRunner.class)
@PowerMockIgnore("jdk.internal.reflect.*")
@PrepareForTest({TblUserSettingsDao.class, BackendControllerUser.class})
public class BackendControllerUserTest {

    @Before
    public void init() throws Exception {
        EntityHelper entityHelper = Whitebox.invokeConstructor(EntityHelper.class);
        Whitebox.setInternalState(EntityHelper.class, "instance", entityHelper);
        EntityManagerFactory emfMock = PowerMock.createMock(EntityManagerFactory.class);
        expect(emfMock.createEntityManager()).andReturn(PowerMock.createNiceMock(EntityManager.class));
        PowerMock.replay( emfMock );
        Whitebox.setInternalState(EntityHelper.class, "entityManagerFactory", emfMock);
    }


    @Test
    public void shouldNotGetUserSettings() throws Exception {
        TblUserSettingsDao usDao = PowerMock.createNiceMock(TblUserSettingsDao.class);
        // no usersettings available
        expect(usDao.getSettingsForUser(anyString(), anyString())).andReturn(Optional.empty());
        PowerMock.replay( usDao );
        PowerMock.expectNew(TblUserSettingsDao.class, anyObject(EntityManager.class)).andReturn(usDao);
        PowerMock.replay(TblUserSettingsDao.class);

        BackendControllerUser bcUser = new BackendControllerUser();
        assertEquals( "{}", bcUser.getUserSettings("bruno") );

    }

}
