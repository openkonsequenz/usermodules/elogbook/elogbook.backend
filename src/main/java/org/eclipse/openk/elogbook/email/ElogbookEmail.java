/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
 */
package org.eclipse.openk.elogbook.email;

import jakarta.mail.MessagingException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.eclipse.openk.elogbook.common.BackendConfig;

import java.util.List;

public class ElogbookEmail {

    protected Email email;

    protected String subject;
    protected String emailBody;

    private static final Logger log = LogManager.getLogger(ElogbookEmail.class.getName());

    public ElogbookEmail(BackendConfig backendConfig) throws MessagingException {
        email = new Email(backendConfig.getEmailSmtpHost(), backendConfig.getEmailSmtpPort(),
                backendConfig.isHtmlEmail());
        email.setFrom(backendConfig.getEmailSender());
    }

    public void setEmailContent(String subject, String textBody) {
        log.debug("loading email content");
        this.emailBody = textBody;
        this.subject = subject == null ? "- kein Betreff -" : subject;
        log.debug("finsished loading email content");
    }

    private void prepareMailToSend() throws MessagingException {
        email.addText(emailBody);
        email.setSubject(subject);
    }

    public void setRecipientsTo(List<String> recipientsTo) throws MessagingException {
        email.addRecipients(recipientsTo);
    }

    public void sendEmail() throws MessagingException {

        try {
            prepareMailToSend();
            boolean ret = email.send();
            if (ret) {
                log.info("Email with subject: " + email.getMessage().getSubject() + " has been successfully sent to smtpserver");
            } else {
                log.error("Some error occured while sending email to smtpserver with subject: " + email.getMessage().getSubject() + " . Email wasn't sent.");
                throw new MessagingException();
            }
        } catch (MessagingException e) {
            log.error("Error occured in sendEmail", e);
            throw e;
        }
    }

}
