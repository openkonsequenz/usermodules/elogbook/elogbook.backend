#!/bin/bash
set -e

psql -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" --dbname "$POSTGRES_DB" <<-EOSQL
CREATE DATABASE betriebstagebuch
    WITH
    OWNER = postgres
    ENCODING = 'UTF8'
    TABLESPACE = pg_default
    CONNECTION LIMIT = -1;

CREATE ROLE btbservice LOGIN
  ENCRYPTED PASSWORD 'md5014d35e4f8632560130b265acb21413f'
  NOSUPERUSER INHERIT NOCREATEDB NOCREATEROLE NOREPLICATION;
EOSQL


#CREATE DATABASE betriebstagebuch
#    WITH
#    OWNER = postgres
#    ENCODING = 'UTF8'
#    LC_COLLATE = 'German_Germany.1252'
#    LC_CTYPE = 'German_Germany.1252'
#    TABLESPACE = pg_default
#    CONNECTION LIMIT = -1;
