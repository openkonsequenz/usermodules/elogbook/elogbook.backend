/**
******************************************************************************
* Copyright © 2017-2018 PTA GmbH.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* 
*     http://www.eclipse.org/legal/epl-v10.html
* 
******************************************************************************
*/
package org.eclipse.openk.elogbook.persistence.dao;

import org.eclipse.openk.elogbook.persistence.model.TblNotificationMailsent;

import javax.persistence.EntityManager;
import javax.persistence.Query;

public class TblNotificationMailsentDao extends GenericDaoJpa<TblNotificationMailsent, Integer> {
	private static final String FROM = " from ";


	public TblNotificationMailsentDao() {
		super();
	}

	public TblNotificationMailsentDao(EntityManager em) {
		super(em);
	}

	@SuppressWarnings("unchecked")
	public TblNotificationMailsent findByIncidentId(Integer incidentId) {

		try {
			String selectString = FROM + TblNotificationMailsent.class.getSimpleName() + " tms where tms.incidentId = :incidentId";
			Query q = getEM().createQuery(selectString);
			q.setParameter("incidentId", incidentId);
			return (TblNotificationMailsent)q.getSingleResult();
		} catch (Throwable t) { //NOSONAR
			LOGGER.error(t.getMessage());
			return null;
		}
	}
}
