/**
******************************************************************************
* Copyright © 2017-2018 PTA GmbH.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* 
*     http://www.eclipse.org/legal/epl-v10.html
* 
******************************************************************************
*/
package org.eclipse.openk.elogbook.controller;


import jakarta.mail.MessagingException;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.eclipse.openk.elogbook.common.BackendConfig;
import org.eclipse.openk.elogbook.common.Globals;
import org.eclipse.openk.elogbook.common.util.ResourceLoaderBase;
import org.eclipse.openk.elogbook.email.ElogbookEmail;
import org.eclipse.openk.elogbook.exceptions.BtbException;
import org.eclipse.openk.elogbook.persistence.dao.AutoCloseEntityManager;
import org.eclipse.openk.elogbook.persistence.dao.EntityHelper;
import org.eclipse.openk.elogbook.persistence.dao.TblNotificationDao;
import org.eclipse.openk.elogbook.persistence.dao.TblNotificationMailsentDao;
import org.eclipse.openk.elogbook.persistence.dao.TblResponsibilityDao;
import org.eclipse.openk.elogbook.persistence.model.TblNotification;
import org.eclipse.openk.elogbook.persistence.model.TblNotificationMailsent;
import org.eclipse.openk.elogbook.viewmodel.Notification;
import org.eclipse.openk.elogbook.viewmodel.ReminderSearchFilter;

import javax.persistence.EntityManager;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class EmailService {
    private static final Logger LOGGER = LogManager.getLogger(EmailService.class.getName());

    private static final EmailService INSTANCE = new EmailService();

    public static final String DATE_PATTERN = "dd.MM.yyyy, HH:mm:ss";

    public static final String NOTIFICATION_TEXT_PLACEHOLDER = "##NOTIFICATION_TEXT###";
    public static final String NOTIFICATION_CREATE_DATE_PLACEHOLDER = "##NOTIFICATION_CREATE_DATE###";
    public static final String NOTIFICATION_REMINDER_DATE_PLACEHOLDER = "##NOTIFICATION_REMINDER_DATE###";
    public static final String TEMPLATE_EMAIL_CONTENT_PLACEHOLDER = "##EMAIL_CONTENT_PLACEHOLDER###";
    public static final String EMAIL_CREATION_SENT_DATE_PLACEHOLDER = "##CREATION_SENT_DATE###";

    public static final String EMAIL_TEMPLATE_PATH = "emails/emailTemplate.html";
    public static final String EMAIL_CONTENT_PATH = "emails/emailContentCheckPresence.html";

    private EmailService() {
    }

    public static EmailService getInstance() {
        return INSTANCE;
    }

    public void sendPresenceCheckReminderMail() throws BtbException {
        EntityManager emOrg = EntityHelper.getEMF().createEntityManager();
        try (AutoCloseEntityManager em = new AutoCloseEntityManager(emOrg)) {

            List<Notification> notificationsReminderAndPresenceCheck = getNotificationsWithReminderAndPresenceCheck(em);

            for (Notification notification : notificationsReminderAndPresenceCheck) {
                if (Globals.NOTIFICATION_TYPE_PRESENCE_CHECK.equals(notification.getType())
                        && checkIsReminderSameDay(notification)
                        && mailWasNotSentAlready(em, notification.getIncidentId())) {
                    sendMail(notification);
                    writeNotificationMailSent(em, notification.getIncidentId());
                }
            }
        } catch (MessagingException e) {
            LOGGER.error("Error while sendPresenceCheckReminderMail", e);
        } catch (Exception e) {
            LOGGER.error( "Error while storing notificationMailSent entry", e);
        }

    }

    public List<Notification> getNotificationsWithReminderAndPresenceCheck(EntityManager em) throws BtbException {
        LOGGER.debug("getNotificationsWithReminder() is called");
        ReminderSearchFilter rsf = new ReminderSearchFilter();
        rsf.setReminderDate(new Date());
        rsf.setDontFilterResponsibilities(true);
        TblNotificationDao dao = new TblNotificationDao(em);
        BackendControllerNotification backendControllerNotification = new BackendControllerNotification();

        TblResponsibilityDao responsibilityDao = new TblResponsibilityDao(em);
        List<TblNotification> tblNotifications = backendControllerNotification.getNotificationListWithReminderFilter(responsibilityDao, dao, rsf);

        return backendControllerNotification.getNotificationMapper(em).mapListToVModel(tblNotifications);
    }

    private boolean checkIsReminderSameDay(Notification notification) {
        if (!BackendConfig.getInstance().isOnlySendSameDayReminderEmail()) return true;
        return DateUtils.isSameDay(notification.getReminderDate(), new Date());
    }

    private boolean mailWasNotSentAlready(EntityManager em, Integer incidentId) {
        LOGGER.debug("mailWasNotSentAlready is started");
        TblNotificationMailsent tms = new TblNotificationMailsentDao(em).findByIncidentId(incidentId);
        return tms == null || !tms.isMailSent();
    }

    private void writeNotificationMailSent(EntityManager em, Integer incidentId) throws Exception {
        LOGGER.debug("writeNotificationMailSent is started");
        TblNotificationMailsent tms = new TblNotificationMailsent();
        tms.setIncidentId(incidentId);
        tms.setMailSent(true);
        tms.setDateMailSent(new Timestamp(System.currentTimeMillis()));
        new TblNotificationMailsentDao(em).store(tms);
        LOGGER.debug("writeNotificationMailSent is finished");
    }

    private void sendMail(Notification notification) throws MessagingException {

        String emailText = createEmailcontent(notification);
        ElogbookEmail gfiEmail = new ElogbookEmail(BackendConfig.getInstance());
        List<String> mailAddressList = new ArrayList<>();
        mailAddressList.add(BackendConfig.getInstance().getEmailPresenceCheckRecipient());
        gfiEmail.setEmailContent(BackendConfig.getInstance().getEmailPresenceCheckSubject(), emailText);
        gfiEmail.setRecipientsTo(mailAddressList);
        gfiEmail.sendEmail();
    }

    private String createEmailcontent(Notification notification) {

        String emailText = loadFromResource(EMAIL_CONTENT_PATH);
        String createDateString = DateFormatUtils.format(notification.getCreateDate(), DATE_PATTERN);
        String reminderDateString = DateFormatUtils.format(notification.getReminderDate(), DATE_PATTERN);
        emailText = emailText.replace(NOTIFICATION_CREATE_DATE_PLACEHOLDER, createDateString);
        emailText = emailText.replace(NOTIFICATION_REMINDER_DATE_PLACEHOLDER, reminderDateString);
        emailText = emailText.replace(NOTIFICATION_TEXT_PLACEHOLDER, notification.getNotificationText());

        if (!BackendConfig.getInstance().isHtmlEmail() || !BackendConfig.getInstance().isUseHtmlEmailTemplate()) {
            return emailText;
        }

        String htmlEmailTemplate = loadFromResource(EMAIL_TEMPLATE_PATH);
        String nowDateString = DateFormatUtils.format(new Date(), DATE_PATTERN);
        htmlEmailTemplate = htmlEmailTemplate.replace(EMAIL_CREATION_SENT_DATE_PLACEHOLDER, nowDateString);
        return htmlEmailTemplate.replace(TEMPLATE_EMAIL_CONTENT_PLACEHOLDER, emailText);
    }

    private String loadFromResource(String path) {
        ResourceLoaderBase resourceLoaderBase = new ResourceLoaderBase();
        return resourceLoaderBase.loadStringFromResource(path);
    }
}
