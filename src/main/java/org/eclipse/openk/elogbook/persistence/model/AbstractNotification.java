/**
******************************************************************************
* Copyright © 2017-2018 PTA GmbH.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* 
*     http://www.eclipse.org/legal/epl-v10.html
* 
******************************************************************************
*/
package org.eclipse.openk.elogbook.persistence.model;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;
import javax.persistence.SequenceGenerator;
import java.io.Serializable;
import java.sql.Timestamp;


/**
 * The persistent class for the tbl_notification database table.
 *
 */
@MappedSuperclass
public class AbstractNotification implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TBL_NOTIF_ID_SEQ")
	@SequenceGenerator(name = "TBL_NOTIF_ID_SEQ", sequenceName = "TBL_NOTIF_ID_SEQ", allocationSize = 1)
	@Column(name = "id", updatable = false)
	private Integer id;

	@Column(name = "incident_id")
	private Integer incidentId;

	@Column(name = "begin_date")
	private Timestamp beginDate;

	@Column(name = "create_date")
	private Timestamp createDate;

	@Column(name = "create_user")
	private String createUser;

	@Column(name = "expected_finished_date")
	private Timestamp expectedFinishedDate;

	@Column(name = "finished_date")
	private Timestamp finishedDate;

	@Column(name = "free_text")
	private String freeText;

	@Column(name = "free_text_extended")
	private String freeTextExtended;

	@Column(name = "mod_date")
	private Timestamp modDate;

	@Column(name = "mod_user")
	private String modUser;

	@Column(name = "notification_text")
	private String notificationText;

	@Column(name = "reminder_date")
	private Timestamp reminderDate;

	@Column(name = "responsibility_control_point")
	private String responsibilityControlPoint;

	@Column(name = "responsibility_forwarding")
	private String responsibilityForwarding;

	@Column(name = "responsibility_forwarding_uuid")
	private String responsibilityForwardingUuid;

	@Column(name = "version")
	private Integer version;

	@Column(name = "admin_flag")
	private Boolean adminFlag;

	@Column(name = "type")
	private String type;

	//bi-directional many-to-one association to RefBranch
	@ManyToOne
	@JoinColumn(name = "fk_ref_branch")
	private RefBranch refBranch;

	//bi-directional many-to-one association to RefNotificationStatus
	@ManyToOne
	@JoinColumn(name = "fk_ref_notification_status")
	private RefNotificationStatus refNotificationStatus;

	@ManyToOne
	@JoinColumn(name = "fk_ref_grid_territory")
	private RefGridTerritory refGridTerritory;

	//bi-directional many-to-one association to RefNotificationStatus
	@ManyToOne
	@JoinColumn(name = "fk_ref_notification_priority")
	private RefNotificationPriority refNotificationPriority;

	public AbstractNotification() {
		// empty Standard constructor
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Timestamp getBeginDate() {
		return beginDate;
	}

	public void setBeginDate(Timestamp beginDate) {
		this.beginDate = beginDate;
	}

	public Timestamp getCreateDate() {
		return this.createDate;
	}

	public void setCreateDate(Timestamp createDate) {
		this.createDate = createDate;
	}

	public String getCreateUser() {
		return this.createUser;
	}

	public void setCreateUser(String creator) {
		this.createUser = creator;
	}

	public Timestamp getExpectedFinishedDate() {
		return this.expectedFinishedDate;
	}

	public void setExpectedFinishedDate(Timestamp expectedFinishedDate) {
		this.expectedFinishedDate = expectedFinishedDate;
	}

	public Timestamp getFinishedDate() {
		return this.finishedDate;
	}

	public void setFinishedDate(Timestamp finishedDate) {
		this.finishedDate = finishedDate;
	}

	public String getFreeText() {
		return this.freeText;
	}

	public void setFreeText(String freeText) {
		this.freeText = freeText;
	}

	public String getFreeTextExtended() {
		return this.freeTextExtended;
	}

	public void setFreeTextExtended(String freeTextExtended) {
		this.freeTextExtended = freeTextExtended;
	}

	public Integer getIncidentId() {
		return this.incidentId;
	}

	public void setIncidentId(Integer incidentId) {
		this.incidentId = incidentId;
	}

	public Timestamp getModDate() {
		return this.modDate;
	}

	public void setModDate(Timestamp modDate) {
		this.modDate = modDate;
	}

	public String getModUser() {
		return this.modUser;
	}

	public void setModUser(String modUser) {
		this.modUser = modUser;
	}

	public String getNotificationText() {
		return this.notificationText;
	}

	public void setNotificationText(String notificationText) {
		this.notificationText = notificationText;
	}

	public Timestamp getReminderDate() {
		return this.reminderDate;
	}

	public void setReminderDate(Timestamp reminderDate) {
		this.reminderDate = reminderDate;
	}

	public String getResponsibilityControlPoint() {
		return this.responsibilityControlPoint;
	}

	public void setResponsibilityControlPoint(String responsibilityControlPoint) {
		this.responsibilityControlPoint = responsibilityControlPoint;
	}

	public String getResponsibilityForwarding() {
		return this.responsibilityForwarding;
	}

	public void setResponsibilityForwarding(String responsibilityForwarding) {
		this.responsibilityForwarding = responsibilityForwarding;
	}

	public Integer getVersion() {
		return this.version;
	}

	public void setVersion(Integer version) {
		this.version = version;
	}

	public RefBranch getRefBranch() {
		return this.refBranch;
	}

	public void setRefBranch(RefBranch refBranch) {
		this.refBranch = refBranch;
	}

	public RefNotificationStatus getRefNotificationStatus() {
		return this.refNotificationStatus;
	}

	public void setRefNotificationStatus(RefNotificationStatus refNotificationStatus) {
		this.refNotificationStatus = refNotificationStatus;
	}

	public RefGridTerritory getRefGridTerritory() {
		return refGridTerritory;
	}

	public void setRefGridTerritory(RefGridTerritory refGridTerritory) {
		this.refGridTerritory = refGridTerritory;
	}

	public Boolean isAdminFlag() {
		return adminFlag;
	}

	public void setAdminFlag(Boolean adminFlag) {
		this.adminFlag = adminFlag;
	}

	public RefNotificationPriority getRefNotificationPriority() {
		return refNotificationPriority;
	}

	public void setRefNotificationPriority(RefNotificationPriority refNotificationPriority) {
		this.refNotificationPriority = refNotificationPriority;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getResponsibilityForwardingUuid() {
		return responsibilityForwardingUuid;
	}

	public void setResponsibilityForwardingUuid(String responsibilityForwardingUuid) {
		this.responsibilityForwardingUuid = responsibilityForwardingUuid;
	}
}
