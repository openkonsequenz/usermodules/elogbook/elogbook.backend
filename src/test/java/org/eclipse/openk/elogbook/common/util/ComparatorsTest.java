/**
******************************************************************************
* Copyright © 2017-2018 PTA GmbH.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* 
*     http://www.eclipse.org/legal/epl-v10.html
* 
******************************************************************************
*/
package org.eclipse.openk.elogbook.common.util;

import static org.junit.Assert.assertTrue;

import org.eclipse.openk.elogbook.common.util.Comparators.TblResponsibilityIdComparator;
import org.eclipse.openk.elogbook.persistence.model.TblResponsibility;
import org.junit.Test;
import org.powermock.reflect.Whitebox;

public class ComparatorsTest {

  @Test
  public void testTblResponsibilityIdComparator(){
    TblResponsibility tblResponsibility1 = new TblResponsibility();
    tblResponsibility1.setId(1);
    TblResponsibility tblResponsibility2 = new TblResponsibility();
    tblResponsibility2.setId(2);

    TblResponsibilityIdComparator tblResponsibilityIdComparator = new TblResponsibilityIdComparator();

    int compareResult = tblResponsibilityIdComparator.compare(tblResponsibility1, tblResponsibility2);
    assertTrue(compareResult < 0);

  }
  
  @Test(expected = IllegalStateException.class)
  public void testMy() throws Exception{
	  Whitebox.invokeConstructor(Comparators.class);
  }

}
