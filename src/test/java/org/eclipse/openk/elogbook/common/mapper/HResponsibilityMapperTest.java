/**
******************************************************************************
* Copyright © 2017-2018 PTA GmbH.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* 
*     http://www.eclipse.org/legal/epl-v10.html
* 
******************************************************************************
*/
package org.eclipse.openk.elogbook.common.mapper;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import org.eclipse.openk.elogbook.common.Globals;
import org.eclipse.openk.elogbook.persistence.model.HTblResponsibility;
import org.eclipse.openk.elogbook.persistence.model.RefBranch;
import org.eclipse.openk.elogbook.persistence.model.RefGridTerritory;
import org.eclipse.openk.elogbook.persistence.model.TblResponsibility;
import org.eclipse.openk.elogbook.viewmodel.HistoricalResponsibility;
import org.eclipse.openk.elogbook.viewmodel.HistoricalShiftChanges;
import org.eclipse.openk.elogbook.viewmodel.Responsibility;
import org.eclipse.openk.elogbook.viewmodel.TerritoryResponsibility;
import org.junit.Before;
import org.junit.Test;

public class HResponsibilityMapperTest {

  private List<TblResponsibility> tblResponsibilityList = new ArrayList<>();
  private List<HTblResponsibility> htblResponsibilityList = new ArrayList<>();

  Calendar calendar = Calendar.getInstance();
  java.util.Date compareDate = calendar.getTime();

  LocalDateTime ldtCreate = LocalDateTime.parse("2017-06-19T15:00:00");
  Timestamp tsCreate = Timestamp.valueOf(ldtCreate);

  LocalDateTime ldtMod = LocalDateTime.parse("2017-06-20T15:00:00");
  Timestamp tsMod = Timestamp.valueOf(ldtMod);

  LocalDateTime ldtTrans = LocalDateTime.parse("2017-06-21T15:00:00");
  Timestamp tsTrans = Timestamp.valueOf(ldtTrans);

  @Before
  public void createResponsibilityList() {

    RefBranch refBranchElectricity = new RefBranch();
    refBranchElectricity.setName(Globals.ELECTRICITY_MARK);
    RefBranch refBranchGas = new RefBranch();
    refBranchGas.setName(Globals.GAS_MARK);
    RefBranch refBranchDistrictHeat = new RefBranch();
    refBranchDistrictHeat.setName(Globals.DISTRICT_HEAT_MARK);
    RefBranch refBranchWater = new RefBranch();
    refBranchWater.setName(Globals.WATER_MARK);
    RefBranch refBranchCfm = new RefBranch();
    refBranchCfm.setName(Globals.CFM_MARK);

    RefGridTerritory refGridTerritory1 = new RefGridTerritory();
    RefGridTerritory refGridTerritory2 = new RefGridTerritory();
    RefGridTerritory refGridTerritory3 = new RefGridTerritory();

    refGridTerritory1.setDescription("Mannheim");
    refGridTerritory2.setDescription("Offenbach");
    refGridTerritory3.setDescription("Stuttgart");


    //Mannheim all (4) responsibilities
    TblResponsibility tblResponsibility0 = new TblResponsibility();
    tblResponsibility0.setId(1);
    tblResponsibility0.setResponsibleUser("responsibleUser1");
    tblResponsibility0.setRefGridTerritory(refGridTerritory1);
    tblResponsibility0.setRefBranch(refBranchElectricity);
    tblResponsibility0.setCreateDate(tsCreate);
    tblResponsibility0.setModDate(tsMod);
    tblResponsibility0.setModUser("modUserRespTest");

    TblResponsibility tblResponsibility1 = new TblResponsibility();
    tblResponsibility1.setId(2);
    tblResponsibility1.setResponsibleUser("responsibleUser1");
    tblResponsibility1.setRefGridTerritory(refGridTerritory1);
    tblResponsibility1.setRefBranch(refBranchGas);

    TblResponsibility tblResponsibility2 = new TblResponsibility();
    tblResponsibility2.setId(3);
    tblResponsibility2.setResponsibleUser("responsibleUser1");
    tblResponsibility2.setRefGridTerritory(refGridTerritory1);
    tblResponsibility2.setRefBranch(refBranchWater);

    TblResponsibility tblResponsibility3 = new TblResponsibility();
    tblResponsibility3.setId(4);
    tblResponsibility3.setResponsibleUser("responsibleUser1");
    tblResponsibility3.setNewResponsibleUser("newResponsibleUser1");
    tblResponsibility3.setRefGridTerritory(refGridTerritory1);
    tblResponsibility3.setRefBranch(refBranchDistrictHeat);

    //Offenbach 1 responsibility
    TblResponsibility tblResponsibility4 = new TblResponsibility();
    tblResponsibility4.setId(33);
    tblResponsibility4.setResponsibleUser("responsibleUser1");
    tblResponsibility4.setRefGridTerritory(refGridTerritory2);
    tblResponsibility4.setRefBranch(refBranchWater);

    //Stuttgart 2 responsibilities
    TblResponsibility tblResponsibility5 = new TblResponsibility();
    tblResponsibility5.setId(44);
    tblResponsibility5.setResponsibleUser("responsibleUser1");
    tblResponsibility5.setNewResponsibleUser("newResponsibleUser1");
    tblResponsibility5.setRefGridTerritory(refGridTerritory3);
    tblResponsibility5.setRefBranch(refBranchDistrictHeat);

    TblResponsibility tblResponsibility6 = new TblResponsibility();
    tblResponsibility6.setId(45);
    tblResponsibility6.setResponsibleUser("responsibleUser1");
    tblResponsibility6.setRefGridTerritory(refGridTerritory3);
    tblResponsibility6.setRefBranch(refBranchCfm);

    tblResponsibilityList.add(tblResponsibility0);
    tblResponsibilityList.add(tblResponsibility1);
    tblResponsibilityList.add(tblResponsibility2);
    tblResponsibilityList.add(tblResponsibility3);
    tblResponsibilityList.add(tblResponsibility4);
    tblResponsibilityList.add(tblResponsibility5);
    tblResponsibilityList.add(tblResponsibility6);

  }

  @Before
  public void createHResponsibilityList() {

    RefBranch refBranchElectricity = new RefBranch();
    refBranchElectricity.setName(Globals.ELECTRICITY_MARK);
    RefBranch refBranchGas = new RefBranch();
    refBranchGas.setName(Globals.GAS_MARK);
    RefBranch refBranchDistrictHeat = new RefBranch();
    refBranchDistrictHeat.setName(Globals.DISTRICT_HEAT_MARK);
    RefBranch refBranchWater = new RefBranch();
    refBranchWater.setName(Globals.WATER_MARK);

    RefGridTerritory refGridTerritory1 = new RefGridTerritory();
    RefGridTerritory refGridTerritory2 = new RefGridTerritory();
    RefGridTerritory refGridTerritory3 = new RefGridTerritory();

    refGridTerritory1.setDescription("Mannheim");
    refGridTerritory2.setDescription("Offenbach");
    refGridTerritory3.setDescription("Stuttgart");



    //Mannheim all (4) responsibilities
    HTblResponsibility htblResponsibility0 = new HTblResponsibility();
    htblResponsibility0.setId(1);
    htblResponsibility0.setResponsibleUser("responsibleUser1");
    htblResponsibility0.setRefGridTerritory(refGridTerritory1);
    htblResponsibility0.setRefBranch(refBranchElectricity);
    htblResponsibility0.setCreateDate(tsCreate);
    htblResponsibility0.setModDate(tsMod);
    htblResponsibility0.setModUser("modUserRespTest1");

    HTblResponsibility htblResponsibility1 = new HTblResponsibility();
    htblResponsibility1.setId(2);
    htblResponsibility1.setTransferDate(tsTrans);
    htblResponsibility1.setResponsibleUser("responsibleUser2");
    htblResponsibility1.setRefGridTerritory(refGridTerritory1);
    htblResponsibility1.setRefBranch(refBranchGas);

    HTblResponsibility htblResponsibility2 = new HTblResponsibility();
    htblResponsibility2.setId(3);
    htblResponsibility2.setResponsibleUser("responsibleUser3");
    htblResponsibility2.setRefGridTerritory(refGridTerritory1);
    htblResponsibility2.setRefBranch(refBranchWater);

    HTblResponsibility htblResponsibility3 = new HTblResponsibility();
    htblResponsibility3.setId(4);
    htblResponsibility3.setResponsibleUser("responsibleUser4");
    htblResponsibility3.setRefGridTerritory(refGridTerritory1);
    htblResponsibility3.setModUser("modResponsibleUser4");
    htblResponsibility3.setRefBranch(refBranchDistrictHeat);

    //Offenbach 1 responsibility
    HTblResponsibility htblResponsibility4 = new HTblResponsibility();
    htblResponsibility4.setId(33);
    htblResponsibility4.setResponsibleUser("responsibleUser5");
    htblResponsibility4.setRefGridTerritory(refGridTerritory2);
    htblResponsibility4.setRefBranch(refBranchWater);

    //Stuttgart 2 responsibilities
    HTblResponsibility htblResponsibility5 = new HTblResponsibility();
    htblResponsibility5.setId(44);
    htblResponsibility5.setResponsibleUser("responsibleUser6");
    htblResponsibility5.setTransactionId(3);
    htblResponsibility5.setRefGridTerritory(refGridTerritory3);
    htblResponsibility5.setRefBranch(refBranchDistrictHeat);

    HTblResponsibility htblResponsibility6 = new HTblResponsibility();
    htblResponsibility6.setId(45);
    htblResponsibility6.setResponsibleUser("responsibleUser7");
    htblResponsibility6.setRefGridTerritory(refGridTerritory3);
    htblResponsibility6.setRefBranch(refBranchWater);

    htblResponsibilityList.add(htblResponsibility0);
    htblResponsibilityList.add(htblResponsibility1);
    htblResponsibilityList.add(htblResponsibility2);
    htblResponsibilityList.add(htblResponsibility3);
    htblResponsibilityList.add(htblResponsibility4);
    htblResponsibilityList.add(htblResponsibility5);
    htblResponsibilityList.add(htblResponsibility6);

  }

  @Test
  public void testMapFromTblResponsibility() {
    TblResponsibility tblResponsibility = tblResponsibilityList.get(0);
    HTblResponsibility hTblResponsibility = HResponsibilityMapper.mapFromTblResponsibility(tblResponsibility);

    assertEquals(hTblResponsibility.getResponsibleUser(), tblResponsibility.getResponsibleUser());
    assertEquals(hTblResponsibility.getCreateDate(), tblResponsibility.getCreateDate());
    assertEquals(hTblResponsibility.getCreateUser(), tblResponsibility.getCreateUser());
    assertEquals(hTblResponsibility.getModDate(), tblResponsibility.getModDate());
    assertEquals(hTblResponsibility.getModUser(), tblResponsibility.getModUser());
    assertEquals(hTblResponsibility.getRefGridTerritory(), tblResponsibility.getRefGridTerritory());
    assertEquals(hTblResponsibility.getRefBranch(), tblResponsibility.getRefBranch());

  }

  @Test
  public void testMapToVModel() {
    HResponsibilityMapper hresponsibilityMappy = HResponsibilityTestHelper.createMapper();
    HTblResponsibility htblResponsibility = htblResponsibilityList.get(0);
    HistoricalResponsibility historicalResponsibility = hresponsibilityMappy.mapToVModel(htblResponsibility);

    assertNotNull(historicalResponsibility);
    assertEquals(historicalResponsibility.getId(), htblResponsibility.getId());
    assertEquals(historicalResponsibility.getTransactionId(), htblResponsibility.getTransactionId());
    assertEquals(historicalResponsibility.getResponsibleUser(), htblResponsibility.getResponsibleUser());
    assertEquals(historicalResponsibility.getCreateDate(), htblResponsibility.getCreateDate());
    assertEquals(historicalResponsibility.getCreateUser(), htblResponsibility.getCreateUser());
    assertEquals(historicalResponsibility.getTransferDate(), htblResponsibility.getTransferDate());
    assertEquals(historicalResponsibility.getModDate(), htblResponsibility.getModDate());
    assertEquals(historicalResponsibility.getModUser(), htblResponsibility.getModUser());
    assertEquals(historicalResponsibility.getRefGridTerritory(), htblResponsibility.getRefGridTerritory());
    assertEquals(historicalResponsibility.getRefBranch(), htblResponsibility.getRefBranch());

  }

  @Test
  public void testMapToVModelList() {

    HResponsibilityMapper hresponsibilityMappy = HResponsibilityTestHelper.createMapper();
    List<HistoricalResponsibility> historicalResponsibilityList = hresponsibilityMappy.mapToVModelList(htblResponsibilityList);

    assertEquals(historicalResponsibilityList.get(0).getRefGridTerritory().getDescription(), "Mannheim");
    assertEquals(historicalResponsibilityList.get(0).getResponsibleUser(), "responsibleUser1");
    assertTrue(historicalResponsibilityList.get(0).getId() == 1);
    assertTrue(historicalResponsibilityList.get(5).getTransactionId() == 3);
    assertEquals(historicalResponsibilityList.get(0).getRefBranch().getName(), Globals.ELECTRICITY_MARK);

    assertEquals(historicalResponsibilityList.get(0).getModUser(), "modUserRespTest1");

    assertEquals(historicalResponsibilityList.get(4).getRefGridTerritory().getDescription(), "Offenbach");
    assertEquals(historicalResponsibilityList.get(5).getRefGridTerritory().getDescription(), "Stuttgart");
    assertEquals(historicalResponsibilityList.get(0).getCreateDate(), tsCreate);
    assertEquals(historicalResponsibilityList.get(0).getModDate(), tsMod);
    assertEquals(historicalResponsibilityList.get(1).getTransferDate(), tsTrans);
  }

  @Test
  public void testMapTblResponsibilitiesInPeriod() {

    HResponsibilityMapper hresponsibilityMappy = HResponsibilityTestHelper.createMapper();
    HistoricalShiftChanges historicalShiftChanges = hresponsibilityMappy.mapTblResponsibilitiesInPeriod(htblResponsibilityList, compareDate, compareDate);

    assertEquals(historicalShiftChanges.getTransferDateFrom(), compareDate);
    assertEquals(historicalShiftChanges.getTransferDateTo(), compareDate);
  }

  @Test
  public void testHTblResponsibilityNull() {

    List<HTblResponsibility> hTblResponsibilities = new ArrayList<>();
    HResponsibilityMapper mappy = HResponsibilityTestHelper.createMapper();
    hTblResponsibilities.add(null);
    List<HistoricalResponsibility> hresList = mappy.mapToVModelList(hTblResponsibilities);
    assertNull(hresList.get(0));

  }

  @Test
  public void testHTblResponsibilityFkNull() {

    HTblResponsibility hTblResponsibility =  new HTblResponsibility();
    HResponsibilityMapper mappy = HResponsibilityTestHelper.createMapper();
    HistoricalResponsibility historicalResponsibility = mappy.mapToVModel(hTblResponsibility);
    assertNull(historicalResponsibility.getRefGridTerritory());
    assertNull(historicalResponsibility.getRefBranch());
  }

  @Test
  public void testMapToContainerVModelList() {

    HResponsibilityMapper mappy = HResponsibilityTestHelper.createMapper();
    List<TerritoryResponsibility> territoryResponsibilityList = mappy.mapToContainerVModelList(htblResponsibilityList);

    assertTrue(territoryResponsibilityList.size() == 3);
    assertEquals(territoryResponsibilityList.get(0).getGridTerritoryDescription(), "Mannheim");
    assertEquals(territoryResponsibilityList.get(0).getResponsibilityList().get(0).getResponsibleUser(), "responsibleUser1");
    assertTrue(territoryResponsibilityList.get(0).getResponsibilityList().get(0).getId() == 1);
    assertTrue(territoryResponsibilityList.get(0).getResponsibilityList().size() == 4);
    assertEquals(territoryResponsibilityList.get(0).getResponsibilityList().get(0).getBranchName() , Globals.ELECTRICITY_MARK);

    assertTrue(territoryResponsibilityList.get(0).getResponsibilityList().get(3).getId() == 4);

    assertEquals(territoryResponsibilityList.get(1).getGridTerritoryDescription(), "Offenbach");
    assertEquals(territoryResponsibilityList.get(2).getGridTerritoryDescription(), "Stuttgart");
  }

  @Test
  public void testMapToContainerVModelList_EmptyList() {

    List<HTblResponsibility> htblResponsibilityList = new ArrayList<>();
    HResponsibilityMapper mappy = HResponsibilityTestHelper.createMapper();
    List<TerritoryResponsibility> territoryResponsibilities = mappy.mapToContainerVModelList(htblResponsibilityList);

    assertTrue(territoryResponsibilities.size() == 0);

  }


  @Test
  public void testMapToContainerVModel() {
    HResponsibilityMapper mappy = HResponsibilityTestHelper.createMapper();
    TerritoryResponsibility terRespExist = new TerritoryResponsibility();;

    List<Responsibility> responsibilityList = new ArrayList<>();

    terRespExist.setGridTerritoryDescription(htblResponsibilityList.get(0).getRefGridTerritory().getDescription());
    terRespExist.setResponsibilityList(responsibilityList);
    responsibilityList.add(mappy.mapToVModelForContainer(htblResponsibilityList.get(0)));
    TerritoryResponsibility terResp = mappy.mapToContainerVModel(htblResponsibilityList.get(0), terRespExist);

    assertEquals(terResp.getGridTerritoryDescription(), htblResponsibilityList.get(0).getRefGridTerritory().getDescription());
    assertEquals(terResp.getResponsibilityList(), responsibilityList);
  }


  @Test
  public void testMapToVModelForContainer() {
    HResponsibilityMapper hresponsibilityMappy = HResponsibilityTestHelper.createMapper();
    HTblResponsibility htblResponsibility = htblResponsibilityList.get(0);
    Responsibility responsibility = hresponsibilityMappy.mapToVModelForContainer(htblResponsibility);

    assertNotNull(responsibility);
    assertEquals(responsibility.getId(), htblResponsibility.getId());
    assertEquals(responsibility.getResponsibleUser(), htblResponsibility.getResponsibleUser());
    assertEquals(responsibility.getBranchName(), htblResponsibility.getRefBranch().getName());
    assertTrue(responsibility.isActive());
  }

}