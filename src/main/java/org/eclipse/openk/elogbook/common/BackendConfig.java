/**
******************************************************************************
* Copyright © 2017-2018 PTA GmbH.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* 
*     http://www.eclipse.org/legal/epl-v10.html
* 
******************************************************************************
*/
package org.eclipse.openk.elogbook.common;

import org.eclipse.openk.elogbook.common.util.ResourceLoaderBase;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import java.io.FileNotFoundException;

public class BackendConfig {

  private static final Logger LOGGER = LogManager.getLogger(BackendConfig.class.getName());

  private static String configFileName = "backendConfigDevLocal.json";
  private String portalBaseURL;
  private String contactBaseDataApiUrl;
  private String elogbookModuleName;

  private int fileRowToRead;
  private String importFilesFolderPath;

  private String activePersistencyUnit;
  private String applicationAccessRole;

  private String emailSmtpHost;
  private String emailSender;
  private String emailSmtpPort;

  private boolean isHtmlEmail;
  private boolean isUseHtmlEmailTemplate;
  private String emailPresenceCheckRecipient;
  private String emailPresenceCheckSubject;

  private String cronEmailPresenceCheck;
  private boolean enableEmailPresenceCheckJob;
  private boolean isOnlySendSameDayReminderEmail;

  private String cronSyncContacts;
  private boolean enableContactSyncJob;
  private String contactServiceTechUser;
  private String contactServiceTechUserPwd;

  private static BackendConfig instance;

  private BackendConfig() {
  }

  public static synchronized BackendConfig getInstance() {
    if (instance == null) {
      String jsonConfig = loadJsonConfig();
      if (jsonConfig == null || jsonConfig.isEmpty()) {
        LOGGER.error("FileNotFound: jsonConfig is empty or NULL");
      } else {
        LOGGER.info("backendConfigFile " + configFileName + " successfully loaded.");
      }
      instance = JsonGeneratorBase.getGson().fromJson(jsonConfig, BackendConfig.class);
    }

    return instance;
  }

  private static String loadJsonConfig() {
    ResourceLoaderBase resourceLoaderBase = new ResourceLoaderBase();
    LOGGER.info("backendConfigFile is: " + configFileName);
    String jsonConfig = resourceLoaderBase.loadStringFromResource(configFileName);
    if (jsonConfig == null || jsonConfig.isEmpty()) {
      LOGGER.info("backendConfigFile not found in resources, loading from external path now...");
      jsonConfig = resourceLoaderBase.loadFromPath(configFileName);
    }
    return jsonConfig;
  }

  public String getPortalBaseURL() {
    return portalBaseURL;
  }

  public String getContactBaseDataApiUrl() {
    return contactBaseDataApiUrl;
  }

  public String getImportFilesFolderPath() {
    return importFilesFolderPath;
  }

  public int getFileRowToRead() {
    return fileRowToRead;
  }

  public String getActivePersistencyUnit() {
    return activePersistencyUnit;
  }

  public String getApplicationAccessRole() {
    return applicationAccessRole;
  }

  public static String getConfigFileName() {
    return configFileName;
  }

  public static void setConfigFileName(String configFileName) {
    BackendConfig.configFileName = configFileName;
  }

  public String getCronEmailPresenceCheck() {
    return cronEmailPresenceCheck;
  }

  public String getEmailSmtpHost() {
    return emailSmtpHost;
  }

  public String getEmailSender() {
    return emailSender;
  }

  public String getEmailSmtpPort() {
    return emailSmtpPort;
  }

  public boolean isEnableEmailPresenceCheckJob() {
    return enableEmailPresenceCheckJob;
  }

  public boolean isHtmlEmail() {
    return isHtmlEmail;
  }

  public String getEmailPresenceCheckRecipient() {
    return emailPresenceCheckRecipient;
  }

  public String getEmailPresenceCheckSubject() {
    return emailPresenceCheckSubject;
  }

  public boolean isUseHtmlEmailTemplate() {
    return isUseHtmlEmailTemplate;
  }

  public boolean isOnlySendSameDayReminderEmail() {
    return isOnlySendSameDayReminderEmail;
  }

  public String getCronSyncContacts() {
    return cronSyncContacts;
  }

  public boolean isEnableContactSyncJob() {
    return enableContactSyncJob;
  }

  public String getContactServiceTechUser() {
    return contactServiceTechUser;
  }

  public String getContactServiceTechUserPwd() {
    return contactServiceTechUserPwd;
  }

  public String getElogbookModuleName() {
    return elogbookModuleName;
  }
}


