/**
******************************************************************************
* Copyright © 2017-2018 PTA GmbH.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* 
*     http://www.eclipse.org/legal/epl-v10.html
* 
******************************************************************************
*/
package org.eclipse.openk.elogbook.common;

public enum NotificationStatus {

	UNKNOWN(0, "unknown"),
	OPEN(1, "open"),
	INPROGRESS(2, "inprogress"),
	FINISHED(3, "finished"),
	CLOSED(4, "closed");

    public final int id;
	public final String statusName;

	NotificationStatus(int id, String name) {
        this.id = id;
        this.statusName = name;
	}
}
