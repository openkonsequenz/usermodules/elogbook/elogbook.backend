/**
******************************************************************************
* Copyright © 2017-2018 PTA GmbH.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* 
*     http://www.eclipse.org/legal/epl-v10.html
* 
******************************************************************************
*/
package org.eclipse.openk.elogbook.viewmodel;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.eclipse.openk.elogbook.common.JsonGeneratorBase;
import org.eclipse.openk.elogbook.common.util.ResourceLoaderBase;
import org.junit.Test;

public class GeneralReturnItemTest extends ResourceLoaderBase {
	// IMPORTANT TEST!!!
	// Make sure, our Interface produces a DEFINED Json!
	// Changes in the interface will HOPEFULLY crash here!!!

	@Test
	public void TestStructureAgainstJson() {
		String json = super.loadStringFromResource("testGeneralReturnItem.json");
		GeneralReturnItem gRRet = JsonGeneratorBase.getGson().fromJson(json, GeneralReturnItem.class);
		assertEquals(gRRet.getRet(), "It works!");
	}

	@Test
	public void TestSetters() {
		GeneralReturnItem gri = new GeneralReturnItem("firstStrike");
		assertTrue("firstStrike".equals(gri.getRet()));
		gri.setRet("Retour");
		assertTrue("Retour".equals(gri.getRet()));

	}

}
