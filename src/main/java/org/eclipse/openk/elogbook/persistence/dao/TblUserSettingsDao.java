/**
******************************************************************************
* Copyright © 2017-2018 PTA GmbH.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* 
*     http://www.eclipse.org/legal/epl-v10.html
* 
******************************************************************************
*/
package org.eclipse.openk.elogbook.persistence.dao;

import org.eclipse.openk.elogbook.exceptions.BtbInternalServerError;
import org.eclipse.openk.elogbook.persistence.model.TblUserSettings;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.Optional;

public class TblUserSettingsDao extends GenericDaoJpa<TblUserSettings, Integer> {
	private static final String FROM = " from ";


	public TblUserSettingsDao() {
		super();
	}

	public TblUserSettingsDao(EntityManager em) {
		super(em);
	}

	/**
	 * Store the settings of a given user
	 *
	 *
	 */
	public void storeUserSettings() throws BtbInternalServerError {
		// for technical reasons
	}

	@SuppressWarnings("unchecked")
	public Optional<TblUserSettings> getSettingsForUser(String user, String settingType) {

		try {
			String selectString = FROM + TblUserSettings.class.getSimpleName() + " v where UPPER(v.username) = UPPER(:username)" +
					"AND UPPER(v.settingType) = UPPER(:settingType)";
			Query q = getEM().createQuery(selectString);
			q.setParameter("username", user);
			q.setParameter( "settingType", settingType);
			Object result = q.getSingleResult();
			return Optional.ofNullable((TblUserSettings)result);
		} catch (Throwable t) { //NOSONAR
			LOGGER.error(t.getMessage());
			return Optional.empty();
		}
	}
}
